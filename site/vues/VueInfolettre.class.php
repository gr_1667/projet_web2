<?php

/**
 * classe VueInfolettre
 * @author Marcio Pelegrini
 * @version 2015-07-02
 */
class VueInfolettre {

    /**
     * @access public
     * @param string $sMsg 
     */
    public static function afficherFormAdd($sMsg = "") {
        $oLettre = new Infolettre(" ");
        VueInfolettre::afficherFormMod($oLettre, $sMsg);
    }
    
    /**
     * @access public
     * @param Infolettre $oLettre
     * @param string $sMsg 
     */
    public static function afficherFormMod(Infolettre $oLettre, $sMsg = "") {
        echo "<div>
            <form  action =\"index.php?s=6&amp;action=add&amp;email=" . $oLettre->getSEmailInfo() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"email\" value =\"" . $oLettre->getSEmailInfo() . "\">
                <input type =\"hidden\" name =\"action\" value =\"add\">
                <fieldset>
                    <br/>
                    <input id=\"formInfolettre\" type =\"email\" name =\"txtEmailInfo\" placeholder=\"email@exemple.com\" size=\"50\" id =\"cc\" required=\"required\" value =\"" . $oLettre->getSEmailInfo(). "\">
                    <br/>
                    
                    <div>
                        <input type=\"submit\" id=\"btnSabonner\" name=\"cmd\" value\"S'abonner\"/>
                    </div>
                </fieldset>
            </form></div>
        ";
    }
}

?>