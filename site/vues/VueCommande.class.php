<?php

/**
 * 
 * Code skeleton generated by dia-uml2php5 plugin
 * written by KDO kdo@zpmag.com
 */
class VueCommande {

    /**
     * @access public
     * @param array aProduits 
     */
    public static function afficherPageCommande($aCommande) {

    /*--========================================================
                              CONTENU
    ========================================================== */
    echo ' 
        
    <main>
    <section class="well well__offset-3">
                <div class="container">';
                    echo "
		 		<h2><em>Mes</em>Commandes</h2>
		 		<h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=add\" title=\"ajouter une Commande\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter une nouvelle commande</span></a></h3>
			
				<table>
					<tr>
						<th>ID</th>
						<th>Date</th>
						<th>Rabais</th>
						<th>Mode de Paiement</th>
						<th>Livraisont</th>
						<th>Actif</th>
					</tr>
					<tr>
						<td>" .$aCommande[0]['COM_ID'] ."</td>
						<td>" .$aCommande[0]['COM_DATE']. "</td>
						<td>" .$aCommande[0]['COM_RABAIS']. "</td>
						<td>" .$aCommande[0]['COM_MOD_PAIEMENT']. "</td>
						<td>" .$aCommande[0]['COM_LIVRAISON']. "</td>
						<td>" .$aCommande[0]['COM_ACTIF']. "</td>
					</tr>
				</table>
				<br>
				<h3>Produits:</h3>
				<br>
				<table>
					<tr>
						<th>Produit ID</th>
						<th>Produit nom</th>
						<th>Produit Image</th>
						<th>Quantité</th>
					</tr>";
					for($i=0; $i < count($aCommande); $i++) {
					echo "
					<tr>
						<td>".$aCommande[$i]['PROD_ID']."</td>
						<td>".$aCommande[$i]['PROD_NOM']."</td>
						<td><img class=\"images_table\"src=\"./medias/Produits/".$aCommande[$i]['PROD_IMAGE']."\" alt=\"".$aCommande[$i]['PROD_IMAGE']."\"></td>
						<td>".$aCommande[$i]['PROD_QTT']."</td>
					</tr>";
					}
					if($i==0){
                    echo "
                    <tr><td>Aucune produit dans la commande.</td><td>&nbsp;</td></tr>
                    ";
            }
				echo "
				</table>
		 	";
    }
	
	/**
     * @access public
     * @param Produit $oProduit
     * @param string $sMsg 
     */
    public static function afficherFormCommande($sMsg = "") {
		$oCommande = new Commande;
        echo "<div id=\"Formulaire\">
            <form enctype=\"multipart/form-data\" action =\"index.php?s=" . $_GET['s'] . "&amp;action=add&amp;idCommande=" . $oCommande->getIdCommande() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"idCommande\" value =\"" . $oCommande->getIdCommande() . "\">
                <input type =\"hidden\" name =\"action\" value =\"" . $_GET['action'] . "\">
                <fieldset>
                    <legend>Commande</legend>
                    
                    <label for =\"rabais\">Rabais</label>
                    <input type =\"text\" name =\"txtRabais\" id =\"rabais\" size=\"40\" required=\"required\" value =\"" . $oCommande->getRabaisCommande() . "\">
                    <br>
                    <label for =\"paiement\">Mode Paiement</label>
                    <input type =\"text\" name =\"txtPaiement\" id =\"paiement\" size=\"40\" required=\"required\" value =\"" . $oCommande->getPaimentCommande() . "\">
                    <br>
					<label for =\"livraison\">Livraison</label>
                    <input type =\"text\" name =\"txtlivraison\" id =\"livraison\" size=\"40\" required=\"required\" value =\"" . $oCommande->getLivraisonCommande() . "\">
                    <br>
					<label for =\"actif\">Actif</label>
                    <input type =\"text\" name =\"txtActif\" id =\"actif\" size=\"40\" required=\"required\" value =\"" . $oCommande->getActifCommande() . "\">
                    <br>
					
                    <div id=\"Bouton\"><input type =\"submit\" name =\"cmd\" value =\"Enregistrer\"></div></fieldset></form></div>";
    }

}

?>