<?php

/**
 * 
 * Code skeleton generated by dia-uml2php5 plugin
 * written by KDO kdo@zpmag.com
 */
class VueAccueil {

    /**
     * @access public
     * @param array aProduits 
     */
    public static function afficherPageAccueil() {
        /* --========================================================
                                  CONTENU
          ========================================================== */
        echo' 
        <section>
            <div class="container">
                <h2><em>Bienvenue</em>Chez Nous</h2>
                <div class="row">
                    <div class="grid_6">
                        <div class="img img__border"><div class="lazy-img" style="padding-bottom: 63.0282%;"><img data-src="images/page-1_img01.jpg" alt=""></div></div>
                        <p class="center indents-1">
                        Le distingué restaurant Le 2030 propose une ambiance raffinée, confortable et une équipe de professionnels avides pour vous satisfaire.
                        Nos menus changent selon les saisons de l’année pour y incorporer des produits biologiques les plus frais disponibles. Nous cherchons à équilibrer les menus traditionnels et contemporains, simples et sophistiqués, dans le but de faire plaisir à tous les gouts.
                        </p>
                    </div>
                    <div class="grid_6">
                        <div class="img img__border"><div class="lazy-img" style="padding-bottom: 63.0282%;"><img data-src="images/page-1_img02.jpg" alt=""></div></div>
                        <p class="center indents-1">
                        Si vous voulez organiser un dîner d&apos;affaires, une réunion d&apos;amis ou même une réception de mariage Le 2030 est le restaurant que vous cherchez. Avec amplement d&apos;espace, avec un menu qui peut être adapté à chaque occasion et avec notre haut niveau de service nous garantissons la réussite de votre événement.
                        </p>
                    </div>
                </div>
                
                <div class="decoration"></div>
                
            <h2><em>Notre</em>Cuisine</h2>
            </div>            
            <div class="gallery">
                <div class="gallery_col-1">
                    <a data-fancybox-group="gallery" href="images/page-1_img03_original.jpg" class="gallery_item thumb lazy-img" style="padding-bottom: 93.96551724137931%;">
                        <img data-src="images/page-1_img03.jpg" alt="">
                        <div class="gallery_overlay">
                            <div class="gallery_caption">
                                <p><em>Veau aux amandes</em></p>
                                <p>Accompagn&eacute; d&apos;une pur&eacute;e de poires, des framboises et des roquettes</p>
                            </div>
                        </div>
                    </a>
                    <a data-fancybox-group="gallery" href="images/page-1_img04_original.jpg" class="gallery_item thumb lazy-img" style="padding-bottom: 74.13793103448276%;">
                        <img data-src="images/page-1_img04.jpg" alt="">
                        <div class="gallery_overlay">
                            <div class="gallery_caption">
                                <p><em>Menu déjeuner</em></p>
                                <p>Le plus vari&eacute; de la ville</p>
                            </div>
                        </div>
                    </a>
                    <a data-fancybox-group="gallery" href="images/page-1_img05_original.jpg" class="gallery_item thumb lazy-img" style="padding-bottom: 94.6551724137931%;">
                        <img data-src="images/page-1_img05.jpg" alt="">
                        <div class="gallery_overlay">
                            <div class="gallery_caption">
                                <p><em>Risotto aux asperges</em></p>
                                <p>Accompagné de bacon, fromage parmesan et champignons</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="gallery_col-2">
                    <a data-fancybox-group="gallery" href="images/page-1_img06_original.jpg" class="gallery_item thumb lazy-img" style="padding-bottom: 52.48322147651007%;">
                        <img data-src="images/page-1_img06.jpg" alt="">
                        <div class="gallery_overlay">
                            <div class="gallery_caption">
                                <p><em>Salade méditerranéenne</em></p>
                                <p>Pr&eacute;par&eacute;e avec des ingr&eacute;dients biologiques</p>
                            </div>
                        </div>
                    </a>
                    <a data-fancybox-group="gallery" href="images/page-1_img07_original.jpg" class="gallery_item thumb lazy-img" style="padding-bottom: 55.97315436241611%;">
                        <img data-src="images/page-1_img07.jpg" alt="">
                        <div class="gallery_overlay">
                            <div class="gallery_caption">
                                <p><em>Plateau de fruits de la mer</em></p>
                                <p>Un incontournable de l&apos;été</p>
                            </div>
                        </div>
                    </a>
                    <a data-fancybox-group="gallery" href="images/page-1_img08_original.jpg" class="gallery_item thumb lazy-img" style="padding-bottom: 96.10738255033557%;">
                        <img data-src="images/page-1_img08.jpg" alt="">
                        <div class="gallery_overlay">
                            <div class="gallery_caption">
                                <p><em> Rouleaux de saumon fumé</em></p>
                                <p>Fait avec du vrai saumon norvégien</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="gallery_col-3">
                    <a data-fancybox-group="gallery" href="images/page-1_img09_original.jpg" class="gallery_item thumb lazy-img" style="padding-bottom: 93.69676320272572%;">
                        <img data-src="images/page-1_img09.jpg" alt="">
                        <div class="gallery_overlay">
                            <div class="gallery_caption">
                                <p><em>Hamburger à l&apos;agneau</em></p>
                                <p>Une expérience culinaire sophistiquée</p>
                            </div>
                        </div>
                    </a>
                    <a data-fancybox-group="gallery" href="images/page-1_img10_original.jpg" class="gallery_item thumb lazy-img" style="padding-bottom: 72.23168654173765%;">
                        <img data-src="images/page-1_img10.jpg" alt="">
                        <div class="gallery_overlay">
                            <div class="gallery_caption">
                                <p><em>Plateau de fromages</em></p>
                                <p>De toutes les variétés et de diverses nationalités</p>
                            </div>
                        </div>
                    </a>
                    <a data-fancybox-group="gallery" href="images/page-1_img11_original.jpg" class="gallery_item thumb lazy-img" style="padding-bottom: 93.69676320272572%;">
                        <img data-src="images/page-1_img11.jpg" alt="">
                        <div class="gallery_overlay">
                            <div class="gallery_caption">
                                <p><em>Fettuccine Printemps</em></p>
                                <p>Léger, santé et délicieux</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </section>
        <section class="parallax parallax1" data-parallax-speed="-0.4">
            <div class="container">
                <h2><em>Infolettre </em>Abonnez-vous!</h2>
                <p class="indents-2">
                Abonnez-vous à notre infolettre pour recevoir en primeur, a chaque mois, les derniers concours, promotions et nouveautés!
                <br/>
                Vous pourrez à tout moment vous désabonner en suivant les instructions indiquées en bas de chaque infolettre.</p>';
                VueInfolettre::afficherFormAdd();

                
            echo '   
                <!--<a href="#" class="btn">View full menu</a>-->
            </div>
            </section>';
            /* section avec les photos des cuisiniers 
        
        <section class="well well__offset-1 bg-1">
            <div class="container">
                <h2><em>Nos</em>Cuisiniers</h2>
                <div class="row row__offset-1">
                    <div class="grid_4">
                        <figure>
                            <div class="img lazy-img" style="padding-bottom: 101.0810810810811%;"><img data-src="images/page-1_img12.jpg" alt=""></div>
                            <figcaption>Kevin Grey</figcaption>
                        </figure>
                        <h3>Integer convallis orci vel mi nelaoreet, at ornare lorem consequat. Phasellus era nisl </h3>
                        <p>Vestibulum volutpat turpis ut massa commodo, quis aliquam massa facilisis.Integer convallis orci vel mi nelaoreet, at ornare lorem consequat. Phasellus era nisl auctor vel veliterol. sed,pharetra venenatis nulla.</p>
                    </div>
                    <div class="grid_4">
                        <figure>
                            <div class="img lazy-img" style="padding-bottom: 101.0810810810811%;"><img data-src="images/page-1_img13.jpg" alt=""></div>
                            <figcaption>Linda Klein</figcaption>
                        </figure>
                        <h3>Oeteger convallis orci vel mi nelaoreet, at ornare lorem consequat. Phasellus era nisl </h3>
                        <p>Vestibulum volutpat turpis ut massa commodo, quis aliquam massa facilisis.Integer convallis orci vel mi nelaoreet, at ornare lorem consequat. Phasellus era nisl auctor vel veliterol. sed,pharetra venenatis nulla.</p>
                    </div>
                    <div class="grid_4">
                        <figure>
                            <div class="img lazy-img" style="padding-bottom: 101.0810810810811%;"><img data-src="images/page-1_img14.jpg" alt=""></div>
                            <figcaption>Ann Shelton</figcaption>
                        </figure>
                        <h3>Koteger convallis orci vel mi nelaoreet, at ornare lorem consequat. Phasellus era nisl </h3>
                        <p>Vestibulum volutpat turpis ut massa commodo, quis aliquam massa facilisis.Integer convallis orci vel mi nelaoreet, at ornare lorem consequat. Phasellus era nisl auctor vel veliterol. sed,pharetra venenatis nulla.</p>
                    </div>
                </div>
                
                <div class="decoration"></div>
                
            </div>
        </section> */
        
        echo ' 
        <section class="well well__offset-2">
            <div class="container center">
                <h2><em>Réservations</h2>
                <br/>
                <p class="indents-2">
                Faites votre réservation pour les déjeuners, dîners ou événements et venez apprécier notre menu varié et excellent service.
                <br/>
                <br/>
                Nous vous attendons.
                </p>
                <address class="address-1">
                    <dl><dt>Adresse:</dt> <dd>2030, Pie IX, Montréal M0N 7R3</dd></dl>
                    <p><em>(514) 123-4567</em></p>
                </address>
                <br/>        
        
                <div id="boutonReservation">
                    <a data-type =\"submit\" name =\"cmd\" href="./index.php?s=3">Réservez ici!</a>
                </div>
            </div>
        </section>';
    }

}
?>