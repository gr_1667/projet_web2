<?php

/**
 * Class VueConnexion.class.php
 * @author Marcio Pelegrini
 * 
 * Classe responsable pour la connexion des administrateurs dans le site de gestion
 *
 * */
class VueConnexion {

    public static function connexion() {
        if (isset($_SESSION['employe'])) {
            header('Location: ../index.php');
        }
        echo '
            <div id="formLogin">
                <form name="frmConnexion" action="#" method="POST">                        
                    <label for="txtUsername">Courriel d\'usager</label>
                    <input type="email" name="txtUsername" id="txtUsername" autofocus="" size="35" required="required" placeholder="exemple@domain.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"/>
                    <label for="txtMotPasse">Mot de passe</label>
                    <input type="password" name="txtMotPasse" id="txtMotPasse" size="20" required="required"/>
                    <input type="submit" value="Connecter" id="btnEnvoyer" name="btnLogin" />               
                </form>';
        if (isset($_POST['btnLogin'])) {
            $client = new Client();
            if (!empty($_POST['txtUsername']) && !empty($_POST['txtMotPasse'])) {
                //Enlève les spaces et transforme l'usager en miniscule
                $user = strtolower(trim($_POST['txtUsername']));
                $pass = trim($_POST['txtMotPasse']);

                //Appelle la fonction Connexion qui se trouve dans fonctions.php
                if (!$client->connexionClient($user, $pass))
                    echo'<h1>Usager ou mot de passe invalide</h1>';
                else
                    header("Location: ../index.php");
            } else
                echo '<h1>Veuillez remplir les champs!</h1>';
        }
        echo '</div>';
    }
  
}
