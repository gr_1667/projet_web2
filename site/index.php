<?php
session_start();
/* Inclure les classes librairie */
require '../lib/dbConn.class.php';
require '../lib/MySqliLib.class.php';
require '../lib/TableauLib.class.php';
require '../lib/TypeException.class.php';
require '../lib/IHMLib.class.php';


/* Inclure les classes Modèles */
require '../modeles/Produit.class.php';
require '../modeles/Client.class.php';
require '../modeles/Categorie.class.php';
require '../modeles/Reservation.class.php';
require '../modeles/Table.class.php';
require '../modeles/Infolettre.class.php';
require '../modeles/Commande.class.php';


/* Inclure les classes Vues */
require 'vues/VueConnexion.class.php';
require 'vues/VueProduit.class.php';
require 'vues/VueAccueil.class.php';
require 'vues/VueCategorie.class.php';
require 'vues/VueContact.class.php';
require 'vues/VueReservation.class.php';
require 'vues/VueCommande.class.php';
require 'vues/VueHeader.class.php';
require 'vues/VueInfolettre.class.php';



/* Inclure le contrôleur */
require 'controleur/Controleur.class.php';
require 'controleur/ControleurCategories.class.php';
//require 'controleur/ControleurClients.class.php';
require 'controleur/ControleurCommandes.class.php';
//require 'controleur/ControleurEvenements.class.php';
require 'controleur/ControleurInfolettres.class.php';
require 'controleur/ControleurProduits.class.php';
require 'controleur/ControleurReservations.class.php';
//require 'controleur/ControleurTables.class.php';

require 'gabarit.php';
?>