<?php
/**
     * classe Controleur 
     * 582-N61-MA Projet Web 2
     * @author Mihai Polerca
     * @version 2015-07-02
     */
class ControleurReservations {

    public static function gererReservationsClient() {
        try {
            if (isset($_GET['action']) == false) {
                $_GET['action'] = "";
            }
            switch ($_GET['action']) {
                case "add":
                    ControleurReservations::gererAjouterReservationClient();
                    break;
                case "mod":
                    ControleurReservations::gererModifierReservationClient();
                    break;
                case "sup":
                    ControleurReservations::gererSupprimerReservationClient();
                    break;
                default:
                    ControleurReservations::gererAfficherToutesLesReservationsClient();
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }			
    }//fin de la fonction gererReservationsClient();


    /**
    * @access public
    */
    public static function gererAjouterReservationClient() {
        try {
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueReservation::afficherFormAdd();
                
            } else {

                //sinon sauvegarder
                $oReservation = new Reservation(1, trim($_POST['dateReservation']), trim($_POST['heureDebutReservation']), trim($_POST['heureFinReservation']), trim($_POST['idClientRes']), NULL, trim($_POST['iNoTable']));
//                echo trim($_POST['idClientRes']);
                $idTable = trim($_POST['iNoTable']);
                $oReservation->ajouterUneReservation($idTable);
                $sMsg = "L'enregistrement de la réservation s'est bien déroulé.";
                ControleurReservations::gererAfficherToutesLesReservationsClient($sMsg);
            }
        } catch (Exception $oExcep) {
            VueReservation::afficherFormAdd($oExcep->getMessage());
        }
    }//fin de la fonction gererAjouterReservation()

    /**
     * @access public
     */
    public static function gererModifierReservationClient() {
        try {
            if (isset($_POST['cmd']) == false) {
                $oReservation = new Reservation($_GET['noReservation']);
                $oReservation->rechercherReservation();
                //Afficher le formulaire de modification
                VueReservation::adm_afficherFormMod($oReservation);
            } else {

                $oReservation = new Reservation($_POST['noReservation'], trim($_POST['dateReservation']), trim($_POST['heureDebutReservation']), trim($_POST['heureFinReservation']), trim($_POST['idClientRes']), NULL, trim($_POST['iNoTable']));
                $oReservation->modifierUneReservation();
                $sMsg = "La modification de la réservation s'est bien déroulée.";
                ControleurReservations::gererAfficherToutesLesReservationsClient($sMsg);
            }
        } catch (Exception $oExcep) {

            VueReservation::adm_afficherFormMod($oReservation, $oExcep->getMessage());
        }
    }//fin de la fonction gererModifierReservationClient()

    /**
     * @access public
     */
    public static function gererSupprimerReservationClient() {
        try {
            $sMsg = "";
            $oReservation = new Reservation($_GET['noReservation']);
            $oReservation->supprimerUneReservation();
            if (isset($_GET['cmd']) == false) {

                //Afficher le formulaire de confirmation
                $sMsg = "Voulez-vous vraiment supprimer cette réservation ?";
                $aHiddens = array("action" => $_GET['action'], "s" => $_GET['s'], "noReservation" => $_GET['noReservation']);
                IHMLib::afficherConfirmation($sMsg, $aHiddens);
            } else {
                if ($_GET['cmd'] == "Oui") {
                    //supprimer					
                    $oReservation->supprimerUneReservation();
                    $sMsg = "La suppression de la réservation s'est bien déroulée.";
                }
                ControleurReservations::gererAfficherToutesLesReservationsClient($sMsg);
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }// fin de la fonction gererSupprimerReservationClient()

    /**
     * @access public
     */
    public static function gererAfficherToutesLesReservationsClient($sMsg = "") {
        try {
//            echo $_SESSION['idClient'];
            //Rechercher toutes les réservations
            $oReservation = new Reservation(0,NULL,NULL,NULL,$_SESSION['idClient'],NULL,NULL);
//            echo $oReservation->getIdPersonReserv();
//            var_dump($oReservation);
            $aoReservation = $oReservation->rechercherToutesLesReservationsParClient();
            
            //afficher toutes les réservations
            VueReservation::afficherToutesLesReservationsParClient($aoReservation, $sMsg);            
            
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
//            $aoReservation = $oReservation->rechercherToutesLesReservationsParClient();
            
        }
    }// fin de la fonction gererAfficherToutesLesReservationsClient()
    
    public static function gererReservationsGuest() {
        try {
            if (isset($_GET['action']) == false) {
                $_GET['action'] = "";
            }
            switch ($_GET['action']) {
                case "add":
                    ControleurReservations::gererAjouterReservationGuest();
                    break;              
                default:
                    ControleurReservations::gererAjouterReservationGuest();
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
        
    }//fin de la fonction gererReservationsClient();
    public static function gererAjouterReservationGuest() {
        try {
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueReservation::afficherFormAddGuest();
            } else {

                //sinon sauvegarder
                $db = dbConn::getConnection();
                 $sRequete = "
            INSERT INTO personnes SET                
                PERS_NOM = :pNom,
                PERS_PRENOM = :pPrenom,
                PERS_PHONE = :pPhone              
            ;";

        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        // Données
        
        $db->bindValue(":pNom",  trim($_POST['txtNomGuestReserv']));
        $db->bindValue(":pPrenom",  trim($_POST['txtPrenomGuestReserv']));        
        $db->bindValue(":pPhone",  trim($_POST['txtPhoneGuestReserv']));
        $db->execute();
        $lastId = dbConn::returnerLastId();
//        echo $lastId;
                $oReservation = new Reservation(1, trim($_POST['dateReservation']), trim($_POST['heureDebutReservation']), trim($_POST['heureFinReservation']), NULL, $lastId, trim($_POST['iNoTable']));
                $idTable = trim($_POST['iNoTable']);
                $oReservation->ajouterUneReservation($idTable);
                $sMsg = "L'ajout de la réservation s'est bien déroulé.";
                VueReservation::afficherFormAddGuest($sMsg);
            }
        } catch (Exception $oExcep) {
            VueReservation::afficherFormAddGuest($oExcep->getMessage());
        }
    }
}//fin de la classe Controleur
?>