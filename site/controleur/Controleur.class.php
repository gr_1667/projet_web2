<?php
    /**
	 * classe Controleur 
	 * 582-N61-MA Projet Web 2
	 * @author Mihai Polerca
	 * @version 2015-07-02
	 */
    class Controleur {
    	
        /**
         * gère la sélection de l'internaute
         */
         public static function gererSite(){
            try{

            if(isset($_GET['s']) == false){
                if(isset($_POST['s']) == false)
                        $_GET['s'] = "";
                else{
                        $_GET['s'] = $_POST['s'];                                        
                }
            }
            if($_GET['s']==0){
                VueHeader::afficherHeaderAccueil();
            }
            else{
                VueHeader::afficherHeaderPages();
            }

            switch($_GET['s']){

                case 0: default :
                    Controleur::gererAfficherAccueil();
                break;
                case 1:
                    ControleurCategories::gererCategories();
                break;
                case 2:
                    ControleurProduits::gererProduits();
                break;
                case 3:
                    if (isset($_SESSION['client'])) {
                        ControleurReservations::gererReservationsClient();
                    }
                    else {
                        ControleurReservations::gererReservationsGuest();
                    }                                   
                    break;       

                case 4:
                    if (isset($_SESSION['idClient'])) {
                        ControleurCommandes::gererCommandes();
                    }
                    else {
                        ControleurCommandes::gererInterdirCommandes();
                    }                                   
                    break;    
                break;

                case 5:
                    Controleur::gererAfficherContactes();
                break;
            case 6:
                ControleurInfolettres::gererInfolettre();
                break;
            }

        }catch(Exception $oExcep){
                echo "<p class=\"erreur\">".$oExcep->getMessage()."</p>";
        }
         }//fin de la fonction gererSite()
         /*
          * la fonction n'est pas complete ni optimise
          */
        public static function gererAfficherAccueil()
        {
            VueAccueil ::afficherPageAccueil();
        }//fin de la fonction gererAfficherAccueil

        public static function gererAfficherReservation()
        {
            VueReservation::afficherPageReservation();
        }//fin de la fonction gererAfficherReservation();

        public static function gererCommande()
        {
            VueCommande::afficherPageCommande();
        }//fin de la fonction gererCommande();

      public static function gererAfficherContactes()
        {
           VueContact::afficherPageContacts();
        }//fin de la fonction gererAfficherContacts();

    }//fin de la classe Controleur
?>