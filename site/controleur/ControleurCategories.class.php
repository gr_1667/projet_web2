<?php
    /**
	 * classe Controleur 
	 * 582-N61-MA Projet Web 2
	 * @author Mihai Polerca
	 * @version 2015-07-02
	 */
    class ControleurCategories {
    	                
                public static function gererCategories() {
                    try {
                        if (isset($_GET['action']) == false) {
                            $_GET['action'] = "";
                        }
                        switch ($_GET['action']) {
                            case "det":
                                ControleurProduits::afficherProduitsParCateg();
                                break;
                            case "mod":
                                ControleurCategories::gererModifierCategorie();
                                break;
                            case "sup":
                                ControleurCategories::gererSupprimerCategorie();
                                break;
                            default:
                                ControleurCategories::gererAfficherToutesLesCategories();
                        }
                    } catch (Exception $oExcep) {
                        echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
                    }			
		}//fin de la fonction gererCategories();
                
                
                public static function gererAfficherToutesLesCategories(){
                    try {
                        //Rechercher tous les categories
                        $aoCategories = Categorie::rechercherAllCategories();
                        //afficher tous les produits
                        VueCategorie::afficherPageCategories($aoCategories);
                    } catch (Exception $oExcep) {
            //Add a comment to this line
                        echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
                    }
    
                }//fin fonction ger
                
                 public static function gererReservation() {
			echo "La page avec reservation";
		}//fin de la fonction gererReservation();
                
                public static function gererCommande() {
			echo "La page avec commande en ligne";
		}//fin de la fonction gererCommande();
                
               public static function gererContactes() {
			echo "La page avec contactes";
		}//fin de la fonction gererContactes();
               
               
	}//fin de la classe Controleur
?>