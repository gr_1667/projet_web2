<?php
    /**
	 * classe Controleur 
	 * 582-N61-MA Projet Web 2
	 * @author Mihai Polerca
	 * @version 2015-07-02
	 */
    class ControleurCommandes {
    	
        public static function gererCommandes() {
            try {
                    if (isset($_GET['action']) == false) {
                        $_GET['action'] = "";
                    }
                    switch ($_GET['action']) {
                        case "aff":
                            ControleurCommandes::gererAfficherCommande();
                            break;
                        case "add":
                            ControleurCommandes::gererAjouterCommande();
                            break;
                        case "checkout":
                            ControleurCommandes::gererSupprimerCategorie();
                            break;
                        default:
                            ControleurCommandes::gererAfficherCommande();
                    }
            } catch (Exception $oExcep) {
                echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
            }			
        }//fin de la fonction gererProduits();

        public static function gererAfficherCommande(){
            try{
                $aoCommande = array ();
				$oCommande = new Commande();
				$oCommande->setClient($_SESSION['idClient']);
				
                //$oCommande->setCat_id_Produit($_GET['idCat']);
                $aoCommandes = $oCommande->rechercherCommande();
                VueCommande::afficherPageCommande($aoCommandes);
            }  catch (Exception $oExcep){
                echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
            }
        }//fin de la classe gererAfficherCommande
		
		public static function gererAjouterCommande(){
            try{
                if (isset($_POST['cmd']) == false) {
                	//Afficher le formulaire d'ajout
                	VueCommande::afficherFormCommande($sMsg = "");
            	} else {

					//sinon sauvegarder
					$oCommande = new Commande(1,NULL, trim($_POST['txtRabais']), trim($_POST['txtPaiement']), trim($_POST['txtlivraison']), trim($_POST['txtActif']), $_SESSION['idClient']);
					$oCommande->ajouterCommande();              
					$sMsg = "L'ajout de la commande - " . $oCommande->getIdCommande() . " - s'est bien déroulé.";
					ControleurCommandes::gererAjouterProduitsalaCommande($oCommande, $sMsg);
				}
            }  catch (Exception $oExcep){
                echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
            }
        }//fin de la classe gererAjouterCommande
        
        /*public static function gererAfficherToutesLesProduits(){
        try {
                //Rechercher tous les categories
                $aoProduits = Produit::rechercherAllProduits();
                //afficher tous les produits
                VueProduit::afficherPageProduits($aoProduits);
            } catch (Exception $oExcep) {
                //Add a comment to this line
                echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
            }
        }*/
		
		/**
     * @access public
     */
    public static function gererAjouterProduitsalaCommande($oCommande, $sMsg) {
        try {
//            $oCategorie = new Categorie();
//            $aOCategories = $oCategorie->rechercherAllCategories();
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueCommande::afficherFormCommande($sMsg = "");
            } else {

                //sinon sauvegarder
				$oProduits = new Produit();
                //$oCommande = new Commande(1,NULL, trim($_POST['txtRabais']), trim($_POST['txtPaiement']), trim($_POST['txtlivraison']), trim($_POST['txtActif']), trim($_POST['txtIdClient']));
                //$oCommande->ajouterCommande();              
				$sMsg = "L'ajout de la commande - " . $oCommande->getIdCommande() . " - s'est bien déroulé.";
                //ControleurCommandes::gererAfficherTousLesCommandes($sMsg);
				//ControleurProduits::gererAfficherTousLesProduits($sMsg);
                $aoProduits = $oProduits->rechercherAllProduits();
				VueProduit::afficherPageProduits($aoProduits);
            }
        } catch (Exception $oExcep) {
            VueProduit::afficherFormCommande($oExcep->getMessage());
        }
    }
	//fin de la fonction gererAjouterProduitsalaCommande();
		
		
		public static function gererInterdirCommandes(){
			echo "Il faut se registrer pour faire une commande.";
		}
    }//fin de la classe ControleurProduits
?>