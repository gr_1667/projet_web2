<?php
    /**
	 * classe Controleur 
	 * 582-N61-MA Projet Web 2
	 * @author Mihai Polerca
	 * @version 2015-07-02
	 */
    class ControleurProduits {
    	
        public static function gererProduits() {
            try {
                    if (isset($_GET['action']) == false) {
                        $_GET['action'] = "";
                    }
                    switch ($_GET['action']) {
                        case "det":
                            ControleurProduits::gererAfficherProduitsParCateg();
                            break;
                        case "reserv":
                            ControleurProduits::gererModifierCategorie();
                            break;
                        case "checkout":
                            ControleurProduits::gererSupprimerCategorie();
                            break;
                        default:
                            ControleurProduits::gererAfficherToutesLesProduits();
                    }
            } catch (Exception $oExcep) {
                echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
            }			
        }//fin de la fonction gererProduits();

        public static function gererafficherProduitsParCateg(){
            try{
                $oProduit = new Produit();
                $oProduit->setCat_id_Produit($_GET['idCat']);
                $aoProduits = $oProduit->rechercherProduitsByCategorie();
                VueProduit::afficherPageProduits($aoProduits);
            }  catch (Exception $oExcep){
                echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
            }
        }//fin de la classe ControleurProduits
        
        public static function gererAfficherToutesLesProduits(){
        try {
                //Rechercher tous les categories
                $aoProduits = Produit::rechercherAllProduits();
                //afficher tous les produits
                VueProduit::afficherPageProduits($aoProduits);
            } catch (Exception $oExcep) {
                //Add a comment to this line
                echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
            }
        }
    }//fin de la classe ControleurProduits
?>