﻿<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8"/>
    <meta name="format-detection" content="telephone=no"/>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="css/animate.css"/>
    <link rel="stylesheet" href="css/camera.css"/>
    <link rel="stylesheet" href="css/contact-form.css"/>
    <link rel="stylesheet" href="css/reservation-form.css"/>
    <link rel="stylesheet" href="css/global.css"/>  
    <link rel="stylesheet" href="css/google-map.css"/>
    <link rel="stylesheet" href="css/grid.css"/>
    <link rel="stylesheet" href="css/jquery.fancybox.css"/>
    <link rel="stylesheet" href="css/navigation.css"/>    
    <link rel="stylesheet" href="css/style.css"/>

    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>

    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a> 
    </div>
    <script src="js/html5shiv.js"></script>
    <![endif]-->
 
    <script src='js/device.min.js'></script> 
</head>

<body>
<div class="page">
    <!--========================================================
                  Chaque page a son propre HEADER
    =========================================================-->
    <?php
    
    /**************input pour login des admin*********************/
    
    //Si un administrateur n'est pas connecté, la page affiche
    //um <div> avec un formulaire de login et mot de passe.
    if (!isset($_SESSION["client"])) {
        VueConnexion::connexion();
    } else {
        echo '<div id="login">';
        echo 'Bienvenue ' . $_SESSION['client'];
        echo '<br/><a href="../site/vues/deconnexion.php">D&eacute;connexion</a>';
        echo '</div>';
    }
    ?>

    <!--========================================================
                              CONTENU
    =========================================================-->
    
    <main>
        <?php
        
            Controleur::gererSite();
        ?>
        
    </main>

    <!--========================================================
                              FOOTER
    =========================================================-->
    <footer>
        <div class="container">
            <ul class="socials">
                <li><a href="http://facebook.com" target="_blank"><img src="./images/facebook.png" /></a></li>
                <li><a href="http://twitter.com" target="_blank"><img src="./images/twitter.png" /></a></li>
                <li><a href="http://youtube.com" target="_blank"><img src="./images/youtube.png" /></a></li>
            </ul>
        </div>
        
        <div class="tm">
            <span> &COPY; Daniel Ferreira | Juan Carlos | Marcio Pelegrini | Mihai Polerca - Projet Web 2 </span>
        </div>
    </footer>
   
</div>

<script src="js/script.js"></script>
</body>
</html>