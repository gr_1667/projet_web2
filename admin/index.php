<?php
session_start();

/* Inclure les classes librairie */
require_once '../lib/MySqliLib.class.php';
require_once '../lib/TypeException.class.php';
require_once '../lib/IHMLib.class.php';
require_once '../lib/dbConn.class.php';


/* Inclure les classes Modèles */
require_once '../modeles/Administrateur.class.php';
require_once '../modeles/Categorie.class.php';
require_once '../modeles/Client.class.php';
require_once '../modeles/Commande.class.php';
require_once '../modeles/Evenement.class.php';
require_once '../modeles/Infolettre.class.php';
require_once '../modeles/Produit.class.php';
require_once '../modeles/Table.class.php';
require_once '../modeles/Reservation.class.php';

/* Inclure les classes Vues */
require_once './vues/VueConnexion.class.php';
require_once './vues/VueCommande.class.php';
require_once './vues/VueClient.class.php';
require_once './vues/VueProduit.class.php';
require_once './vues/VueEvenement.class.php';
require_once './vues/VueCategorie.class.php';
require_once './vues/VueTable.class.php';
require_once './vues/VueAdministrateur.class.php';
require_once './vues/VueInfolettre.class.php';
require_once './vues/VueReservation.class.php';

/* Inclure les contrôleurs */
require_once 'Controleur/ControleurAdmin.class.php';
require_once 'Controleur/ControleurAdministrateurs.class.php';
require_once 'Controleur/ControleurCategories.class.php';
require_once 'Controleur/ControleurClients.class.php';
require_once 'Controleur/ControleurCommandes.class.php';
require_once 'Controleur/ControleurEvenements.class.php';
require_once 'Controleur/ControleurInfolettres.class.php';
require_once 'Controleur/ControleurProduits.class.php';
require_once 'Controleur/ControleurReservations.class.php';
require_once 'Controleur/ControleurTables.class.php';

require_once 'gabarit.php';
?>