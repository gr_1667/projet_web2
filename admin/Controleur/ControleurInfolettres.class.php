<?php

/**
 * Classe controleur pour le cote Administrateur
 * @date 2015-06-30
 * @author Marcio Pelegrini - Mihai Polerca - Juan Carlos - Daniel Ferreira
 * 
 */
class ControleurInfolettres {

    /*     * ********************************************************** */
    /* Gestion des Infolettres                                    */
    /*     * ********************************************************** */

    /**
     * @access public 
     */
    public static function gererInfolettre() {
        try {
            if (isset($_GET['action']) == false) {
                $_GET['action'] = "";
            }
            switch ($_GET['action']) {
                case "add":
                    ControleurInfolettres::gererAjouterInfolettre();
                    break;
                case "sup":
                    ControleurInfolettres::gererSupprimerInfolettre();
                    break;
                default:
                    ControleurInfolettres::gererAfficherTousLesInfolettres();
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAjouterInfolettre() {
        try {
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueInfolettre::adm_afficherFormAdd();
            } else {
                //sinon sauvegarder
                $oLettre = new Infolettre(trim($_POST['txtEmailInfo']));
                $oLettre->ajouterUnCourriel();
                $sMsg = "L'ajout du courriel - " . $oLettre->getSEmailInfo() . " - s'est bien déroulé.";
                VueInfolettre::adm_afficherFormAdd($sMsg);
            }
        } catch (Exception $oExcep) {
            VueInfolettre::adm_afficherFormAdd($oExcep->getMessage());
        }
    }

    /**
     * @access public
     */
    public static function gererSupprimerInfolettre() {
        try {
            $sMsg = "";
            $oLettre = new Infolettre($_GET['email']);
            $oLettre->rechercherUnCourriel();
            if (isset($_GET['cmd']) == false) {

                //Afficher le formulaire de confirmation
                $sMsg = "Voulez-vous vraiment supprimer ce courriel - " . $oLettre->getSEmailInfo() . " - ?";
                $aHiddens = array("action" => $_GET['action'], "s" => $_GET['s'], "email" => $_GET['email']);
                IHMLib::afficherConfirmation($sMsg, $aHiddens);
            } else {
                if ($_GET['cmd'] == "Oui") {
                    //supprimer					
                    $oLettre->supprimerUnCourriel();
                    $sMsg = "La suppression du courriel - " . $oLettre->getSEmailInfo() . " - s'est bien déroulée.";
                }
                ControleurInfolettres::gererAfficherTousLesInfolettres();
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAfficherTousLesInfolettres($sMsg = "") {
        try {
            //Rechercher tous les clients
            $aoLettre = Infolettre::rechercherTousCourriels();
            //afficher tous les membres
            VueInfolettre::afficherTousLesInfolettres($aoLettre, $sMsg);
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

}
?>
