<?php

/**
 * Classe controleur pour le cote Administrateur
 * @date 2015-06-30
 * @author Marcio Pelegrini - Mihai Polerca - Juan Carlos - Daniel Ferreira
 * 
 */
class ControleurTables {

    /*     * *************************************************************** */
    /* Gestion des Tables                                          */
    /*     * *************************************************************** */

    /**
     * @access public 
     */
    public static function gererTables() {
        try {
            if (isset($_GET['action']) == false) {
                $_GET['action'] = "";
            }
            switch ($_GET['action']) {
                case "add":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurTables::gererAjouterTable();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                case "mod":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurTables::gererModifierTable();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                case "sup":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurTables::gererSupprimerTable();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                default:
                    ControleurTables::gererAfficherToutesLesTables();
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAjouterTable() {
        try {
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueTable::afficherFormAdd();
            } else {

                //sinon sauvegarder
                $oTable = new Table(1, trim($_POST['txtCapaciteTable']));
                $oTable->ajouterUneTable();
                $sMsg = "L'ajout de la table s'est bien déroulé.";
                VueTable::afficherFormAdd($sMsg);
            }
        } catch (Exception $oExcep) {
            VueTable::afficherFormAdd($oExcep->getMessage());
        }
    }

//fin de la fonction gererAjouterTable()

    /**
     * @access public
     */
    public static function gererModifierTable() {
        try {
            if (isset($_POST['cmd']) == false) {
                $oTable = new Table($_GET['idTable']);
                $oTable->rechercherTable();
                //Afficher le formulaire de modification
                VueTable::afficherFormMod($oTable);
            } else {

                $oTable = new Table($_POST['idTable'], trim($_POST['txtCapaciteTable']));
                $oTable->modifierUneTable();
                $sMsg = "La modification de la table s'est bien déroulée.";
                ControleurTables::gererAfficherToutesLesTables($sMsg);
            }
        } catch (Exception $oExcep) {
            VueTable::afficherFormMod($oTable, $oExcep->getMessage());
        }
    }

    /**
     * @access public
     */
    public static function gererSupprimerTable() {
        try {
            $sMsg = "";
            $oTable = new Table($_GET['idTable']);
            $oTable->rechercherTable();
            if (isset($_GET['cmd']) == false) {

                //Afficher le formulaire de confirmation
                $sMsg = "Voulez-vous vraiment supprimer cette table ?";
                $aHiddens = array("action" => $_GET['action'], "s" => $_GET['s'], "idTable" => $_GET['idTable']);
                IHMLib::afficherConfirmation($sMsg, $aHiddens);
            } else {
                if ($_GET['cmd'] == "Oui") {
                    //supprimer					
                    $oTable->supprimerUneTable();
                    $sMsg = "La suppression de la table s'est bien déroulée.";
                }
                ControleurTables::gererAfficherToutesLesTables($sMsg);
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAfficherToutesLesTables($sMsg = "") {
        try {
            //Rechercher toutes les tables
            $aoTables = Table::rechercherAllTables();
            //afficher toutes les tables
            VueTable::afficherToutesLesTables($aoTables, $sMsg);
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }
}
?>
