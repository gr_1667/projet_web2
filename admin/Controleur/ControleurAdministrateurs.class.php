<?php

/**
 * Classe controleur pour le cote Administrateur
 * @date 2015-06-30
 * @author Marcio Pelegrini
 * 
 */
class ControleurAdministrateurs {
    /*     * ************************************************************ */
    /* Gestion des Administrateurs                                    */
    /*     * ************************************************************ */

    /**
     * @access public 
     */
    public static function gererAdministrateurs() {
        try {
            if ((!isset($_SESSION['role'])) || ($_SESSION['role'] != 'admin')) {
                echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module</p>';
            } else {
                if (isset($_GET['action']) == false) {
                    $_GET['action'] = "";
                }
                switch ($_GET['action']) {
                    case "add":
                        ControleurAdministrateurs::gererAjouterAdministrateur();
                        break;
                    case "mod":
                        ControleurAdministrateurs::gererModifierAdministrateur();
                        break;
                    case "sup":
                        ControleurAdministrateurs::gererSupprimerAdministrateur();
                        break;
                    default:
                        ControleurAdministrateurs::gererAfficherTousLesAdministrateurs();
                }
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAjouterAdministrateur() {
        try {
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueAdministrateur::adm_afficherFormAdd();
            } else {
                if ($_POST['txtMotPasse'] == $_POST['txtMotPasse2']) {
                    //sinon sauvegarder
                    $oAdmin = new Administrateur(1, trim($_POST['txtEmail']), trim($_POST['txtNom']), trim($_POST['txtPrenom']), trim($_POST['txtMotPasse']), $_POST['txtRole']);
                    $oAdmin->ajouterUnAdministrateur();
                    $sMsg = "L'ajout de l'administrateur " . $oAdmin->getSPrenomAdm() . " " . $oAdmin->getSNomAdm() . " s'est bien déroulé.";
                    VueAdministrateur::adm_afficherFormAdd();
                } else {
                    echo "<p class=\"erreur\">Les mots de passe doivent être egals</p>";
                }
            }
        } catch (Exception $oExcep) {
            VueAdministrateur::adm_afficherFormAdd($oExcep->getMessage());
        }
    }

    /**
     * @access public
     */
    public static function gererModifierAdministrateur() {
        try {
            if (isset($_POST['cmd']) == false) {
                $oAdmin = new Administrateur($_GET['idAdm']);
                $oAdmin->rechercherUnAdministrateur();
                //Afficher le formulaire de modification
                VueAdministrateur::afficherFormMod($oAdmin);
            } else {
                $mp = trim($_POST['txtMotPasse']);
                $mp2 = trim($_POST['txtMotPasse2']);
                if ($mp == $mp2) {
                    $oAdmin = new Administrateur($_POST['idAdm'], trim($_POST['txtEmail']), trim($_POST['txtNom']), trim($_POST['txtPrenom']), trim($_POST['txtMotPasse']), $_POST['txtRole']);
                    $oAdmin->modifierUnAdministrateur();
                    $sMsg = "La modification de l'administrateur " . $oAdmin->getSPrenomAdm() . " " . $oAdmin->getSNomAdm() . " s'est bien déroulée.";
                    ControleurAdministrateurs::gererAfficherTousLesAdministrateurs($sMsg);
                } else {
                    echo "<p class=\"erreur\">Les mots de passe doivent être égals</p>";
                }
            }
        } catch (Exception $oExcep) {
            VueAdministrateur::afficherFormMod($oAdmin, $oExcep->getMessage());
        }
    }

    /**
     * @access public
     */
    public static function gererSupprimerAdministrateur() {
        try {
            $sMsg = "";
            $oAdmin = new Administrateur($_GET['idAdm']);
            $oAdmin->rechercherUnAdministrateur();
            if (isset($_GET['cmd']) == false) {

                //Afficher le formulaire de confirmation
                $sMsg = "Voulez-vous vraiment supprimer l'administrateur " . $oAdmin->getSPrenomAdm() . " " . $oAdmin->getSNomAdm() . " ?";
                $aHiddens = array("action" => $_GET['action'], "s" => $_GET['s'], "idAdm" => $_GET['idAdm']);
                IHMLib::afficherConfirmation($sMsg, $aHiddens);
            } else {
                if ($_GET['cmd'] == "Oui") {
                    //supprimer					
                    $oAdmin->supprimerUnAdministrateur();
                    $sMsg = "La suppression de l'administrateur " . $oAdmin->getSPrenomAdm() . " " . $oAdmin->getSNomAdm() . " s'est bien déroulée.";
                }
                ControleurAdministrateurs::gererAfficherTousLesAdministrateurs($sMsg);
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAfficherTousLesAdministrateurs($sMsg = "") {
        try {
            //Rechercher tous les clients
            $aoAdmin = Administrateur::rechercherTousAdministrateurs();
            //afficher tous les membres
            VueAdministrateur::afficherTousLesAdministrateurs($aoAdmin, $sMsg);
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

}

?>
