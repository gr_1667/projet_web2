<?php

/**
 * Classe controleur pour le cote Administrateur
 * @date 2015-06-30
 * @author Marcio Pelegrini - Mihai Polerca - Juan Carlos - Daniel Ferreira
 * 
 */
class ControleurProduits {
    /*     * *************************************************************** */
    /* Gestion des Produits                                        */
    /*     * ************************************************************** */

    /**
     * @access public 
     */
    public static function gererProduits() {
        try {
            if (isset($_GET['action']) == false) {
                $_GET['action'] = "";
            }
            switch ($_GET['action']) {
                case "add":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurProduits::gererAjouterProduit();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                case "mod":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurProduits::gererModifierProduit();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                case "sup":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurProduits::gererSupprimerProduit();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                default:
                    ControleurProduits::gererAfficherTousLesProduits($sMsg = "");
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

//fin fonction gererProduits

    /**
     * @access public
     */
    public static function gererAjouterProduit() {
        try {
//            $oCategorie = new Categorie();
//            $aOCategories = $oCategorie->rechercherAllCategories();
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueProduit::adm_afficherFormAdd($sMsg = "");
            } else {

                //sinon sauvegarder
                $imgProduit = $_FILES['imgProduit']['name'];
                $oProduit = new Produit(1, trim($_POST['txtNomProduit']), trim($_POST['txtDescProduit']), trim($_POST['txtPrixProduit']), $imgProduit, trim($_POST['txtOCatProduit']));
                $oProduit->setCat_id_Produit(trim($_POST['txtOCatProduit']));
                $oProduit->ajouterUnProduit();
                //Déplace les imagers ver /Produits
                $dir_images = './medias/Produits/' . $_FILES['imgProduit']['name'];
                if (move_uploaded_file($_FILES['imgProduit']['tmp_name'], $dir_images)) {
                    $sMsg = "L'ajout du produit - " . $oProduit->getNomProduit() . " - s'est bien déroulé.";
                    ControleurProduits::gererAfficherTousLesProduits($sMsg);
                }
            }
        } catch (Exception $oExcep) {
            VueProduit::adm_afficherFormAdd($oExcep->getMessage());
        }
    }

//fin de la fonction gererAjouterProduit();

    /**
     * @access public
     */
    public static function gererModifierProduit() {
        try {
            if (isset($_POST['cmd']) == false) {
                $oProduit = new Produit($_GET['idProduit']);
                $oProduit->rechercherProduit();
                //Afficher le formulaire de modification
                VueProduit::adm_afficherFormMod($oProduit);
            } else {
                $imgProduit = $_FILES['imgProduit']['name'];
                $oProduit = new Produit($_POST['idProduit'], trim($_POST['txtNomProduit']), trim($_POST['txtDescProduit']), trim($_POST['txtPrixProduit']), $imgProduit, trim($_POST['txtOCatProduit']));
                $oProduit->setCat_id_Produit(trim($_POST['txtOCatProduit']));
                $oProduit->modifierUnProduit();
                //Déplace les imagers ver /Produits
                $dir_images = './medias/Produits/' . $_FILES['imgProduit']['name'];
                if (move_uploaded_file($_FILES['imgProduit']['tmp_name'], $dir_images)) {
                    $sMsg = "La modification du produit - " . $oProduit->getNomProduit() . " - s'est bien déroulée.";
                    ControleurProduits::gererAfficherTousLesProduits($sMsg);
                }
            }
        } catch (Exception $oExcep) {

            VueProduit::adm_afficherFormMod($oProduit, $oExcep->getMessage());
        }
    }

//fin fonction gererModifierProduit();

    /**
     * @access public
     */
    public static function gererSupprimerProduit() {
        try {
            $sMsg = "";
            $oProduit = new Produit($_GET['idProduit']);
            $oProduit->rechercherProduit();
            if (isset($_GET['cmd']) == false) {

                //Afficher le formulaire de confirmation
                $sMsg = "Voulez-vous vraiment supprimer le produit - " . $oProduit->getNomProduit() . " - ?";
                $aHiddens = array("action" => $_GET['action'], "s" => $_GET['s'], "idProduit" => $_GET['idProduit']);
                IHMLib::afficherConfirmation($sMsg, $aHiddens);
            } else {
                if ($_GET['cmd'] == "Oui") {
                    //supprimer					
                    $oProduit->supprimerUnProduit();
                    $sMsg = "La suppression du produit - " . $oProduit->getNomProduit() . " - s'est bien déroulée.";
                }
                ControleurProduits::gererAfficherTousLesProduits($sMsg);
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

//fin fonction gererSupprimerProduit();

    /**
     * @access public
     */
    public static function gererAfficherTousLesProduits($sMsg = "") {
        try {
            //Rechercher tous les produits
            $aoProduits = Produit::rechercherAllProduits();
            //afficher tous les produits
            VueProduit::afficherTousLesProduits($aoProduits, $sMsg);
        } catch (Exception $oExcep) {
//Add a comment to this line
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

}

?>
