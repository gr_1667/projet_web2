<?php

/**
 * Classe controleur pour le cote Administrateur
 * @date 2015-06-30
 * @author Marcio Pelegrini - Mihai Polerca - Juan Carlos - Daniel Ferreira
 * 
 */
class ControleurReservations {
    /*     * *************************************************************** */
    /* Gestion des Réservations                                              */
    /*     * *************************************************************** */

    /**
     * @access public 
     */
    public static function gererReservations() {
        try {
            if (isset($_GET['action']) == false) {
                $_GET['action'] = "";
            }
            switch ($_GET['action']) {
                case "add":
                        ControleurReservations::gererAjouterReservation();
                    break;
                case "mod":
                        ControleurReservations::gererModifierReservation();
                    break;
                case "sup":
                        ControleurReservations::gererSupprimerReservation();
                    break;
                default:
                    ControleurReservations::gererAfficherToutesLesReservations();
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAjouterReservation() {
        try {
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueReservation::afficherFormAdd();
            } else {
                $pers =  trim($_POST['idGuestRes']);
                $clie =  trim($_POST['idClientRes']);

                if ($pers == ''){
                    $pers = NULL;
                }
                if ($clie == ''){
                    $clie = NULL;
                }
 
                //sinon sauvegarder
                $oReservation = new Reservation(1, trim($_POST['dateReservation']), trim($_POST['heureDebutReservation']), trim($_POST['heureFinReservation']), $clie, $pers, trim($_POST['iNoTable']));
                $idTable = trim($_POST['iNoTable']);
                $oReservation->ajouterUneReservation($idTable);
                $sMsg = "L'ajout de la réservation s'est bien déroulé.";
                VueReservation::afficherFormAdd($sMsg);
            }
        } catch (Exception $oExcep) {
            VueReservation::afficherFormAdd($oExcep->getMessage());
        }

    }

//fin de la fonction gererAjouterReservation()

    /**
     * @access public
     */
    public static function gererModifierReservation() {
        try {
            if (isset($_POST['cmd']) == false) {
                $oReservation = new Reservation($_GET['noReservation']);
                $oReservation->rechercherReservation();
                //Afficher le formulaire de modification
                VueReservation::adm_afficherFormMod($oReservation);
            } else {

                $oReservation2 = new Reservation($_POST['noReservation'], trim($_POST['dateReservation']), trim($_POST['heureDebutReservation']), trim($_POST['heureFinReservation']), trim($_POST['idClientRes']), trim($_POST['idGuestRes']), trim($_POST['iNoTable']));
                $oReservation2->modifierUneReservation();
                $sMsg = "La modification de la réservation s'est bien déroulée.";
                ControleurReservations::gererAfficherToutesLesReservations($sMsg);
            }
        } catch (Exception $oExcep) {

            VueReservation::adm_afficherFormMod($oReservation2, $oExcep->getMessage());
        }
    }

//fin de la fonction gererModifierReservation()

    /**
     * @access public
     */
    public static function gererSupprimerReservation() {
        try {
            $sMsg = "";
            $oReservation = new Reservation($_GET['noReservation']);
            $oReservation->supprimerUneReservation();
            if (isset($_GET['cmd']) == false) {

                //Afficher le formulaire de confirmation
                $sMsg = "Voulez-vous vraiment supprimer cette réservation ?";
                $aHiddens = array("action" => $_GET['action'], "s" => $_GET['s'], "noReservation" => $_GET['noReservation']);
                IHMLib::afficherConfirmation($sMsg, $aHiddens);
            } else {
                if ($_GET['cmd'] == "Oui") {
                    //supprimer					
                    $oReservation->supprimerUneReservation();
                    $sMsg = "La suppression de la réservation s'est bien déroulée.";
                }
                ControleurReservations::gererAfficherToutesLesReservations($sMsg);
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

// fin de la fonction gererSupprimerReservation()

    /**
     * @access public
     */
    public static function gererAfficherToutesLesReservations($sMsg = "") {
        try {
            //Rechercher toutes les réservations
            $aoReservation = Reservation::rechercherToutesLesReservations();
            //afficher toutes les réservations
            VueReservation::afficherToutesLesReservations($aoReservation, $sMsg);
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

// fin de la fonction gererAfficherToutesLesReservations()
}

?>
