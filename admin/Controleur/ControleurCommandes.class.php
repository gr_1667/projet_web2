<?php

/**
 * Classe controleur pour le cote Administrateur
 * @date 2015-06-30
 * @author Marcio Pelegrini - Mihai Polerca - Juan Carlos - Daniel Ferreira
 * 
 */
class ControleurCommandes {

	/************************************************************ */
    /* Gestion des Commandes                                    */
    /************************************************************ */

    /**
     * @access public 
     */
    public static function gererCommandes() {
        try {
            if (isset($_GET['action']) == false) {
                $_GET['action'] = "";
            }
            switch ($_GET['action']) {
                case "add":
                    ControleurCommandes::gererAjouterCommande();
                    break;
				case "mod":
                    ControleurCommandes::gererModiffierCommande();
                    break;
                case "sup":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurCommandes::gererSupprimerCommande();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
				case "aff":
                    ControleurCommandes::gererAfficherCommande();
                    break;
				case "apc":
                    ControleurCommandes::gererAjouterProduitsalaCommande();
                    break;
                default:
                    ControleurCommandes::gererAfficherTousLesCommandes();
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }
	
	/**
     * @access public
     */
    public static function gererAjouterCommande() {
        try {
//            $oCategorie = new Categorie();
//            $aOCategories = $oCategorie->rechercherAllCategories();
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueCommande::afficherFormCommande($sMsg = "");
            } else {

                //sinon sauvegarder
                $oCommande = new Commande(1,NULL, trim($_POST['txtRabais']), trim($_POST['txtPaiement']), trim($_POST['txtlivraison']), trim($_POST['txtActif']), trim($_POST['txtIdClient']));
                $oCommande->ajouterCommande();              
				$sMsg = "L'ajout de la commande - " . $oCommande->getIdCommande() . " - s'est bien déroulé.";
                ControleurCommandes::gererAjouterProduitsalaCommande($oCommande, $sMsg);
                
            }
        } catch (Exception $oExcep) {
            VueProduit::afficherFormCommande($oExcep->getMessage());
        }
    }
	//fin de la fonction gererAjouterCommande();
	
	/**
     * @access public
     */
    public static function gererModifierCommande() {
        try {
            if (isset($_POST['cmd']) == false) {
                $oCommande = new Commande($_GET['idCommande']);
                $oCommande->rechercherCommande();
                //Afficher le formulaire de modification
                VueCommande::afficherFormCommande($oCommande);
            } else {
                $oCommande = new Commande($_POST['idProduit'], trim($_POST['txtNomProduit']), trim($_POST['txtDescProduit']), trim($_POST['txtPrixProduit']), $imgProduit, trim($_POST['txtOCatProduit']));
                $oProduit ->setCat_id_Produit(trim($_POST['txtOCatProduit']));
                $oCommande->modifierUnProduit();
                  //Déplace les imagers ver /Produits
                $dir_images = './medias/Produits/' . $_FILES['imgProduit']['name'];                
                if (move_uploaded_file($_FILES['imgProduit']['tmp_name'], $dir_images)) {
                $sMsg = "La modification du produit - " . $oProduit->getNomProduit() . " - s'est bien déroulée.";
                  ControleurProduits::gererAfficherTousLesProduits($sMsg);
                }
            }
        } catch (Exception $oExcep) {

            VueProduit::adm_afficherFormMod($oProduit, $oExcep->getMessage());
        }
    }

//fin fonction gererModifierCommande();
	
	/**
     * @access public
     */
    public static function gererSupprimerCommande() {
        try {
            $sMsg = "";
            $oCommande = new Commande($_GET['idCommande']);
            $oCommande->rechercherCommande();
            if (isset($_GET['cmd']) == false) {

                //Afficher le formulaire de confirmation
                $sMsg = "Voulez-vous vraiment supprimer la commande - " . $oCommande->getIdCommande() . " - ?";
                $aHiddens = array("action" => $_GET['action'], "s" => $_GET['s'], "idCommande" => $_GET['idCommande']);
                IHMLib::afficherConfirmation($sMsg, $aHiddens);
            } else {
                if ($_GET['cmd'] == "Oui") {
                    //supprimer					
                    $oCommande->supprimerCommande();
                    $sMsg = "La suppression de la commande - " . $oCommande->getIdCommande() . " - s'est bien déroulée.";
                }
                ControleurCommandes::gererAfficherTousLesCommandes($sMsg);
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

	//fin fonction gererSupprimerCommande();
	
	/**
     * @access public
     */
    public static function gererAfficherCommande($sMsg = "") {
        try {
			$aoCommande = array ();
			$oCommande = new Commande();
			$oCommande->setIdCommande($_GET['idCommande']);
            //Rechercher la commande
            //$oCommande->rechercherCommande();
			$aoCommande = $oCommande->rechercherCommande();
            //afficher tous les membres
            //VueCommande::afficherUneCommande($oCommande, $sMsg);
			VueCommande::afficherUneCommande($aoCommande, $sMsg);
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }
	//fin fonction gererAfficherCommande();
	
	/**
     * @access public
     */
    public static function gererAfficherTousLesCommandes($sMsg = "") {
        try {
            //Rechercher tous les commandes
            $aoCommandes = Commande::rechercherAllCommandes();
            //afficher tous les membres
            VueCommande::afficherAllCommandes($aoCommandes, $sMsg);
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }
	
	
	/**
     * @access public
     */
    public static function gererAjouterProduitsalaCommande($oCommande, $sMsg) {
        try {
//            $oCategorie = new Categorie();
//            $aOCategories = $oCategorie->rechercherAllCategories();
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueCommande::afficherFormCommande($sMsg = "");
            } else {

                //sinon sauvegarder
				$oProduits = new Produits();
                //$oCommande = new Commande(1,NULL, trim($_POST['txtRabais']), trim($_POST['txtPaiement']), trim($_POST['txtlivraison']), trim($_POST['txtActif']), trim($_POST['txtIdClient']));
                //$oCommande->ajouterCommande();              
				$sMsg = "L'ajout de la commande - " . $oCommande->getIdCommande() . " - s'est bien déroulé.";
                //ControleurCommandes::gererAfficherTousLesCommandes($sMsg);
				//ControleurProduits::gererAfficherTousLesProduits($sMsg);
                $aoProduits = $oProduits->rechercherAllProduits();
            }
        } catch (Exception $oExcep) {
            VueProduit::afficherFormCommande($oExcep->getMessage());
        }
    }
	//fin de la fonction gererAjouterProduitsalaCommande();

}
?>
