<?php

/**
 * Classe controleur pour le cote Administrateur
 * @date 2015-06-30
 * @author Marcio Pelegrini - Mihai Polerca - Juan Carlos - Daniel Ferreira
 * 
 */
class ControleurClients {
    /*     * *************************************************************** */
    /* Gestion des Clients                                            */
    /*     * ************************************************************** */

    /**
     * @access public 
     */
    public static function gererClients() {
        try {
            if (isset($_GET['action']) == false) {
                $_GET['action'] = "";
            }
            switch ($_GET['action']) {
                case "add":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurClients::gererAjouterClient();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                case "mod":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurClients::gererModifierClient();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                case "sup":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurClients::gererSupprimerClient();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                default:
                    ControleurClients::gererAfficherTousLesClients();
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAjouterClient() {
        try {
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueClient::adm_afficherFormAdd();
            } else {
                if ($_POST['txtMotPasseClient'] == $_POST['txtMotPasseClient2']) {
                    //sinon sauvegarder
                    $oClient = new Client(1, trim($_POST['txtEmailClient']), trim($_POST['txtNomClient']), trim($_POST['txtPrenomClient']), trim($_POST['txtAdresseClient']), trim($_POST['txtVilleClient']), trim($_POST['txtCodePostalClient']), trim($_POST['txtPhoneClient']), trim($_POST['txtMotPasseClient']), '1', trim($_POST['txtPointsClient']));
                    $oClient->ajouterUnClient();
                    $sMsg = "L'ajout du client - " . $oClient->getSNomClient() . " - s'est bien déroulé.";
                    VueClient::adm_afficherFormAdd($sMsg);
                } else {
                    echo "<p class=\"erreur\">Les mots de passe doivent être egals</p>";
                }
            }
        } catch (Exception $oExcep) {
            VueClient::adm_afficherFormAdd($oExcep->getMessage());
        }
    }

//fin de la fonction gererAjouterGroupe()

    /**
     * @access public
     */
    public static function gererModifierClient() {
        try {
            if (isset($_POST['cmd']) == false) {
                $oClient = new Client($_GET['idClient']);
                $oClient->rechercherClient();
                //Afficher le formulaire de modification
                VueClient::adm_afficherFormMod($oClient);
            } else {
                $mp = trim($_POST['txtMotPasseClient']);
                $mp2 = trim($_POST['txtMotPasseClient2']);
                if ($mp == $mp2) {
                    $oClient = new Client($_POST['idClient'], trim($_POST['txtEmailClient']), trim($_POST['txtNomClient']), trim($_POST['txtPrenomClient']), trim($_POST['txtAdresseClient']), trim($_POST['txtVilleClient']), trim($_POST['txtCodePostalClient']), trim($_POST['txtPhoneClient']), $mp, '1', trim($_POST['txtPointsClient']));
                    $oClient->modifierUnClient();
                    $sMsg = "La modification du client - " . $oClient->getSNomClient() . " - s'est bien déroulée.";
                    ControleurClients::gererAfficherTousLesClients($sMsg);
                } else {
                    echo "<p class=\"erreur\">Les mots de passe doivent être égals</p>";
                }
            }
        } catch (Exception $oExcep) {
            /*   $oClient = new Membre($_GET['idGroupe']);
              $oGroupe->rechercher(); */
            VueClient::adm_afficherFormMod($oClient, $oExcep->getMessage());
        }
    }

    /**
     * @access public
     */
    public static function gererSupprimerClient() {
        try {
            $sMsg = "";
            $oClient = new Client($_GET['idClient']);
            $oClient->rechercherClient();
            if (isset($_GET['cmd']) == false) {

                //Afficher le formulaire de confirmation
                $sMsg = "Voulez-vous vraiment supprimer ce client - " . $oClient->getSNomClient() . " - ?";
                $aHiddens = array("action" => $_GET['action'], "s" => $_GET['s'], "idClient" => $_GET['idClient']);
                IHMLib::afficherConfirmation($sMsg, $aHiddens);
            } else {
                if ($_GET['cmd'] == "Oui") {
                    //supprimer					
                    $oClient->supprimerUnClient();
                    $sMsg = "La suppression du client - " . $oClient->getSNomClient() . " - s'est bien déroulée.";
                }
                ControleurClients::gererAfficherTousLesClients($sMsg);
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAfficherTousLesClients($sMsg = "") {
        try {
            //Rechercher tous les clients
            $aoClient = Client::rechercherTousClients();
            //afficher tous les membres
            VueClient::afficherTousLesClients($aoClient, $sMsg);
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

}

?>
