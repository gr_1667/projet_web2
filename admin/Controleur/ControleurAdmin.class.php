<?php

/**
 * Classe controleur pour le cote Administrateur
 * @date 2015-06-30
 * @author Marcio Pelegrini - Mihai Polerca - Juan Carlos - Daniel Ferreira
 * 
 */

class ControleurAdmin {
   

    /**
     * @access public
     */
    public static function gererSite() {
        try {
            if (isset($_GET['s']) == false) {
                $_GET['s'] = 0;
            }
            switch ($_GET['s']) {
                case 1:
                    ControleurClients::gererClients();
                    break;
                case 2:
                    ControleurProduits::gererProduits();
                    break;
                case 3:
                    ControleurCategories::gererCategories();
                    break;
                case 4:
                    ControleurCommandes::gererCommandes();
                    break;
                case 5:
                    ControleurEvenements::gererEvenements();
                    break;
                case 6:
                    ControleurTables::gererTables();
                    break;
                case 7:
                    ControleurReservations::gererReservations();
                    break;
                case 8:
                    ControleurAdministrateurs::gererAdministrateurs();
                    break;
                case 9:
                    ControleurInfolettres::gererInfolettre();
                    break;
                case 10:
                    header("Location: ../Documentation/html/index.html");
                    break;

                case 0: default :
                    echo "<h2>Bienvenue: " . $_SESSION['administrateur'] . "</h2>";
                    echo "<p>Veuillez choisir l'une des option du menu en haut.</p>";
                    echo '<img src="../admin/medias/Le2030.png" alt=""/>';

                    break;
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }
}


?>
