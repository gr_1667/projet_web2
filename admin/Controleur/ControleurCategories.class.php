<?php

/**
 * Classe controleur pour le cote Administrateur
 * @date 2015-06-30
 * @author Marcio Pelegrini - Mihai - Juan Carlos - Daniel Ferreira
 * 
 */
class ControleurCategories {
    /*     * *************************************************************** */
    /* Gestion des Categories                                          */
    /*     * *************************************************************** */

    /**
     * @access public 
     */
    public static function gererCategories() {
        try {
            if (isset($_GET['action']) == false) {
                $_GET['action'] = "";
            }
            switch ($_GET['action']) {
                case "add":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurCategories::gererAjouterCategorie();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                case "mod":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurCategories::gererModifierCategorie();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                case "sup":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurCategories::gererSupprimerCategorie();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                default:
                    ControleurCategories::gererAfficherToutesLesCategories();
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAjouterCategorie() {
        try {
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueCategorie::afficherFormAdd();
            } else {

                //sinon sauvegarder
                $imgCategorie = $_FILES['imgCategorie']['name'];
                $oCategorie = new Categorie(1, trim($_POST['txtNomCategorie']), trim($_POST['txtDescCategorie']), $imgCategorie);
                $oCategorie->ajouterUneCategorie();
                $dir_images = './medias/Categories/' . $_FILES['imgCategorie']['name'];
                if (move_uploaded_file($_FILES['imgCategorie']['tmp_name'], $dir_images)) {
                    $sMsg = "L'ajout de la catégorie s'est bien déroulé.";
                    VueCategorie::afficherFormAdd($sMsg);
                }
            }
        } catch (Exception $oExcep) {
            VueCategorie::afficherFormAdd($oExcep->getMessage());
        }
    }

//fin de la fonction adm_gererAjouterCategorie()

    /**
     * @access public
     */
    public static function gererModifierCategorie() {
        try {
            if (isset($_POST['cmd']) == false) {
                $oCategorie = new Categorie($_GET['idCategorie']);
                $oCategorie->rechercherCategorie();
                //Afficher le formulaire de modification
                VueCategorie::afficherFormMod($oCategorie);
            } else {
                $imgCategorie = $_FILES['imgCategorie']['name'];
                $oCategorie2 = new Categorie($_POST['idCategorie'], trim($_POST['txtNomCategorie']), trim($_POST['txtDescCategorie']), $imgCategorie);
                $oCategorie2->modifierUneCategorie();
//                var_dump($oCategorie2);
                $dir_images = './medias/Categories/' . $_FILES['imgCategorie']['name'];
                if (move_uploaded_file($_FILES['imgCategorie']['tmp_name'], $dir_images)) {
                    $sMsg = "La modification de la catégorie s'est bien déroulée.";
                    ControleurCategories::gererAfficherToutesLesCategories();
                }
            }
        } catch (Exception $oExcep) {
            /*   $oClient = new Membre($_GET['idGroupe']);
              $oGroupe->rechercher(); */
            VueCategorie::afficherFormMod($oCategorie, $oExcep->getMessage());
        }
    }

    /**
     * @access public
     */
    public static function gererSupprimerCategorie() {
        try {
            $sMsg = "";
            $oCategorie = new Categorie($_GET['idCategorie']);
            $oCategorie->rechercherCategorie();
            if (isset($_GET['cmd']) == false) {

                //Afficher le formulaire de confirmation
                $sMsg = "Voulez-vous vraiment supprimer cette catégorie ?";
                $aHiddens = array("action" => $_GET['action'], "s" => $_GET['s'], "idCategorie" => $_GET['idCategorie']);
                IHMLib::afficherConfirmation($sMsg, $aHiddens);
            } else {
                if ($_GET['cmd'] == "Oui") {
                    //supprimer					
                    $oCategorie->supprimerUneCategorie();
                    $sMsg = "La suppression de la catégorie s'est bien déroulée.";
                }
                ControleurCategories::gererAfficherToutesLesCategories($sMsg);
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function gererAfficherToutesLesCategories($sMsg = "") {
        try {
            //Rechercher toutes les catégories
            $aoCategorie = Categorie::rechercherAllCategories();
            //afficher toutes les catégories
            VueCategorie::afficherToutesLesCategories($aoCategorie, $sMsg);
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

}

?>
