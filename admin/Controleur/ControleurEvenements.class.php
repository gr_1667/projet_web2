<?php

/**
 * Classe controleur pour le cote Administrateur
 * @date 2015-06-30
 * @author Marcio Pelegrini - Mihai Polerca - Juan Carlos - Daniel Ferreira
 * 
 */
class ControleurEvenements {
    /*     * *************************************************************** */
    /* Gestion des Événements                                          */
    /*     * *************************************************************** */

    /**
     * @access public 
     */
    public static function gererEvenements() {
        try {
            if (isset($_GET['action']) == false) {
                $_GET['action'] = "";
            }
            switch ($_GET['action']) {
                case "add":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurEvenements::adm_gererAjouterEvenement();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                case "mod":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurEvenements::adm_gererModifierEvenement();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                case "sup":
                    if (isset($_SESSION['role']) && ($_SESSION['role'] == 'admin'))
                        ControleurEvenements::adm_gererSupprimerEvenement();
                    else
                        echo '<p class="erreur">Vous n\'avez pas l\'autorization pour acceder ce module.</p>';
                    break;
                default:
                    ControleurEvenements::adm_gererAfficherTousLesEvenements();
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function adm_gererAjouterEvenement() {
        try {
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueEvenement::adm_afficherFormAdd();
            } else {

                //sinon sauvegarder
                $nom_image = trim($_FILES['imgEvenement']['name']);
                $oCategorie = new Evenement(1, trim($_POST['txtDescriptEvent']), $nom_image, trim($_POST['dateEvenement']));
                $oCategorie->ajouterUnEvenement();

                //Déplace les imagers ver /Evenements
                $dir_images = './medias/Evenements/' . $_FILES['imgEvenement']['name'];
                if (move_uploaded_file($_FILES['imgEvenement']['tmp_name'], $dir_images)) {
                    $sMsg = "L'ajout de l'événement s'est bien déroulé.";
                    VueEvenement::adm_afficherFormAdd($sMsg);
                }
            }
        } catch (Exception $oExcep) {
            VueEvenement::adm_afficherFormAdd($oExcep->getMessage());
        }
    }

//fin de la fonction gererAjouterEvenement()

    /**
     * @access public
     */
    public static function adm_gererModifierEvenement() {
        try {
            if (isset($_POST['cmd']) == false) {
                $oCategorie = new Evenement($_GET['noEvenement']);
                $oCategorie->rechercherEvenement();
                //Afficher le formulaire de modification
                VueEvenement::adm_afficherFormMod($oCategorie);
            } else {
                $nom_image = trim($_FILES['imgEvenement']['name']);
                $oCategorie = new Evenement($_POST['noEvenement'], trim($_POST['txtDescriptEvent']), $nom_image, trim($_POST['dateEvenement']));
                $oCategorie->modifierUnEvenement();
                //Déplace les imagers ver /Evenements
                $dir_images = './medias/Evenements/' . $_FILES['imgEvenement']['name'];
                if (move_uploaded_file($_FILES['imgEvenement']['tmp_name'], $dir_images)) {
                    $sMsg = "La modification de l'événement s'est bien déroulée.";
                    ControleurEvenements::adm_gererAfficherTousLesEvenements($sMsg);
                }
            }
        } catch (Exception $oExcep) {
            /*   $oClient = new Membre($_GET['idGroupe']);
              $oGroupe->rechercher(); */
            VueEvenement::adm_afficherFormMod($oCategorie, $oExcep->getMessage());
        }
    }

    /**
     * @access public
     */
    public static function adm_gererSupprimerEvenement() {
        try {
            $sMsg = "";
            $oCategorie = new Evenement($_GET['noEvenement']);
            $oCategorie->rechercherEvenement();
            if (isset($_GET['cmd']) == false) {

                //Afficher le formulaire de confirmation
                $sMsg = "Voulez-vous vraiment supprimer cet événement ?";
                $aHiddens = array("action" => $_GET['action'], "s" => $_GET['s'], "noEvenement" => $_GET['noEvenement']);
                IHMLib::afficherConfirmation($sMsg, $aHiddens);
            } else {
                if ($_GET['cmd'] == "Oui") {
                    //supprimer					
                    $oCategorie->supprimerUnEvenement();
                    $sMsg = "La suppression de lévénement s'est bien déroulée.";
                }
                ControleurEvenements::adm_gererAfficherTousLesEvenements($sMsg);
            }
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

    /**
     * @access public
     */
    public static function adm_gererAfficherTousLesEvenements($sMsg = "") {
        try {
            //Rechercher tous les événements
            $aoEvenement = Evenement::rechercherTousLesEvenements();
            //afficher tous les événements
            VueEvenement::adm_afficherTousLesEvenements($aoEvenement, $sMsg);
        } catch (Exception $oExcep) {
            echo "<p class=\"erreur\">" . $oExcep->getMessage() . "</p>";
        }
    }

}

?>
