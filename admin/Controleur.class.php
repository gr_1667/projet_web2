<?php
    
    /**
	 * classe Controleur 
	 * 582-P41-MA Programmation Web Dynamique 3
	 * @author Caroline Martin
	 * @version 2015-03-03
	 */
    class Controleur {
    	
		/**
		 * gère la sélection de l'internaute
		 */
		public static function gererSite(){
			try{
				if(isset($_GET['s']) == false){
					$_GET['s'] = 0;
				}
				switch($_GET['s']){
					
					case 1 ://gerer tout les Chansons
						Controleur::gererChansons();
						break;
					
					case 0: default ://Accueil
						echo "Bienvenue sur le site d'administration.";		
				}
			}catch(Exception $oExcep){
				echo "<p class=\"erreur\">".$oExcep->getMessage()."</p>";
			}
		}//fin de la fonction gereSite()
		
		
		/* *************************************************************************************** */
		/* Gestion des groupes */
		/* *************************************************************************************** */
		/**
		 * @access public
		 */

		public static function gererChansons() {
			try{
				if(isset($_GET['action']) == false){
					$_GET['action'] = "";
				}
				switch($_GET['action']){
					case "add":
						Controleur::gererAjouterUneChanson();
						break;					
					case "mod":
						Controleur::gererModifierUneChanson();
						break;
					case "sup":
						Controleur::gererSupprimerUneChanson();
						break;				
					default:
						Controleur::gererAfficherTousLesChansons();
				}
				
			}catch(Exception $oExcep){
				echo "<p class=\"erreur\">".$oExcep->getMessage()."</p>";
			}
		}//fin de la fonction gererChansons()
		
		/**
	 * @access public
	 */
	 
	public static function gererAfficherTousLesChansons($sMsg=""){
		try{
			//Rechercher tous les chansons
			$aoSongs = Song::rechercherAllSongs();
			
			//afficher tous les membres
			VueSong::adm_afficherToutesLesChansons($aoSongs,$sMsg="");
				//echo "<p class=\"erreur\">".$oExcep->getMessage()."</p>"; 
				//print_r($aoSongs);
		
		}catch(Exception $oExcep){
			VueSong::adm_afficherTousLesChansons($oExcep->getMessage());
		}
	}//fin de la fonction gererAfficherTousLesGroupes()

	
	/**
	 * @access public
	 */
	
	public static function gererAjouterUneChanson(){
		try{
			if(isset($_POST['cmd']) == false){
				//Afficher le formulaire d'ajout
				VueSong::adm_afficherFormAjoutChanson($sMsg="");
			}else{
				//sinon sauvegarder
				$oSong = new Song(1, trim($_POST['txtUrlSong']),trim($_POST['txtTitreSong']), NULL);
				$oSong->ajouterUneSong();
				$sMsg = "L'ajout du chansons - ".$oSong->getTitreSong()." - s'est bien déroulé.";
				$aoSongs = Song::rechercherAllSongs();
				VueSong::adm_afficherToutesLesChansons($aoSongs,$sMsg);
			}
		}catch(Exception $oExcep){
			VueSong::adm_afficherFormAjoutChanson($oExcep->getMessage());
		}
	}//fin de la fonction gererAjouterUneChanson()
	
	/**
	 * @access public
	 */
	
	public static function gererModifierUneChanson(){
		try{
			if(isset($_POST['cmd']) == false){
				$oSong = new Song($_GET['idSong']);
				$oSong->rechercherSong();
				//Afficher le formulaire de modification
				VueSong::adm_afficherFormModificationChanson($oSong, $sMsg="");
			}else{
				//echo $_POST['idSong'];
				//sinon sauvegarder
				$idSong = $_POST['idSong'];
				$oSong = new Song($idSong,$_POST['txtUrlSong'], trim($_POST['txtTitreSong']));
				$oSong->modifierUneSong();
				$sMsg = "La modification du chanson - ".$oSong->getTitreSong()." - s'est bien déroulée.";
				//Controleur::gererAfficherTousLesChansons($sMsg);
				$aoSongs = Song::rechercherAllSongs();
				VueSong::adm_afficherToutesLesChansons($aoSongs,$sMsg);
			}
		}catch(Exception $oExcep){
				$oSong = new Song($_POST['idSong']);
				$oSong->rechercherSong();
				//Afficher le formulaire de modification
				VueSong::adm_afficherFormModificationChanson($oSong, $oExcep->getMessage());
			//echo "<p class=\"erreur\">".$oExcep->getMessage()."</p>";
		}
	}//fin de la fonction gererModifierUneChanson()
	
	/**
	 * @access public
	 */
	 
	public static function gererSupprimerUneChanson(){
		try{
			$sMsg="";
			$oSong = new Song($_GET['idSong']);
			$oSong->rechercherSong();
			if(isset($_GET['cmd']) == false){
	
				//Afficher le formulaire de confirmation
				$sMsg ="Voulez-vous vraiment supprimer la chanson - ".$oSong->getTitreSong()." - ?";
				$aHiddens = array("action"=>$_GET['action'], "s"=>$_GET['s'], "idSong"=>$_GET['idSong']);
				VueMessage::afficherMsgConfirmation($sMsg, $aHiddens);
				
			}else{
				if($_GET['cmd']=="Oui"){
					//supprimer					
					$oSong->supprimerUneSong();
					$sMsg = "La suppression du chanson - ".$oSong->getTitreSong()." - s'est bien déroulée.";
					Controleur::gererAfficherTousLesChansons($sMsg);
				}
				
			}
		}catch(Exception $oExcep){
			echo "<p class=\"erreur\">".$oExcep->getMessage()."</p>";
		}
	}//fin de la fonction gererSupprimerUneChanson()
		
		
		
		static public function gererActions()
		{
			try{
				switch($_GET['action']){
					case "add":
						//Si c'est à l'arrivée 
						if(isset($_POST['cmd'])==false){							
							//afficher le form d'ajout
							VueSong::adm_afficherFormAjoutChanson($sMsg="");
							
						}else{//Sinon
							//Sauvegarder la base de données
							$oSong= new Song ($_POST['sUrlSong'], $_POST['sTitreSong']);
							$iLignes = $oSong->ajouterUneSong();
							$sMsg = "L'ajout de - " .$oSong->getTitreSong()." s'est déroulé avec succès - Numéro de inséré : ".$iLignes;
							Controleur::gererAfficherTousLesChansonsT($sMsg);
						}
						break;
					case "mod":
					
						//Si c'est à l'arrivée 
						if(isset($_POST['cmd'])==false){
							//$oSong = new Song($_GET['idSong']);
							//$oSong->rechercherUneSong();
							//afficher le form de modification
							VueSong::adm_afficherFormModificationChanson($oSong, $sMsg="");
							
						}else{//Sinon
							//Sauvegarder la base de données
							$oSong= new Song ($_POST['sUrlSong'], $_POST['sTitreSong']);
							$iLignes = $oSong->modifierUneSong();
							$sMsg = "La modification de - " .$oSong->getTitreSong()." s'est déroulé avec succès - Numéro de modifie : ".$iLignes;
								Controleur::gererAfficherTousLesChansonsT($sMsg);
						}
						break;
					case "sup":						
						if(isset($_GET['cmd'])==false){
							$oSong = new Song($_GET['idSong']);
							$oSong->rechercherUneSong();							
							$sMsg = "Voulez-vous vraiment supprimer la chanson - ".$oSong->getTitreSong()." - ?";
							$aHidden = array("s"=>$_GET['s'], "idSong"=>$_GET['idSong'], "action"=>$_GET['action']);
							//$sMsg = "La suppression de - " .$oSonng->getTitre()." - s'est déroulée avec succès";
							VueMessage::afficherMsgConfirmation($sMsg, $aHidden);	
						}else{							
							if($_GET['cmd'] == "Oui"){
								$iLignes = $oSong->supprimerUneSong();
								$sMsg = "La suppression de - " .$oSonng->getTitre()." - s'est déroulée avec succès";
								Controleur::gererAfficherTousLesChansonsT($sMsg);								
							}							
						}
						
						break;
					default:
						echo "défaut";
						break;
				}
				
				
			}catch(Exception $oExcep){
				echo "<p class=\"erreur\">".$oExcep->getMessage()."</p>";
			}
		}//fin de la fonction gererActions()
		
    }//fin de la classe Controleur
?>