<?php

/**
 * Class VueConnexion.class.php
 * @author Marcio Pelegrini
 * 
 * Classe responsable pour la connexion des administrateurs dans le site de gestion
 *
 * */
class VueConnexion {

    public static function connexion() {
        if (isset($_SESSION['employe'])) {
            header('Location: index.php');
        }
        echo '
            <div id="Formulaire">
                <form name="frmConnexion" action="#" method="POST">
                    <fieldset>
                        <legend>Connexion </legend>
                        <label for="txtUsername">Courriel d\'usager</label>
                        <input type="email" name="txtUsername" id="txtUsername" autofocus="" size="50" required="required" placeholder="exemple@domain.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"/><br/>
                        <label for="txtMotPasse">Mot de passe</label>
                        <input type="password" name="txtMotPasse" id="txtMotPasse" size="20" required="required"/><br/>
                        <div id="Bouton">
                            <input type="submit" value="Connecter" id="btnEnvoyer" name="btnLogin" />
                        </div>
                        <h3>Monsieur Faïçal, pour accéder le système vous pouvez utilisez
                            l\'usager et mot de passe suivante: <br/>
                            Pour le niveau Admin - Accés total<br/>
                                    Usager: bob@bob.com<br/>
                                    Mot de passe: Bob123<br/>
                                    Pour le niveau Commun -> Employé ordinaire<br/>
                                    Usager: toto@toto.com<br/>
                                    Mot de passe: Toto123<br/>
                            Merci<br/>
                        </h3>
                    </fieldset>
                </form>';
        if (isset($_POST['btnLogin'])) {
            $adm = new Administrateur();
            if (!empty($_POST['txtUsername']) && !empty($_POST['txtMotPasse'])) {
                //Enlève les spaces et transforme l'usager en miniscule
                $user = strtolower(trim($_POST['txtUsername']));
                $pass = trim($_POST['txtMotPasse']);

                //Appelle la fonction Connexion qui se trouve dans fonctions.php
                if (!$adm->connexionAdministrateur($user, $pass))
                    echo'<h1>Usager ou mot de passe invalide</h1>';
                else
                    header("Location: ./index.php");
            } else
                echo '<h1>Veuillez remplir les champs!</h1>';
        }
        echo '</div>';
    }
  
}
