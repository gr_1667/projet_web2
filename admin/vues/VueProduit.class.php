<?php

/**
 * classe VueProduit
 * @author Mihai Polerca
 * @version 2015-06-7
 */
class VueProduit {

    /**
     * afficher le formulaire de saisie
     * @param string $sMsg 
     */
    public static function afficherFormRechercheUnProduitParNom($sMsg = "") {
        echo "
            <form action =\"index.php?s=" . $_GET['s'] . "&amp;action=\"search\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <fieldset>
                    <label for =\"gr\">Nom produit</label>
                    <input type =\"text\" name =\"txtNomProduit\" id =\"gr\" value =\"\">
                    <br>

                    <input type =\"submit\" name =\"cmd\" value =\"Rechercher\">
                </fieldset>
            </form>
        ";
    }

//fin de la fonction afficherFormRechercheUnProduitParNom()

    /**
     * afficher le groupe
     * @param Groupe $oProduit
     */
    public static function afficherUnProduit(Produit $oProduit) {
        echo "
            <h2>Produit</h2>
            <dl>
                <dt>" . $oProduit->getIdProduit() . "</dt>
                <dd>" . $oProduit->getNomProduit() . "</dd>
                <dd>" . $oProduit->getDescProduit() . "</dd>
                <dd>" . $oProduit->getPrixProduit() . "</dd>
                <dd>" . $oProduit->getImageProduit() . "</dd>
                <dd>" . $oProduit->getCat_id_Produit() . "</dd>
            <dl>
            ";
    }

    /**
     * @access public
     * @param string $sMsg 
     */
    public static function adm_afficherFormAdd($sMsg = "") {
        $oProduit = new Produit(0, " ", " ", " ", " ", " ", 1);

        VueProduit::adm_afficherFormMod($oProduit, $sMsg);
    }

    /**
     * @access public
     * @param Produit $oProduit
     * @param string $sMsg 
     */
    public static function adm_afficherFormMod(Produit $oProduit, $sMsg = "") {
        echo "<div id=\"Formulaire\">
            <form enctype=\"multipart/form-data\" action =\"index.php?s=" . $_GET['s'] . "&amp;action=" . $_GET['action'] . "&amp;idProduit=" . $oProduit->getIdProduit() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"idProduit\" value =\"" . $oProduit->getIdProduit() . "\">
                <input type =\"hidden\" name =\"action\" value =\"" . $_GET['action'] . "\">
                <fieldset>
                    <legend>Produit</legend>
                    <label for =\"idp\">Id</label>
                    <input type =\"text\" readonly autofocus=\"\" name =\"txtIdProduit\" id =\"np\" size=\"40\" required=\"required\" value =\"" . $oProduit->getIdProduit() . "\">
                    <br>
                    <label for =\"np\">Nom</label>
                    <input type =\"text\" name =\"txtNomProduit\" id =\"np\" size=\"40\" required=\"required\" value =\"" . $oProduit->getNomProduit() . "\">
                    <br>
                    <label for =\"dp\">Description</label>
                    <textarea name =\"txtDescProduit\" id =\"dp\" rows=\"5\" cols=\"3\" required=\"required\" value =\"\">" . $oProduit->getDescProduit() . "</textarea>
                    <br>
                    <label for =\"pp\">Prix</label>
                    <input type =\"text\" name =\"txtPrixProduit\" id =\"pp\" size=\"40\" required=\"required\" value =\"" . $oProduit->getPrixProduit() . "\">
                    <br>
                    <label for =\"ip\">Image</label>
                    <input type =\"file\" name =\"imgProduit\" id =\"ip\" size=\"40\" required=\"required\" value =\"" . $oProduit->getImageProduit() . "\">
                    <br>
                    <label for =\"cp\">Categorie Produit</label>
                    <select id =\"cp\" name =\"txtOCatProduit\">";
        $oCategorie = new Categorie(0, " ", " ", " ", "j");
        $aOCategories = $oCategorie->rechercherAllCategories();

        foreach ($aOCategories as $oCategorie) {
            echo '<option value="' . $oCategorie->getIdCategorie() . '">' . $oCategorie->getNomCategorie() . '</option>';
        }
        echo "</select> <br><div id=\"Bouton\"><input type =\"submit\" name =\"cmd\" value =\"Enregistrer\"></div></fieldset></form></div>";
    }

    /**
     * @access public
     * @param array $aoGpes
     */
    public static function afficherTousLesProduits($aoProduits, $sMsg = "") {
        //$aoProduits = new Produit();
        $sClasse = "";

        if ($sMsg != "&nbsp;" && trim($sMsg) != "") {
            $sClasse = "class=\"msg\"";
        }
        echo "<h2>Gérer les produits</h2>";
        if ($_SESSION['role'] == 'admin') {
            echo "<h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=add\" title=\"ajouter un produit\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter un nouveau produit</span></a></h3>
		<p  style=\"height:5px;\" " . $sClasse . ">" . $sMsg . "</p>";
        }
        echo "<table>
                    <tr>
                        <th>ID</th>
                        <th>Nom</th>
                        <th>Description</th>
                        <th>Prix</th>
                        <th>Image</th>
                        <th>Categorie</th>";
        if ($_SESSION['role'] == 'admin')
            echo "<th colspan=\"2\">Actions</th>";
        echo "</tr>";
        if (count($aoProduits) <= 0) {
            echo "
                <tr>
                    <td colspan=\"3\">Aucun produit n'existe actuellement. Veuillez en ajouter un.</td>
                </tr>";
        } else {
            for ($i = 0; $i < count($aoProduits); $i++) {
                $oCategorie = new Categorie($aoProduits[$i]->getCat_id_Produit());
                $oCategorie->rechercherCategorie();
                echo "
                    <tr>
                        <td>" . $aoProduits[$i]->getIdProduit() . "</td>
                        <td>" . $aoProduits[$i]->getNomProduit() . "</td>
                        <td>" . $aoProduits[$i]->getDescProduit() . "</td>
                        <td>" . $aoProduits[$i]->getPrixProduit() . "</td>
                        <td><img class=\"images_table\"src=\"./medias/Produits/" . $aoProduits[$i]->getImageProduit() . "\" alt=\"" . $aoProduits[$i]->getNomProduit() . "\"></td>
                        <td>" . $oCategorie->getNomCategorie() . "</td>";
                if ($_SESSION['role'] == 'admin') {
                    echo "<td><a href=\"index.php?s=" . $_GET['s'] .
                    "&amp;action=mod&amp;idProduit=" . $aoProduits[$i]->getIdProduit() . "\" title=\"modifier un produit\"><img src=\"./medias/edit_24.png\"></a></td>
                        <td><a href=\"index.php?s=" . $_GET['s'] .
                    "&amp;action=sup&amp;idProduit=" . $aoProduits[$i]->getIdProduit() . "\" title=\"supprimer un produit\"><img src=\"./medias/delete_24.png\"></a></td>";
                }
                echo "</tr>";
            }
        }
        echo "</table>";
    }

//fin de la fonction adm_afficherTousLesProduits()
}

//fin de la classe VueProduit
?>