<?php

/**
 * classe VueEvenement
 * @author Daniel Ferreira
 * @version 2015-07-02
 */
class VueEvenement {

    /**
     * @access public
     * @param string $sMsg 
     */
    public static function adm_afficherFormAdd($sMsg = "") {
        $oEvenement = new Evenement(0, " ", " ", " ", " ");
        VueEvenement::adm_afficherFormMod($oEvenement, $sMsg);
    }

//fin de la fonction adm_afficherFormAdd()

    /**
     * @access public
     * @param $oEvenement
     * @param string $sMsg 
     */
    public static function adm_afficherFormMod(Evenement $oEvenement, $sMsg = "") {
        echo "<div id=\"Formulaire\">
            <form enctype=\"multipart/form-data\" action =\"index.php?s=" . $_GET['s'] . "&amp;action=" . $_GET['action'] . "&amp;noEvenement=" . $oEvenement->getNoEvenement() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"noEvenement\" value =\"" . $oEvenement->getNoEvenement() . "\">
                <input type =\"hidden\" name =\"action\" value =\"" . $_GET['action'] . "\">
                <fieldset>
                    <legend>Événement</legend>
                    <label for =\"de\">Description</label>
                    <input type =\"text\" name =\"txtDescriptEvent\" size=\"50\" id =\"de\" required=\"required\" autofocus=\"\" value =\"" . $oEvenement->getDescriptEvent() . "\">
                    <br>
                    
                    <label for =\"imgEvenement\">Image</label>
                    <input type =\"file\" name =\"imgEvenement\" id =\"imgEvenement\" size=\"40\" required=\"required\" value =\"" . $oEvenement->getImgEvenement() . "\">
                    <br>
                    
                    <label for =\"de\">Date</label>
                    <input type =\"date\" name =\"dateEvenement\" id =\"de\" size=\"40\" required=\"required\" value =\"" . $oEvenement->getDateEvenement() . "\">
                    <br>
                   
                    <div id=\"Bouton\">
                        <input type =\"submit\" name =\"cmd\" value =\"Enregistrer\">
                    </div>
                </fieldset>
            </form></div>
        ";
    }

// fin de la fonction adm_afficherFormMod

    /**
     * @access public
     * @param array $aoEvenements
     */
    public static function adm_afficherTousLesEvenements($aoEvenements, $sMsg = "") {

        $sClasse = "";

        if ($sMsg != "&nbsp;" && trim($sMsg) != "") {
            $sClasse = "class=\"msg\"";
        }
        echo "<h2>Gérer les événements</h2>";
        if ($_SESSION['role'] == 'admin') {
            echo "<h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=add\" title=\"ajouter un evenement\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter un nouveau événement</span></a></h3>
		<p  style=\"height:5px;\" " . $sClasse . ">" . $sMsg . "</p>";
        }
        echo "<table>
                    <tr>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Date</th>";
        if ($_SESSION['role'] == 'admin')
            echo "<th colspan=\"2\">Actions</th>";
        echo "</tr>";
        if (count($aoEvenements) <= 0) {
            echo "
                <tr>
                    <td colspan=\"3\">Aucun événement n'existe actuellement. Veuillez en ajouter un.</td>
                </tr>";
        } else {
            for ($i = 0; $i < count($aoEvenements); $i++) {
                echo "
                    <tr>
                        <td><img class=\"images_table\" src=\"./medias/Evenements/" . $aoEvenements[$i]->getImgEvenement() . "\" alt=\"\"/></td>
                        <td>" . $aoEvenements[$i]->getDescriptEvent() . "</td>
                        <td>" . $aoEvenements[$i]->getDateEvenement() . "</td>";
                if ($_SESSION['role'] == 'admin') {
                    echo "<td><a href=\"index.php?s=" . $_GET['s'] .
                    "&amp;action=mod&amp;noEvenement=" . $aoEvenements[$i]->getNoEvenement() . "\" title=\"modifier un evenement\"><img src=\"./medias/edit_24.png\"></a></td>
                        <td><a href=\"index.php?s=" . $_GET['s'] .
                    "&amp;action=sup&amp;noEvenement=" . $aoEvenements[$i]->getNoEvenement() . "\" title=\"supprimer un evenement\"><img src=\"./medias/delete_24.png\"></a></td>";
                }
                echo "</tr>";
            }
        }
        echo "</table>";
    }

//fin de la fonction adm_afficherTousLesEvenements()
}

//fin de la classe VueGroupe
?>