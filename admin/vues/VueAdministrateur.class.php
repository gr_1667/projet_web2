<?php

/**
 * classe VueAdministrateur
 * @author Marcio Pelegrini
 * @version 2015-07-02
 */
class VueAdministrateur {

    /**
     * @access public
     * @param string $sMsg 
     */
    public static function adm_afficherFormAdd($sMsg = "") {
        $oAdm = new Administrateur(0, " ", " ", " ", " ", " ");
        VueAdministrateur::afficherFormMod($oAdm, $sMsg);
    }

    /**
     * @access public
     * @param Administrateur $oAdm
     * @param string $sMsg 
     */
    public static function afficherFormMod(Administrateur $oAdm, $sMsg = "") {
        echo "<div id=\"Formulaire\">
            <form action =\"index.php?s=" . $_GET['s'] . "&amp;action=" . $_GET['action'] . "&amp;idAdm=" . $oAdm->getIdAdm() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"idAdm\" value =\"" . $oAdm->getIdAdm() . "\">
                <input type =\"hidden\" name =\"action\" value =\"" . $_GET['action'] . "\">
                <fieldset>
                    <legend>Administrateur</legend>
                    <label for =\"cc\">Courriel</label>
                    <input type =\"email\" name =\"txtEmail\" size=\"50\" id =\"cc\" required=\"required\" autofocus=\"\" value =\"" . $oAdm->getSEmailAdm() . "\">
                    <br>
                    <label for =\"nc\">Nom</label>
                    <input type =\"text\" name =\"txtNom\" id =\"nc\" size=\"40\" required=\"required\" value =\"" . $oAdm->getSNomAdm() . "\">
                    <br>
                    <label for =\"pc\">Prénom</label>
                    <input type =\"text\" name =\"txtPrenom\" id =\"pc\" size=\"40\" required=\"required\" value =\"" . $oAdm->getSPrenomAdm() . "\">
                    <br>
                    <label for =\"tpr\">Type de rôle</label>
                    <select id =\"tpr\" name =\"txtRole\">
                      <option value=\"employe\">Employé</option>
                      <option value=\"admin\">Administrateur</option>
                    </select>
                    <br>
                    <label for =\"mp1\">Mot de passe</label>
                    <input type =\"password\" name =\"txtMotPasse\" id =\"mp1\" required=\"required\" size=\"20\" value =\"\">
                    <br>
                    <label for =\"mp2\">Confirmation</label>
                    <input type =\"password\" name =\"txtMotPasse2\" id =\"mp2\" required=\"required\" size=\"20\" value =\"\">
                    <br><br>
                    <div id=\"Bouton\">
                        <input type =\"submit\" name =\"cmd\" value =\"Enregistrer\">
                    </div>
                </fieldset>
            </form></div>
        ";
    }

    /**
     * @access public
     * @param array $aoAdm
     */
    public static function afficherTousLesAdministrateurs($aoAdm, $sMsg = "") {
        //$aoAdm = new Administrateur();
        $sClasse = "";

        if ($sMsg != "&nbsp;" && trim($sMsg) != "") {
            $sClasse = "class=\"msg\"";
        }
        echo "
		<h2>Gérer les administrateurs</h2>
                <h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=add\" title=\"ajouter un administrateur\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter un nouveau administrateur</span></a></h3>
		<p  style=\"height:5px;\" " . $sClasse . ">" . $sMsg . "</p>
		<table>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Courriel</th>
                        <th>Rôle</th>
                        <th colspan=\"2\">Actions</th>
                    </tr>
		";
        if (count($aoAdm) <= 0) {
            echo "
                <tr>
                    <td colspan=\"3\">Aucun administrateur n'existe actuellement. Veuillez en ajouter un.</td>
                </tr>";
        } else {
            for ($i = 0; $i < count($aoAdm); $i++) {
                echo "
                    <tr>
                        <td>" . $aoAdm[$i]->getSNomAdm() . "</td>
                        <td>" . $aoAdm[$i]->getSPrenomAdm() . "</td>
                        <td>" . $aoAdm[$i]->getSEmailAdm() . "</td>
                        <td>" . $aoAdm[$i]->getRoleAdm() . "</td>
                        <td><a href=\"index.php?s=" . $_GET['s'] .
                "&amp;action=mod&amp;idAdm=" . $aoAdm[$i]->getIdAdm() . "\" title=\"modifier un administrateur\"><img src=\"./medias/edit_24.png\"></a></td>
                        <td><a href=\"index.php?s=" . $_GET['s'] .
                "&amp;action=sup&amp;idAdm=" . $aoAdm[$i]->getIdAdm() . "\" title=\"supprimer un administrateur\"><img src=\"./medias/delete_24.png\"></a></td>
                    </tr>
                ";
            }
        }
        echo "</table>";
    }

//fin de la fonction adm_afficherTousLesGroupes()
}

//fin de la classe VueAdministrateur
?>