<?php

/**
 * classe VueClient
 * @author Marcio Pelegrini
 * @version 2015-06-30
 */
class VueClient {

    /**
     * afficher le formulaire de saisie
     * @param string $sMsg 
     */
    public static function afficherFormRechercheUnClientParNom($sMsg = "") {
        echo "
            <form action =\"index.php?s=" . $_GET['s'] . "&amp;action=\"search\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <fieldset>
                    <label for =\"gr\">Nom client</label>
                    <input type =\"text\" name =\"txtNomClient\" id =\"gr\" value =\"\">
                    <br>

                    <input type =\"submit\" name =\"cmd\" value =\"Rechercher\">
                </fieldset>
            </form>
        ";
    }

//fin de la fonction afficherFormClient()

    /**
     * afficher le groupe
     * @param Groupe $oClient
     */
    public static function afficherUnClient(Client $oClient) {
        echo "
            <h2>Client</h2>
            <dl>
                <dt>" . $oClient->getIdClient() . "</dt>
                <dd>" . $oClient->getSNomClient() . "</dd>
            <dl>
            ";
    }

    /**
     * @access public
     * @param string $sMsg 
     */
    public static function adm_afficherFormAdd($sMsg = "") {
        $oClient = new Client(0, " ", " ", " ", " ");
        VueClient::adm_afficherFormMod($oClient, $sMsg);
    }

    /**
     * @access public
     * @param Client $oClient
     * @param string $sMsg 
     */
    public static function adm_afficherFormMod(Client $oClient, $sMsg = "") {
        echo "<div id=\"Formulaire\">
            <form action =\"index.php?s=" . $_GET['s'] . "&amp;action=" . $_GET['action'] . "&amp;idClient=" . $oClient->getIdClient() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"idClient\" value =\"" . $oClient->getIdClient() . "\">
                <input type =\"hidden\" name =\"action\" value =\"" . $_GET['action'] . "\">
                <fieldset>
                    <legend>Client</legend>
                    <label for =\"cc\">Courriel</label>
                    <input type =\"email\" name =\"txtEmailClient\" autofocus=\"\" size=\"50\" id =\"cc\" required=\"required\" value =\"" . $oClient->getSEmailClient() . "\">
                    <br>
                    <label for =\"nc\">Nom</label>
                    <input type =\"text\" name =\"txtNomClient\" id =\"nc\" size=\"40\" required=\"required\" value =\"" . $oClient->getSNomClient() . "\">
                    <br>
                    <label for =\"pc\">Prénom</label>
                    <input type =\"text\" name =\"txtPrenomClient\" id =\"pc\" size=\"40\" required=\"required\" value =\"" . $oClient->getSPrenomClient() . "\">
                    <br>
                    <label for =\"ac\">Adresse</label>
                    <input type =\"text\" name =\"txtAdresseClient\" id =\"ac\" size=\"50\" required=\"required\" value =\"" . $oClient->getSAdresseClient() . "\">
                    <br>
                    <label for =\"vc\">Ville</label>
                    <input type =\"text\" name =\"txtVilleClient\" id =\"vc\" size=\"40\" required=\"required\" value =\"" . $oClient->getSVilleClient() . "\">
                    <br>
                    <label for =\"cp\">Code postal</label>
                    <input type =\"text\" name =\"txtCodePostalClient\" id =\"cp\" size=\"20\" pattern=\"^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$\" required=\"required\" value =\"" . $oClient->getSCodePostalClient() . "\"> Ex. H1H 1H1
                    <br>
                    <label for =\"phone\">Phone</label>
                    <input type =\"text\" name =\"txtPhoneClient\" id =\"phone\" size=\"20\" required=\"required\" pattern=\"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$\" value =\"" . $oClient->getSPhoneClient() . "\"> Ex. (514) 999-9999
                    <br>
                    <label for =\"mp1\">Mot de passe</label>
                    <input type =\"password\" name =\"txtMotPasseClient\" id =\"mp1\" required=\"required\" size=\"20\" value =\"\">
                    <br>
                    <label for =\"mp2\">Confirmation</label>
                    <input type =\"password\" name =\"txtMotPasseClient2\" id =\"mp2\" required=\"required\" size=\"20\" value =\"\">
                    <br>
                    <label for =\"points\">Points</label>
                    <input type =\"text\" name =\"txtPointsClient\" id =\"points\" required=\"required\" size=\"15\" pattern = \"[0-9]{1,9}\" value =\"" . $oClient->getIPointsClient() . "\">
                    <br>
                    <div id=\"Bouton\">
                        <input type =\"submit\" name =\"cmd\" value =\"Enregistrer\">
                    </div>
                </fieldset>
            </form></div>
        ";
    }

    /**
     * @access public
     * @param array $aoGpes
     */
    public static function afficherTousLesClients($aoClients, $sMsg = "") {
        //$aoClients = new Client();
        $sClasse = "";

        if ($sMsg != "&nbsp;" && trim($sMsg) != "") {
            $sClasse = "class=\"msg\"";
        }
        echo "<h2>Gérer les clients</h2>";
        if ($_SESSION['role'] == 'admin') {
            echo "
                <h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=add\" title=\"ajouter un client\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter un nouveau client</span></a></h3>
		<p  style=\"height:5px;\" " . $sClasse . ">" . $sMsg . "</p>";
        }
        echo "<table>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Courriel</th>
                        <th>Phone</th>
             ";
        if ($_SESSION['role'] == 'admin')
            echo "<th colspan=\"2\">Actions</th>";
        echo "</tr>";
        if (count($aoClients) <= 0) {
            echo "
                <tr>
                    <td colspan=\"3\">Aucun client n'existe actuellement. Veuillez en ajouter un.</td>
                </tr>";
        } else {
            for ($i = 0; $i < count($aoClients); $i++) {
                echo "
                    <tr>
                        <td>" . $aoClients[$i]->getSNomClient() . "</td>
                        <td>" . $aoClients[$i]->getSPrenomClient() . "</td>
                        <td>" . $aoClients[$i]->getSEmailClient() . "</td>
                        <td>" . $aoClients[$i]->getSPhoneClient() . "</td>";
                if ($_SESSION['role'] == 'admin') {
                    echo "<td><a href=\"index.php?s=" . $_GET['s'] .
                    "&amp;action=mod&amp;idClient=" . $aoClients[$i]->getIdClient() . "\" title=\"modifier un client\"><img src=\"./medias/edit_24.png\"></a></td>
                        <td><a href=\"index.php?s=" . $_GET['s'] .
                    "&amp;action=sup&amp;idClient=" . $aoClients[$i]->getIdClient() . "\" title=\"supprimer un client\"><img src=\"./medias/delete_24.png\"></a></td>";
                }
                echo "</tr>";
            }
        }
        echo "</table>";
    }

//fin de la fonction adm_afficherTousLesGroupes()
}

//fin de la classe VueClient
?>