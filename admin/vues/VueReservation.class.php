<?php

/**
 * classe VueReservation
 * @author Daniel Ferreira
 * @version 2015-07-06
 */
class VueReservation
{
    /**
     * @access public
     * @param string $sMsg 
     */
    public static function afficherFormAdd($sMsg = "") {
        $oReservation = new Reservation(0, NULL,NULL,NULL,NULL,NULL,NULL);        
        VueReservation::adm_afficherFormMod($oReservation, $sMsg);

}//fin de la fonction afficherFormAdd()

    /**
     * @access public
     * @param $oReservation
     * @param string $sMsg 
     */
    public static function adm_afficherFormMod(Reservation $oReservation, $sMsg = "") {
        echo "<div id=\"Formulaire\">
            <form action =\"index.php?s=" . $_GET['s'] . "&amp;action=" . $_GET['action'] . "&amp;noReservation=" . $oReservation->getNoReserv() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"noReservation\" value =\"" . $oReservation->getNoReserv() . "\">
                <input type =\"hidden\" name =\"action\" value =\"" . $_GET['action'] . "\">
                <fieldset>
                    <legend>Réservations</legend>
                    <label for =\"dr\">Date</label>
                    <input type =\"date\" name =\"dateReservation\" id =\"dr\" size=\"40\" required=\"required\" autofocus=\"\" value =\"" . $oReservation->getDateReserv() . "\">
                    <br>
                    
                    <label for =\"hdr\">Heure de début</label>
                    <input type =\"time\" name =\"heureDebutReservation\" id =\"hdr\" size=\"40\" required=\"required\" value =\"" . $oReservation->getHeureDebutReserv() . "\">
                    <br>
                    
                    <label for =\"hfr\">Heure de fin</label>
                    <input type =\"time\" name =\"heureFinReservation\" id =\"hfr\" size=\"40\" required=\"required\" value =\"" . $oReservation->getHeureFinReserv() . "\">
                    <br>
                                       
                    <label for =\"cr\">Id du Client</label>
                       <select id =\"cr\" name =\"idClientRes\">
                            <option value=\"\"></option>";
                            $oClient = new Client();
                            $aoClients = $oClient ->rechercherTousClients();
                            foreach($aoClients as $oClient)
                            {
                                echo '<option value="'.$oClient->getIdClient().'">'.$oClient->getIdClient().'</option>';
                            }
                        echo "</select>
                    <br>                    
                    <label for =\"gr\">Id du guest</label>
                       <select id =\"gr\" name =\"idGuestRes\">
                            <option value=\"\"></option>";
                            $db=  dbConn::getConnection();
                            $sRequete= "SELECT * FROM personnes";
                            $aPersonnes = dbConn::recuperer($sRequete);                           
                            foreach($aPersonnes as $Personne)
                            {
                                echo '<option value="'.$Personne['PERS_ID'].'">'.$Personne['PERS_ID'].'</option>';
                            }
                        echo "</select>
                    <br>
                    
                    <label for =\"tr\">Table</label>
                    <select id =\"tr\" name =\"iNoTable\">";
                    $oTable = new Table(0, " ");
                    $aoTable = $oTable ->rechercherAllTables();
                    foreach($aoTable as $oTable)
                    {
                        echo '<option value="'.$oTable->getIdTable().'">'.$oTable->getIdTable().'</option>';
                    }
                    echo "</select>
                    <br>
                   
                    <div id=\"Bouton\">
                        <input type =\"submit\" name =\"cmd\" value =\"Enregistrer\">
                    </div>
                </fieldset>
            </form></div>
        ";
        
    }// fin de la fonction adm_afficherFormMod

    /**
     * @access public
     * @param array $aoReservations
     */
    public static function afficherToutesLesReservations($aoReservations, $sMsg = "") {
        
        $sClasse = "";

        if ($sMsg != "&nbsp;" && trim($sMsg) != "") {
            $sClasse = "class=\"msg\"";
        }
        echo "
		<h2>Gérer les réservations</h2>
                <h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=add\" title=\"ajouter une reservation\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter une nouvelle réservation</span></a></h3>
		<p  style=\"height:5px;\" " . $sClasse . ">" . $sMsg . "</p>
		<table>
                    <tr>
                        <th>Date</th>
                        <th>Début</th>
                        <th>Fin</th>                        
                        <th>Id Client</th>
                        <th>Id Guest</th>
                        <th>Table</th>
                        <th colspan=\"2\">Actions</th>
                    </tr>
		";
        if (count($aoReservations) <= 0) {
            echo "
                <tr>
                    <td colspan=\"8\">Aucune réservation n'existe actuellement. Veuillez en ajouter une.</td>
                </tr>";
        } else {
            for ($i = 0; $i < count($aoReservations); $i++) {
                echo "
                    <tr>
                        <td>" . $aoReservations[$i]->getDateReserv() . "</td>
                        <td>" . $aoReservations[$i]->getHeureDebutReserv() . "</td>
                        <td>" . $aoReservations[$i]->getHeureFinReserv() . "</td>
                        <td>" . $aoReservations[$i]->getIdClientReserv() ."</td>
                        <td>" . $aoReservations[$i]->getIdPersonReserv() ."</td>
                        <td>" . $aoReservations[$i]->getNoTable() . "</td>
                        <td><a href=\"index.php?s=" . $_GET['s'] . 
                            "&amp;action=mod&amp;noReservation=" . $aoReservations[$i]->getNoReserv() . "\" title=\"modifier une reservation\"><img src=\"./medias/edit_24.png\"></a></td>
                        <td><a href=\"index.php?s=" . $_GET['s'] . 
                            "&amp;action=sup&amp;noReservation=" . $aoReservations[$i]->getNoReserv() . "\" title=\"supprimer une reservation\"><img src=\"./medias/delete_24.png\"></a></td>
                    </tr>
                ";
            }
        }
        echo "</table>";
    } //fin de la fonction afficherToutesLesReservations()
    
} //fin de la classe VueGroupe
?>