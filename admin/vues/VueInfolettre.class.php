<?php

/**
 * classe VueInfolettre
 * @author Marcio Pelegrini
 * @version 2015-07-02
 */
class VueInfolettre {

    /**
     * @access public
     * @param string $sMsg 
     */
    public static function adm_afficherFormAdd($sMsg = "") {
        $oLettre = new Infolettre(" ");
        VueInfolettre::afficherFormMod($oLettre, $sMsg);
    }
    
    /**
     * @access public
     * @param Infolettre $oLettre
     * @param string $sMsg 
     */
    public static function afficherFormMod(Infolettre $oLettre, $sMsg = "") {
        echo "<div id=\"Formulaire\">
            <form action =\"index.php?s=" . $_GET['s'] . "&amp;action=" . $_GET['action'] . "&amp;email=" . $oLettre->getSEmailInfo() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"email\" value =\"" . $oLettre->getSEmailInfo() . "\">
                <input type =\"hidden\" name =\"action\" value =\"" . $_GET['action'] . "\">
                <fieldset>
                    <legend>Infolettre</legend>
                    <label for =\"cc\">Courriel</label>
                    <input type =\"email\" autofocus=\"\" name =\"txtEmailInfo\" size=\"50\" id =\"cc\" required=\"required\" value =\"" . $oLettre->getSEmailInfo(). "\">
                    <br>
                    
                    <div id=\"Bouton\">
                        <input type =\"submit\" name =\"cmd\" value =\"Enregistrer\">
                    </div>
                </fieldset>
            </form></div>
        ";
    }

    /**
     * @access public
     * @param array $aoLettre
     */
    public static function afficherTousLesInfolettres($aoLettre, $sMsg = "") {
        //$aoLettre = new Infolettre();
        $sClasse = "";

        if ($sMsg != "&nbsp;" && trim($sMsg) != "") {
            $sClasse = "class=\"msg\"";
        }
        echo "
		<h2>Gérer les infolettres</h2>
                <h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=add\" title=\"ajouter un courriel\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter un nouveau courriel</span></a></h3>
		<p  style=\"height:5px;\" " . $sClasse . ">" . $sMsg . "</p>
		<table>
                    <tr>
                        <th>Courriel</th>
                        <th>Actions</th>
                    </tr>
		";
        if (count($aoLettre) <= 0) {
            echo "
                <tr>
                    <td colspan=\"3\">Aucun courriel n'existe actuellement. Veuillez en ajouter un.</td>
                </tr>";
        } else {
            for ($i = 0; $i < count($aoLettre); $i++) {
                echo "
                    <tr>
                        <td>" . $aoLettre[$i]->getSEmailInfo() . "</td>
                        <td><a href=\"index.php?s=" . $_GET['s'] .
                "&amp;action=sup&amp;email=" . $aoLettre[$i]->getSEmailInfo() . "\" title=\"supprimer un courriel\"><img src=\"./medias/delete_24.png\"></a></td>
                    </tr>
                ";
            }
        }
        echo "</table>";
    } 
} //fin de la classe VueInfolettre
?>