<?php

/**
 * 
 * Code skeleton generated by dia-uml2php5 plugin
 * written by KDO kdo@zpmag.com
 */
class VueMessage {

    /**
     * @access public
     * @param string $sMsg 

     */
    public static function afficherMsgConfirmation($sMsg = "", $aHiddens = array()) {

        echo "
		<p>" . $sMsg . "</p>
		<form action=\"index.php\" method=\"get\" >
			<fieldset>
			<legend>Confirmation</legend>
		";
        foreach ($aHiddens as $sCle => $sValue) {
            echo "
				<input type=\"hidden\" name=\"" . $sCle . "\" value=\"" . $sValue . "\">
		";
        }
        echo "	
				<input type=\"submit\" name=\"cmd\" value=\"Oui\">
				<input type=\"submit\" name=\"cmd\" value=\"Non\">
			</fieldset>
		</form>
		";
    }

}
?>