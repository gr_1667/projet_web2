<?php

	/**
	 * classe VueCommande 
	 * 582-N61-MA Projet Web 2
	 * @author Caroline Martin - Juan Carlos Nino
	 * @version 2015-06-29
	 */
    class VueCommande {

		 /**
		 * afficher le formulaire de saisie
		 * @param string $sMsg 
		 */
		public static function afficherFormRechercheUneCommandeParID($sMsg = "") {
			echo "
				<form action =\"index.php?s=" . $_GET['s'] . "&amp;action=\"search\" method =\"post\" >
					<p class=\"erreur\">" . $sMsg . "</p>
					<fieldset>
						<label for =\"gr\">Id de la Commande</label>
						<input type =\"text\" name =\"txtIdCommande\" id =\"gr\" value =\"\">
						<br>
	
						<input type =\"submit\" name =\"cmd\" value =\"Rechercher\">
					</fieldset>
				</form>
			";
		} //fin de la fonction afficherFormRechercheUnProduitParNom()
		 
		 /**
		 * afficher Une Commande
		 * @param Commande $oCommande 
		 */
		 //public static function afficherUneCommande(Commande $oCommande){
		public static function afficherUneCommande($aCommande){
			echo "
		 		<h3>Commande:</h3>
		 		<table>
					<tr>
						<th>ID</th>
						<th>Date</th>
						<th>Rabais</th>
						<th>Mode de Paiement</th>
						<th>Livraison</th>
						<th>Actif</th>
					</tr>
					<tr>
						<td>" .$aCommande[0]['COM_ID'] ."</td>
						<td>" .$aCommande[0]['COM_DATE']. "</td>
						<td>" .$aCommande[0]['COM_RABAIS']. "</td>
						<td>" .$aCommande[0]['COM_MOD_PAIEMENT']. "</td>
						<td>" .$aCommande[0]['COM_LIVRAISON']. "</td>
						<td>" .$aCommande[0]['COM_ACTIF']. "</td>
					</tr>
				</table>
				<br>
				<h3>Produits:</h3>
				<br>
				<table>
					<tr>
						<th>Produit ID</th>
						<th>Produit nom</th>
						<th>Produit Image</th>
						<th>Quantité</th>
					</tr>";
					for($i=0; $i < count($aCommande); $i++) {
					echo "
					<tr>
						<td>".$aCommande[$i]['PROD_ID']."</td>
						<td>".$aCommande[$i]['PROD_NOM']."</td>
						<td><img class=\"images_table\"src=\"./medias/Produits/".$aCommande[$i]['PROD_IMAGE']."\" alt=\"".$aCommande[$i]['PROD_IMAGE']."\"></td>
						<td>".$aCommande[$i]['PROD_QTT']."</td>
					</tr>";
					}
				echo "
				</table>
		 	";
			
			var_dump($aCommande);
		 }//fin de la fonction afficherUneCommande()
		 
		 
		/**
		 * afficher Tous les Commandes
		 * @param array $aoCommandes 
		 */	 
		public static  function afficherAllCommandes($aoCommandes, $sMsg = "") {
			
			$sClasse = "";
			if ($sMsg != "&nbsp;" && trim($sMsg) != "") {
				$sClasse = "class=\"msg\"";
			}
			echo "
			<h2>Gérer les Commandes</h2>
					<h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=add\" title=\"ajouter une Commande\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter une nouvelle commande</span></a></h3>
			<p  style=\"height:5px;\" " . $sClasse . ">" . $sMsg . "</p>
			<table>
						<tr>
							<th>ID</th>
							<th>Date</th>
							<th>Rabais</th>
							<th>Mode de Paiement</th>
							<th>Livraisont</th>
							<th>Actif</th>
							<th>Client</th>
							<th colspan=\"2\">Actions</th>
						</tr>
			";
			if (count($aoCommandes) <= 0) {
				echo "
					<tr>
						<td colspan=\"3\">Aucune commande n'existe actuellement. Veuillez en ajouter une.</td>
					</tr>";
			} else {
				for ($i = 0; $i < count($aoCommandes); $i++) {
					//$oProduit = new Produit($aoCommandes[$i]->getCat_id_Produit()); 
					//$oProduit ->rechercherProduit();
					echo "
						<tr>
							<td>" . $aoCommandes[$i]->getIdCommande() . "</td>
							<td>" . $aoCommandes[$i]->getDateCommande() . "</td>
							<td>" . $aoCommandes[$i]->getRabaisCommande() . "</td>
							<td>" . $aoCommandes[$i]->getPaimentCommande() . "</td>
							<td>" . $aoCommandes[$i]->getLivraisonCommande() . "</td>
							<td>" . $aoCommandes[$i]->getActifCommande() . "</td>
							<td>" . $aoCommandes[$i]->getClient() . "</td>
							<td><a href=\"index.php?s=" . $_GET['s'] . 
								"&amp;action=aff&amp;idCommande=" . $aoCommandes[$i]->getIdCommande() . "\" title=\"modifier une commande\"><img src=\"./medias/view.png\"></a></td>";
                                        if ($_SESSION['role'] == 'admin'){
							echo "<td><a href=\"index.php?s=" . $_GET['s'] . 
								"&amp;action=sup&amp;idCommande=" . $aoCommandes[$i]->getIdCommande() . "\" title=\"supprimer une commande\"><img src=\"./medias/delete_24.png\"></a></td>";
                                                                    }
						echo "</tr>";
				}
			}
			echo "</table>";
		} //fin de la fonction adm_afficherAllCommandes()
			
		/**
		 * afficher Tous les Produits
		 * @param array $aoCommandes 
		 */	 
		public static  function afficherAllProduits($aoCommandes, $sMsg = "") {
			
			$sClasse = "";
			if ($sMsg != "&nbsp;" && trim($sMsg) != "") {
				$sClasse = "class=\"msg\"";
			}
			echo "
			<h2>Ajouter Produits a la Commande</h2>
					<h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=apc\" title=\"ajouter produit a la Commande\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter un produit a la commande</span></a></h3>
			<p  style=\"height:5px;\" " . $sClasse . ">" . $sMsg . "</p>
			<table>
						<tr>
							<th>ID</th>
                        	<th>Nom</th>
                        	<th>Description</th>
                        	<th>Prix</th>
                        	<th>Image</th>
                        	<th>Categorie</th>
                        	<th colspan=\"2\">Actions</th>
						</tr>
			";
			if (count($aoProduits) <= 0) {
				echo "
					<tr>
						<td colspan=\"3\">Aucune Produit n'existe actuellement. Veuillez en ajouter un.</td>
					</tr>";
			} else {
				for ($i = 0; $i < count($aoProduits); $i++) {
					//$oProduit = new Produit($aoCommandes[$i]->getCat_id_Produit()); 
					//$oProduit ->rechercherProduit();
					echo "
						<tr>
                        <td>" . $aoProduits[$i]->getIdProduit() . "</td>
                        <td>" . $aoProduits[$i]->getNomProduit() . "</td>
                        <td>" . $aoProduits[$i]->getDescProduit() . "</td>
                        <td>" . $aoProduits[$i]->getPrixProduit() . "</td>
                        <td><img class=\"images_table\"src=\"./medias/Produits/" . $aoProduits[$i]->getImageProduit() . "\" alt=\"".$aoProduits[$i]->getNomProduit()."\"></td>
                        <td>" . $oCategorie->getNomCategorie() . "</td>
                        <td><a href=\"index.php?s=" . $_GET['s'] . 
                            "&amp;action=mod&amp;idProduit=" . $aoProduits[$i]->getIdProduit() . "\" title=\"modifier un produit\"><img src=\"./medias/edit_24.png\"></a></td>
                        <td><a href=\"index.php?s=" . $_GET['s'] . 
                            "&amp;action=sup&amp;idProduit=" . $aoProduits[$i]->getIdProduit() . "\" title=\"supprimer un produit\"><img src=\"./medias/delete_24.png\"></a></td>
                    </tr>
					";
				}
			}
			echo "</table>";
		} //fin de la fonction afficherAllProduits()
		
		
		/**
		 * afficher All PlayList
		 * @param array $aoPlayList 
		 */	 
		public static  function afficherAllCommandesAvecModiffier($aoCommandes,$idCommande) {
			echo "<table id=\"all\"><tr>";
			for($iComm=0; $iComm<count($aoCommandes); $iComm++){
				echo /*"
				<dd>".$aoPlayList[$iPL]->getNomPlayList()."</dd>*/
				"<tr><td>".$aoCommandes[$iComm]->getIdCommande()."</td><td><a href=\"index.php?s=".$_GET['s']."&amp;action=add&idSong=".$idCommande."&idPlayList=".$aoCommandes[$iComm]->getIdCommande()."\"><span>modifier</span></a></td></tr>			
			";
			}
			echo "
				</tr>			
			";
		}
		
		/**
     * @access public
     * @param Produit $oProduit
     * @param string $sMsg 
     */
    public static function afficherFormCommande($sMsg = "") {
		$oCommande = new Commande;
        echo "<div id=\"Formulaire\">
            <form enctype=\"multipart/form-data\" action =\"index.php?s=" . $_GET['s'] . "&amp;action=add&amp;idCommande=" . $oCommande->getIdCommande() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"idCommande\" value =\"" . $oCommande->getIdCommande() . "\">
                <input type =\"hidden\" name =\"action\" value =\"" . $_GET['action'] . "\">
                <fieldset>
                    <legend>Commande</legend>
                    
                    <label for =\"rabais\">Rabais</label>
                    <input type =\"text\" name =\"txtRabais\" autofocus=\"\" id =\"rabais\" size=\"40\" required=\"required\" value =\"" . $oCommande->getRabaisCommande() . "\">
                    <br>
                    <label for =\"paiement\">Mode Paiement</label>
                    <input type =\"text\" name =\"txtPaiement\" id =\"paiement\" size=\"40\" required=\"required\" value =\"" . $oCommande->getPaimentCommande() . "\">
                    <br>
					<label for =\"livraison\">Livraison</label>
                    <input type =\"text\" name =\"txtlivraison\" id =\"livraison\" size=\"40\" required=\"required\" value =\"" . $oCommande->getLivraisonCommande() . "\">
                    <br>
					<label for =\"actif\">Actif</label>
                    <input type =\"text\" name =\"txtActif\" id =\"actif\" size=\"40\" required=\"required\" value =\"" . $oCommande->getActifCommande() . "\">
                    <br>
					<label for =\"client\">ID Client</label>
                    <input type =\"text\" name =\"txtIdClient\" id =\"client\" size=\"40\" required=\"required\" value =\"" . $oCommande->getClient() . "\">
                    <br>
                    <div id=\"Bouton\"><input type =\"submit\" name =\"cmd\" value =\"Enregistrer\"></div></fieldset></form></div>";
    }
	 
	 /**
		 * afficher All PlayList
		 * @param array $aoPlayList 
		 */	 
		
	 
    }//fin de la classe VuePlayList
?>