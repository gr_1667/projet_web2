<?php

/**
 * classe VueEvenement
 * @author Daniel Ferreira
 * @version 2015-07-02
 */
class VueCategorie
{
    /**
     * @access public
     * @param string $sMsg 
     */
    public static function afficherFormAdd($sMsg = "") {
        $oCategorie = new Categorie(0, " ");
        VueCategorie::afficherFormMod($oCategorie, $sMsg);
}//fin de la fonction adm_afficherFormAdd()

    /**
     * @access public
     * @param $oCategorie
     * @param string $sMsg 
     */
    public static function afficherFormMod(Categorie $oCategorie, $sMsg = "") {
        echo "<div id=\"Formulaire\">
            <form enctype=\"multipart/form-data\" action =\"index.php?s=" . $_GET['s'] . "&amp;action=" . $_GET['action'] . "&amp;idCategorie=" . $oCategorie->getIdCategorie() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"idCategorie\" value =\"" . $oCategorie->getIdCategorie() . "\">
                <input type =\"hidden\" name =\"action\" value =\"" . $_GET['action'] . "\">
                <fieldset>
                    <legend>Catégorie</legend>
                    <label for =\"ic\">Nom</label>
                    <input type =\"text\" name =\"txtNomCategorie\" size=\"50\"  id =\"ic\" required=\"required\" autofocus=\"\" value =\"" . $oCategorie->getNomCategorie() . "\">
                    <br>
                    <label for =\"dc\">Description</label>
                    <textarea name =\"txtDescCategorie\" id =\"dc\" rows=\"5\" cols=\"3\" required=\"required\" value =\"\">" . $oCategorie->getDescCategorie() . "</textarea>
                    <br>
                    <label for =\"ic\">Image</label>
                    <input type =\"file\" name =\"imgCategorie\" id =\"ic\" size=\"40\" required=\"required\" value =\"" . $oCategorie->getImageCategorie() . "\">
                    <br>
                    <div id=\"Bouton\">
                        <input type =\"submit\" name =\"cmd\" value =\"Enregistrer\">
                    </div>
                </fieldset>
            </form></div>
        ";
    }// fin de la fonction adm_afficherFormMod

    /**
     * @access public
     * @param array $aoCategories
     */
    public static function afficherToutesLesCategories($aoCategories, $sMsg = "") {
        
        $sClasse = "";

        if ($sMsg != "&nbsp;" && trim($sMsg) != "") {
            $sClasse = "class=\"msg\"";
        }
        echo "
		<h2>Gérer les catégories</h2>";
                if ($_SESSION['role'] == 'admin'){
                    echo "<h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=add\" title=\"ajouter une categorie\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter une nouvelle catégorie</span></a></h3>
		<p  style=\"height:5px;\" " . $sClasse . ">" . $sMsg . "</p>";
                }
                echo "
		<table>
                    <tr>
                        <th>ID</th>
                        <th>Nom Catégorie</th>
                        <th>Description Catégorie</th>
                        <th>Image Catégorie</th>";
                        if ($_SESSION['role'] == 'admin')
                            echo "<th colspan=\"2\">Actions</th>";
                    echo "</tr>";
        if (count($aoCategories) <= 0) {
            echo "
                <tr>
                    <td colspan=\"3\">Aucune catégorie n'existe actuellement. Veuillez en ajouter une.</td>
                </tr>";
        } else {
            for ($i = 0; $i < count($aoCategories); $i++) {
                echo "
                    <tr>
                        <td>" . $aoCategories[$i]->getIdCategorie() . "</td>
                        <td>" . $aoCategories[$i]->getNomCategorie() . "</td>
                        <td>" . $aoCategories[$i]->getDescCategorie() . "</td>
                        <td><img class=\"images_table\"src=\"./medias/Categories/" . $aoCategories[$i]->getImageCategorie() . "\" alt=\"".$aoCategories[$i]->getNomCategorie()."\"></td>";
                
                if ($_SESSION['role'] == 'admin') {
                    echo "<td><a href=\"index.php?s=" . $_GET['s'] . 
                            "&amp;action=mod&amp;idCategorie=" . $aoCategories[$i]->getIdCategorie() . "\" title=\"modifier une categorie\"><img src=\"./medias/edit_24.png\"></a></td>
                        <td><a href=\"index.php?s=" . $_GET['s'] . 
                            "&amp;action=sup&amp;idCategorie=" . $aoCategories[$i]->getIdCategorie() . "\" title=\"supprimer une categorie\"><img src=\"./medias/delete_24.png\"></a></td>";
                }
                echo "</tr>";
            }
        }
        echo "</table>";
    } //fin de la fonction adm_afficherToutesLesCategories()
    
} //fin de la classe VueCategorie
?>