<?php

/**
 * classe VueTable
 * @author Daniel Ferreira
 * @version 2015-07-03
 */
class VueTable {

    /**
     * @access public
     * @param string $sMsg 
     */
    public static function afficherFormAdd($sMsg = "") {
        $oTable = new Table(0, " ");
        VueTable::afficherFormMod($oTable, $sMsg);
    }

//fin de la fonction afficherFormAdd()

    /**
     * @access public
     * @param $oTable
     * @param string $sMsg 
     */
    public static function afficherFormMod(Table $oTable, $sMsg = "") {
        echo "<div id=\"Formulaire\">
            <form action =\"index.php?s=" . $_GET['s'] . "&amp;action=" . $_GET['action'] . "&amp;idTable=" . $oTable->getIdTable() . "\" method =\"post\" >
                <p class=\"erreur\">" . $sMsg . "</p>
                <input type =\"hidden\" name =\"idTable\" value =\"" . $oTable->getIdTable() . "\">
                <input type =\"hidden\" name =\"action\" value =\"" . $_GET['action'] . "\">
                <fieldset>
                    <legend>Capacité de la table</legend>
                    <label for =\"ic\">Capacité</label>
                    <input type =\"text\" name =\"txtCapaciteTable\" size=\"50\"  id =\"ic\" required=\"required\" autofocus=\"\" value =\"" . $oTable->getCapacTable() . "\">
                    <br>
                    
                    <div id=\"Bouton\">
                        <input type =\"submit\" name =\"cmd\" value =\"Enregistrer\">
                    </div>
                </fieldset>
            </form></div>
        ";
    }

// fin de la fonction afficherFormMod

    /**
     * @access public
     * @param array $aoTables
     */
    public static function afficherToutesLesTables($aoTables, $sMsg = "") {

        $sClasse = "";

        if ($sMsg != "&nbsp;" && trim($sMsg) != "") {
            $sClasse = "class=\"msg\"";
        }
        echo "<h2>Gérer les tables</h2>";
        if ($_SESSION['role'] == 'admin') {
            echo "<h3><a href=\"index.php?s=" . $_GET['s'] . "&amp;action=add\" title=\"ajouter une table\"><span><img src=\"./medias/add_24.png\">&nbsp;Ajouter une nouvelle table</span></a></h3>
		<p  style=\"height:5px;\" " . $sClasse . ">" . $sMsg . "</p>";
        }
        echo "<table>
                    <tr>
                        <th>No. Table</th>
                        <th>Capacité</th>";
        if ($_SESSION['role'] == 'admin')
            echo "<th colspan=\"2\">Actions</th>";
        echo "</tr>";
        if (count($aoTables) <= 0) {
            echo "
                <tr>
                    <td colspan=\"3\">Aucune table n'existe actuellement. Veuillez en ajouter une.</td>
                </tr>";
        } else {
            for ($i = 0; $i < count($aoTables); $i++) {
                echo "
                    <tr>
                        <td>" . $aoTables[$i]->getIdTable() . "</td>
                        <td>" . $aoTables[$i]->getCapacTable() . "</td>";
                if ($_SESSION['role'] == 'admin') {
                    echo "<td><a href=\"index.php?s=" . $_GET['s'] .
                    "&amp;action=mod&amp;idTable=" . $aoTables[$i]->getIdTable() . "\" title=\"modifier une table\"><img src=\"./medias/edit_24.png\"></a></td>
                        <td><a href=\"index.php?s=" . $_GET['s'] .
                    "&amp;action=sup&amp;idTable=" . $aoTables[$i]->getIdTable() . "\" title=\"supprimer une table\"><img src=\"./medias/delete_24.png\"></a></td>";
                }
                echo "</tr>";
            }
        }
        echo "</table>";
    }//fin de la fonction afficherToutesLesTables()
}//fin de la classe VueTable
?>