<!doctype html>
<html lang="fr">
    <head>
        <title>Module gestion Le 2030 Restaurant</title>
        <meta charset="utf-8"> 
        <meta name=viewport content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
          <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    </head>
    <body>
        <div id="Home">
            <header>
                <h1>LE 2030 Restaurant - MODULE DE GESTION</h1>
                <?php
                //Si un administrateur n'est pas connecté, la page affiche
                //um <div> avec un formulaire de login et mot de passe.
                if (!isset($_SESSION['administrateur'])) {
                    VueConnexion::connexion();
                } else {
                    echo '<div id="Login">';
                    echo $_SESSION['administrateur'];
                    echo '<br/><a href="../admin/vues/deconnexion.php">D&eacute;connexion</a>';
                    echo '</div>';
                ?> 

                </header>
                <nav>
                    <?php
                    if (isset($_GET['s']) == false) {
                        $_GET['s'] = 0;
                    }
                    $aOptions = array("Accueil", "Clients", "Produits", "Catégories","Commandes", "Événements", "Tables", "Réservation", "Administrateurs", "Infolettre", "Documentation");
                    echo '<ul class="menu">';
                    for ($iOption = 0; $iOption < count($aOptions); $iOption++) {
                        $sClasse = "";
                        if ($iOption == $_GET['s'])
                            $sClasse = " class=\"actif\" ";
                        echo "<li>
                            <a href=\"index.php?s=" . $iOption . "\"" . $sClasse . ">" . $aOptions[$iOption] . "</a>
                        </li>";
                    }
                    echo '</ul>';
                    ?>
                </nav>
                <article id="index">
                    
                    <section>
                        <?php
                        ControleurAdmin::gererSite();
                        ?>
                    </section>
                </article>
                <footer>
                    <p id="lnkClient"><a href="../site/index.php">CLIENT</a></p>
                    <p>&COPY; <?php echo date("Y");  ?>
                        Daniel Ferreira | Juan Carlos | Marcio Pelegrini | Mihai Polerca - Projet Web 2</p>
                </footer>
            </div>
        </body>
    </html>
    <?php
}
?>