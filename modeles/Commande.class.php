<?php

/*
 * Classe modèle pour les commandes
 */

/**
 * Description of Commandes
 *
 * @author JuanCarlosNino
 * @date 2015-06-23 -> 2015-07-14
 * 
 */
class Commande {

    /**
     *
     * Variables privés de la classe Client
     */
    private $idCommande;
    private $dDateCommande;
    private $iTotalCommande;
    private $iRabaisCommande;
    private $sPaimentCommande;
    private $iLivraisonCommande;
    private $cActifCommande;
	private $iClient;
	private $iQuantite;
    private $aProduits = array() ;
    

    /**
     * @access public
     * ajoute un commande
     * @return boolean false si l'ajout s'est mal déroulé ou le numéro du id inséré
     */
    public function ajouterCommande() {
    //Connexion à la base de données
        $oConnexion = new MySqliLib();
    //Requete de ajout d'un commande
        $sRequete = "
            INSERT INTO commandes
				(COM_DATE, COM_RABAIS, COM_MOD_PAIEMENT, COM_LIVRAISON, COM_ACTIF, CLIENTS_CLI_ID)
				values 
				( CURDATE(),
				'" . $oConnexion->getMySqli()->real_escape_string($this->iRabaisCommande) . "',
				'" . $oConnexion->getMySqli()->real_escape_string($this->sPaimentCommande) . "',
				'" . $oConnexion->getMySqli()->real_escape_string($this->iLivraisonCommande) . "',
				'" . $oConnexion->getMySqli()->real_escape_string($this->cActifCommande) . "',
				'" . $oConnexion->getMySqli()->real_escape_string($this->iClient ) . "')
            ;";

        //Exécuter la requête
        if ($oConnexion->executer($sRequete) == true)
            return $oConnexion->getMySqli()->insert_id;
        return false;
    }

    /**
     * @access public
     * modifie un commande à partir de son id
     * @return boolean false si la modification s'est mal déroulée ou 
     * @return integer le nombre de lignes modifiées si la modification s'est bien déroulée 
     */
    public function modifierCommande() {
        //Connexion à la base de données
        $oConnexion = new MySqliLib();
        //Requete de ajout d'une playList
        $sRequete = "
			UPDATE commandes
			SET COM_DATE ='" . $oConnexion->getMySqli()->real_escape_string($this->dDateCommande) . "',
            	COM_RABAIS ='" . $oConnexion->getMySqli()->real_escape_string($this->iRabaisCommande) . "',
            	COM_MOD_PAIEMENT ='" . $oConnexion->getMySqli()->real_escape_string($this->sPaimentCommande) . "',
            	COM_LIVRAISON ='" . $oConnexion->getMySqli()->real_escape_string($this->iLivraisonCommande) . "',
            	COM_ACTIF ='" . $oConnexion->getMySqli()->real_escape_string($this->cActifCommande) . "',
            	CLIENTS_CLI_ID ='" . $oConnexion->getMySqli()->real_escape_string($this-> iClient) . "'
			WHERE COM_ID = " . $this->idCommande . ";";

//Exécuter la requête
        if ($oConnexion->executer($sRequete) == true)
            return $oConnexion->getMySqli()->affected_rows;
        return false;
    }

    /**
     * @access public
     * supprime un commande par son id
     * @return boolean false si la suppression s'est mal déroulée ou 
     * @return integer le nombre de lignes supprimées si la suppression s'est bien déroulée 
     */
    public function supprimerCommande() {
//Connexion à la base de données
        $oConnexion = new MySqliLib();
//Requete de suppression d'une client identifiée par son iNoClient (ON DELETE CASCADE sur la table joue)
        $sRequete = "
			DELETE FROM commandes
			WHERE COM_ID = " . $this->idCommande . ";";

//Exécuter la requête
        if ($oConnexion->executer($sRequete) == true)
            return $oConnexion->getMySqli()->affected_rows;
        return false;
    }
	

    /**
     * @access public
     * recherche un commande par son id
     * @return boolean true si la recherche est fructueuse false sinon
     */
    public  function rechercherCommande() {
		//Connecter à la base de données
		$oConnexion = new MySqliLib();
		//Réaliser la requête de recherche par le idPlayList
		$sRequete= "SELECT commandes.COM_ID, COM_DATE, COM_RABAIS, COM_MOD_PAIEMENT, COM_LIVRAISON, COM_ACTIF, CLIENTS_CLI_ID, produits.PROD_ID, PROD_NOM, PROD_IMAGE, commandes_produits.PROD_QTT
			FROM commandes_produits
			LEFT JOIN commandes ON commandes_produits.COMMANDES_COM_ID = commandes.COM_ID
			LEFT JOIN produits ON commandes_produits.PRODUITS_PROD_ID = produits.PROD_ID
			WHERE commandes.COM_ID =".$this->idCommande.";";
		
		//Exécuter la requête
		$oResult = $oConnexion->executer($sRequete);
		if($oResult != false){
			//Récupérer le tableau des enregistrements s'il existe
			$aCommande = $oConnexion->recupererTableau($oResult);
			
			if(empty($aCommande[0]) != true){
				//Affecter les propriétés de l'objet en cours avec les valeurs
				$this->setIdCommande($aCommande[0]['COM_ID']);
                $this->setDateCommande($aCommande[0]['COM_DATE']);
				$this->setRabaisCommande($aCommande[0]['COM_RABAIS']);
				$this->setPaimentCommande($aCommande[0]['COM_MOD_PAIEMENT']);
				$this->setLivraisonCommande($aCommande[0]['COM_LIVRAISON']);
				$this->setActifCommande($aCommande[0]['COM_ACTIF']);
				$this->setClient($aCommande[0]['CLIENTS_CLI_ID']);
				$this->setProduits($aCommande);
				//retourner true
				return $aCommande;	
			}
			return false;
		}
	}//fin de la fonction rechercherPlayList() 
		
	/**
	 * @access public
	 * recherche toutes les chansons disponibles dans la base de données
	 * @return array d'objets de type Produit
	 */
	public  function rechercherAllCommandes() {
		//Connecter à la base de données
		//$oConnexion = new MySqliLib();
		$db = dbConn::getConnection();
		
		//Réaliser la requête de recherche par le sTitreProduit
		$sRequete= "SELECT * FROM commandes";
							
		//Exécuter la requête
		//$oResult = $oConnexion->executer($sRequete);
		$aCommandes = dbConn::recuperer($sRequete);
		
		if($aCommandes != false){
			//Récupérer le tableau des enregistrements s'il existe
			//$aCommande = $oConnexion->recupererTableau($oResult);
			
			$aoCommandes = array();
			
			for($iComm=0; $iComm < count($aCommandes); $iComm++){
				$aoCommandes[$iComm] = new Commande(
					$aCommandes[$iComm]['COM_ID'], 
					$aCommandes[$iComm]['COM_DATE'], 
					$aCommandes[$iComm]['COM_RABAIS'], 
					$aCommandes[$iComm]['COM_MOD_PAIEMENT'], 
					$aCommandes[$iComm]['COM_LIVRAISON'], 
					$aCommandes[$iComm]['COM_ACTIF'], 
					$aCommandes[$iComm]['CLIENTS_CLI_ID']
				);
			}				
			//retourner le tableau de toutes les chansons
			return $aoCommandes;	
		}
		return false;
	}

    /**
     * Accesseurs
     */
    public function setIdCommande($idCommande) {
        $this->idCommande = $idCommande;
    }
    public function getIdCommande() {
        return $this->idCommande;
    }

    public function setDateCommande($dDateCommande) {
        $this->dDateCommande = $dDateCommande;
    }
    public function getDateCommande() {
        return $this->dDateCommande;
    }
	
	public function setRabaisCommande($iRabaisCommande) {
        $this->iRabaisCommande = $iRabaisCommande;
    }
    public function getRabaisCommande() {
        return htmlentities($this->iRabaisCommande);
    }
	
	public function setPaimentCommande($sPaimentCommande) {
        $this->sPaimentCommande = $sPaimentCommande;
    }
    public function getPaimentCommande() {
        return htmlentities($this->sPaimentCommande);
    }
	
	public function setLivraisonCommande($iLivraisonCommande) {
        $this->iLivraisonCommande = $iLivraisonCommande;
    }
    public function getLivraisonCommande() {
        return htmlentities($this->iLivraisonCommande);
    }
	
	public function setActifCommande($cActifCommande) {
        $this->cActifCommande = $cActifCommande;
    }
    public function getActifCommande() {
        return htmlentities($this->cActifCommande);
    }
	
	public function setClient($iClient) {
        $this->iClient = $iClient;
    }
    public function getClient() {
        return htmlentities($this->iClient);
    }
	
	public function setProduits($aProduits) {
        TypeException::estArray($aProduits);
		
		if(isset($aProduits[0]) == true && is_object($aProduits[0]) == true && $aProduits[0] instanceof Produits){
			$this->aProduits = array();
			$this->aProduits = $aProduits;
		}else{
			$aProdCommande = array();
		 	for($iProd=0; $iProd<count($aProduits); $iProd++){		 		
		 		if($aProduits[$iProd]['PROD_ID'] != 0)
					$aProdCommande[$iProd] = new Produit($aProduits[$iProd]['PROD_ID']);
				if(isset($aProduits[$iProd]['PROD_NOM']) == true)
					$aProdCommande[$iProd]->setNomProduit(trim($aProduits[$iProd]['PROD_NOM']));
				if(isset($aProduits[$iProd]['PROD_DESCRIPTION']) == true)
					$aProdCommande[$iProd]->setDescProduit(trim($aProduits[$iProd]['PROD_DESCRIPTION']));
				if(isset($aProduits[$iProd]['PROD_PRIX']) == true)
					$aProdCommande[$iProd]->setPrixProduit(trim($aProduits[$iProd]['PROD_PRIX']));
				if(isset($aProduits[$iProd]['PROD_IMAGE']) == true)
					$aProdCommande[$iProd]->setImageProduit(trim($aProduits[$iProd]['PROD_IMAGE']));
				if(isset($aProduits[$iProd]['PROD_ACTIF']) == true)
					$aProdCommande[$iProd]->setActifProduit(trim($aProduits[$iProd]['PROD_ACTIF']));
				if(isset($aProduits[$iProd]['CATEGORIES_CAT_ID']) == true)
					$aProdCommande[$iProd]->setCat_id_Produit(trim($aProduits[$iProd]['CATEGORIES_CAT_ID']));
			}//fin du for
        //$this->aProduits = $cActifCommande;
		}
    }
	
    public function getProduits() {
        return $this->aProduits;
    }

    public function __construct($idCommande = 1, $dDateCommande = "2000-01-01", $iRabaisCommande = 1, $sPaimentCommande = "Visa", $iLivraisonCommande = "5", $cActifCommande = "Y", $iClient = 1) {
        $this->setidCommande($idCommande);
        $this->setDateCommande($dDateCommande);
		$this->setRabaisCommande($iRabaisCommande);
        $this->setPaimentCommande($sPaimentCommande);
        $this->setLivraisonCommande($iLivraisonCommande);
		$this->setActifCommande($cActifCommande);
		$this->setClient($iClient);
    }

}

?>