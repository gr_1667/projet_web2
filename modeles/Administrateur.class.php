<?php

/*
 * Classe modèle pour les administrateurs
 */

/**
 *
 * @author MarcioAndrei
 * @date 2015-06-23
 * 
 */
class Administrateur {

    /**
     *
     * Variables privés de la classe Administrateur
     */
    private $idAdm;
    private $sEmailAdm;
    private $sNomAdm;
    private $sPrenomAdm;
    private $sMotPasseAdm;
    private $sRoleAdm;

    /**
     * Accesseurs
     */
    function getIdAdm() {
        return $this->idAdm;
    }

    function getSEmailAdm() {
        return $this->sEmailAdm;
    }

    function getSNomAdm() {
        return $this->sNomAdm;
    }

    function getSPrenomAdm() {
        return $this->sPrenomAdm;
    }

    function getSMotPasseAdm() {
        return $this->sMotPasseAdm;
    }

    function setIdAdm($idAdm) {
        TypeException::estNumerique($idAdm);
        $this->idAdm = $idAdm;
    }

    function setSEmailAdm($sEmailAdm) {
        TypeException::estString($sEmailAdm);
        $this->sEmailAdm = htmlentities($sEmailAdm);
    }

    function setSNomAdm($sNomAdm) {
        TypeException::estString($sNomAdm);
        $this->sNomAdm = htmlentities($sNomAdm);
    }

    function setSPrenomAdm($sPrenomAdm) {
        TypeException::estString($sPrenomAdm);
        $this->sPrenomAdm = htmlentities($sPrenomAdm);
    }
    
    function setSMotPasseAdm($sMotPasseAdm) {
        $this->sMotPasseAdm = sha1($sMotPasseAdm);
    }
    
    function getRoleAdm() {
        return $this->sRoleAdm;
    }

    function setRoleAdm($sRoleAdm) {
        $this->sRoleAdm = $sRoleAdm;
    }
        

    /**
     * @access public
     * connexion de l'administrateur dans le système
     * @return boolean false si la connexion a échoué
     */
    public function connexionAdministrateur($user, $pass) {
        //Faire l'encryptation du mot de passe à être comparable laquelle 
        $motPasse = sha1($pass);
        $sRequete = "SELECT ADM_PRENOM, ADM_ID, ADM_ROLE FROM administrateurs"
                . " WHERE ADM_EMAIL = :pEmail AND ADM_MOTPASSE = :pMotPasse";

        //Connecter à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        
        //Exécuter la requête
        $db->bindValue(":pEmail", $user);
        $db->bindValue(":pMotPasse", $motPasse);
        $db->execute();
        $db->setFetchMode(PDO::FETCH_ASSOC);
        $aAdm = $db->fetchall();

        if ($aAdm != false) {
            if (empty($aAdm[0]) != true) {
                //Affecter les propriétés de l'objet en cours avec les valeurs

                $_SESSION['administrateur'] = $aAdm[0]['ADM_PRENOM'];
                $_SESSION['id'] = $aAdm[0]['ADM_ID'];
                $_SESSION['role'] = $aAdm[0]['ADM_ROLE'];

                //retourner true
                return true;
            }
            return false;
        }
    }

    /**
     * @access public
     * ajoute un administrateur
     * @return boolean false si l'ajout s'est mal déroulé ou le numéro du id inséré
     */
    public function ajouterUnAdministrateur() {
        //Requete de ajout d'un administrateur
        $sRequete = "
            INSERT INTO administrateurs SET
                ADM_EMAIL = :pEmail,
                ADM_NOM = :pNom,
                ADM_PRENOM = :pPrenom,
                ADM_MOTPASSE = :pMotPasse,
                ADM_ROLE = :pRole
            ;";
        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        // Données
        $db->bindValue(":pEmail", $this->sEmailAdm);
        $db->bindValue(":pNom", $this->sNomAdm);
        $db->bindValue(":pPrenom", $this->sPrenomAdm);
        $db->bindValue(":pMotPasse", $this->sMotPasseAdm);
        $db->bindValue(":pRole", $this->sRoleAdm);

        //Execute la requete
        return $db->execute();
    }

    /**
     * @access public
     * modifie l'administrateur à partir de son id
     * @return boolean false si la modification s'est mal déroulée ou 
     * @return integer le nombre de lignes modifiées si la modification s'est bien déroulée 
     */
    public function modifierUnAdministrateur() {
        //Requete de ajout d'un administrateur
        $sRequete = "
            UPDATE administrateurs SET
                ADM_EMAIL = :pEmail,
                ADM_NOM = :pNom,
                ADM_PRENOM = :pPrenom,
                ADM_MOTPASSE = :pMotPasse,
                ADM_ROLE = :pRole
                WHERE ADM_ID = :pID
            ;";
        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        // Données
        $db->bindValue(":pEmail", $this->sEmailAdm);
        $db->bindValue(":pNom", $this->sNomAdm);
        $db->bindValue(":pPrenom", $this->sPrenomAdm);
        $db->bindValue(":pMotPasse", $this->sMotPasseAdm);
        $db->bindValue(":pRole", $this->sRoleAdm);
        $db->bindValue(":pID", $this->idAdm);

        //Execute la requete
        return $db->execute();
    }

    /**
     * @access public
     * supprime un administrateur par son id
     * @return boolean false si la suppression s'est mal déroulée ou 
     * @return integer le nombre de lignes supprimées si la suppression s'est bien déroulée 
     */
    public function supprimerUnAdministrateur() {
        //Requete de SQL
        $sRequete = "
            DELETE FROM administrateurs
                WHERE ADM_ID = :pID
            ;";
        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        // Données
        $db->bindValue(":pID", $this->idAdm);

        //Execute la requete
        return $db->execute();
    }

    /**
     * @access public
     * recherche un client par son id
     * @return boolean true si la recherche est fructueuse false sinon
     */
    public function rechercherUnAdministrateur() {
        //Réaliser la requête de recherche par le sTitreClient
        $sRequete = "SELECT * FROM administrateurs
                    WHERE ADM_ID = :pID";
        //Connecter à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        //Exécuter la requête
        $db->bindValue(":pID", $this->idAdm);
        $db->execute();
        $db->setFetchMode(PDO::FETCH_ASSOC);
        $aAdm = $db->fetchall();

        if ($aAdm != false) {
            //Récupérer le tableau des enregistrements s'il existe

            if (empty($aAdm[0]) != true) {
                //Affecter les propriétés de l'objet en cours avec les valeurs
                //$this->setCHAMP($aClient[0]['CHAMP_DE_LA_BASE_DONNES']);
                $this->setIdAdm($aAdm[0]['ADM_ID']);
                $this->setSEmailAdm($aAdm[0]['ADM_EMAIL']);
                $this->setSNomAdm($aAdm[0]['ADM_NOM']);
                $this->setSPrenomAdm($aAdm[0]['ADM_PRENOM']);
                $this->setRoleAdm($aAdm[0]['ADM_ROLE']);
                //retourner true
                return true;
            }
            return false;
        }
    }

    /**
     * @access public
     * recherche tous les clients disponibles dans la base de données
     * @return array d'objets de type Client
     */
    public static function rechercherTousAdministrateurs() {
        //Connecter à la base de données
        $db = dbConn::getConnection();
        //Réaliser la requête de recherche de toutes les chansons
        $sRequete = "SELECT * FROM administrateurs;";

        //Exécuter la requête
        $aAdm = dbConn::recuperer($sRequete);
        if ($aAdm != false) {
            //Récupérer le tableau des enregistrements s'il existe
            //$aAdm = $oConnexion->recupererTableau($oResult);

            $aoAdm = array();

            for ($i = 0; $i < count($aAdm); $i++) {
                $aoAdm[$i] = new Administrateur($aAdm[$i]['ADM_ID'], $aAdm[$i]['ADM_EMAIL'], $aAdm[$i]['ADM_NOM'], $aAdm[$i]['ADM_PRENOM'], $aAdm[$i]['ADM_MOTPASSE'], $aAdm[$i]['ADM_ROLE']);
            }

            //retourner le tableau de toutes les chansons
            return $aoAdm;
        }
        return false;
    }

    public function __construct($iNoadm = 1, $sEmailAdm = " ", $sNomAdm = " ", $sPrenomAdm = " ", $sMotPasseAdm = " ", $sRole = " ") {
        $this->setIdAdm($iNoadm);
        $this->setSEmailAdm($sEmailAdm);
        $this->setSNomAdm($sNomAdm);
        $this->setSPrenomAdm($sPrenomAdm);
        $this->setSMotPasseAdm($sMotPasseAdm);
        $this->setRoleAdm($sRole);
    }

}

?>