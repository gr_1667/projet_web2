<?php

/*
 * Classe modèle pour l'infolettre
 */

/**
 *
 * @author MarcioAndrei
 * @date 2015-06-23
 * 
 */
class Infolettre {

    /**
     *
     * Variables privés de la classe Infolettre
     */
    private $sEmailInfo;

    /**
     * Accesseurs
     */
    function getSEmailInfo() {
        return $this->sEmailInfo;
    }

    function setSEmailInfo($sEmailInfo) {
        TypeException::estString($sEmailInfo);
        $this->sEmailInfo = $sEmailInfo;
    }

    /**
     * @access public
     * ajoute un courriel pour infolettre
     * @return boolean false si l'ajout s'est mal déroulé ou le numéro du id inséré
     */
    public function ajouterUnCourriel() {
        //Requete
        $sRequete = "
            INSERT INTO infolettre SET
                INFO_EMAIL = :pEmail
            ;";

        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        // Données
        $db->bindValue(":pEmail", $this->sEmailInfo);
        //Execute la requete
        return $db->execute();
    }

    /**
     * @access public
     * supprime un administrateur par son id
     * @return boolean false si la suppression s'est mal déroulée ou 
     * @return integer le nombre de lignes supprimées si la suppression s'est bien déroulée 
     */
    public function supprimerUnCourriel() {
        //Requete de SQL
        $sRequete = "
            DELETE FROM infolettre
                WHERE INFO_EMAIL = :pEmail
            ;";
        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        // Données
        $db->bindValue(":pEmail", $this->sEmailInfo);

        //Execute la requete
        return $db->execute();
    }

    /**
     * @access public
     * recherche un client par son id
     * @return boolean true si la recherche est fructueuse false sinon
     */
    public function rechercherUnCourriel() {
        //Réaliser la requête de recherche par le sTitreClient
        $sRequete = "SELECT * FROM infolettre
                    WHERE INFO_EMAIL = :pEmail";
        //Connecter à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        //Exécuter la requête
        $db->bindValue(":pEmail", $this->sEmailInfo);
        $db->execute();
        $db->setFetchMode(PDO::FETCH_ASSOC);
        $aInfo = $db->fetchall();

        if ($aInfo != false) {
            //Récupérer le tableau des enregistrements s'il existe

            if (empty($aInfo[0]) != true) {
                //Affecter les propriétés de l'objet en cours avec les valeurs
                //$this->setCHAMP($aClient[0]['CHAMP_DE_LA_BASE_DONNES']);
                $this->setSEmailInfo($aInfo[0]['INFO_EMAIL']);
                //retourner true
                return true;
            }
            return false;
        }
    }

    /**
     * @access public
     * recherche tous les clients disponibles dans la base de données
     * @return array d'objets de type Client
     */
    public static function rechercherTousCourriels() {
        //Connecter à la base de données
        $db = dbConn::getConnection();
        //Réaliser la requête de recherche de toutes les chansons
        $sRequete = "SELECT * FROM infolettre;";

        //Exécuter la requête
        $aInfo = dbConn::recuperer($sRequete);
        if ($aInfo != false) {
            $aoInfo = array();

            for ($i = 0; $i < count($aInfo); $i++) {
                $aoInfo[$i] = new Infolettre($aInfo[$i]['INFO_EMAIL']);
            }

            //retourner le tableau de toutes les chansons
            return $aoInfo;
        }
        return false;
    }

//fin de la fonction rechercherTousClients() 

    public function __construct($sEmail = " ") {
        $this->setSEmailInfo($sEmail);
    }

}

?>