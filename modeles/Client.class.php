<?php

/*
 * Classe modèle pour les clients
 */

/**
 * Description of Client
 *
 * @author MarcioAndrei
 * @date 2015-06-23
 * 
 */
class Client {

    /**
     *
     * Variables privés de la classe Client
     */
    private $idClient;
    private $sEmailClient;
    private $sNomClient;
    private $sPrenomClient;
    private $sAdresseClient;
    private $sVilleClient;
    private $sProvinceClient;
    private $sCodePostalClient;
    private $sPhoneClient;
    private $sMotPasseClient;
    private $iPointsClient;
    private $sActifClient;

    /**
     * Accesseurs
     */
    function getIdClient() {
        return $this->idClient;
    }

    function getSEmailClient() {
        return $this->sEmailClient;
    }

    function getSNomClient() {
        return $this->sNomClient;
    }

    function getSPrenomClient() {
        return $this->sPrenomClient;
    }

    function getSAdresseClient() {
        return $this->sAdresseClient;
    }

    function getSVilleClient() {
        return $this->sVilleClient;
    }

    function getSProvinceClient() {
        return $this->sProvinceClient;
    }

    function getSCodePostalClient() {
        return $this->sCodePostalClient;
    }

    function getSPhoneClient() {
        return $this->sPhoneClient;
    }

    function getSMotPasseClient() {
        return $this->sMotPasseClient;
    }

    function getIPointsClient() {
        return $this->iPointsClient;
    }

    function getSActifClient() {
        return $this->sActifClient;
    }

    function setIdClient($idClient) {
        TypeException::estNumerique($idClient);
        $this->idClient = $idClient;
    }

    function setSEmailClient($sEmailClient) {
        TypeException::estString($sEmailClient);
        $this->sEmailClient = htmlentities($sEmailClient);
    }

    function setSNomClient($sNomClient) {
            $this->sNomClient = htmlentities($sNomClient);
    }

    function setSPrenomClient($sPrenomClient) {
        TypeException::estString($sPrenomClient);
        $this->sPrenomClient = htmlentities($sPrenomClient);
    }

    function setSAdresseClient($sAdresseClient) {
        TypeException::estString($sAdresseClient);
        $this->sAdresseClient = htmlentities($sAdresseClient);
    }

    function setSVilleClient($sVilleClient) {
        TypeException::estString($sVilleClient);
        $this->sVilleClient = htmlentities($sVilleClient);
    }

    function setSProvinceClient($sProvinceClient) {
        TypeException::estString($sProvinceClient);
        $this->sProvinceClient = htmlentities($sProvinceClient);
    }

    function setSCodePostalClient($sCodePostalClient) {
        TypeException::estString($sCodePostalClient);
        $this->sCodePostalClient = htmlentities($sCodePostalClient);
    }

    function setSPhoneClient($sPhoneClient) {
        $this->sPhoneClient = htmlentities($sPhoneClient);
    }

    function setSMotPasseClient($sMotPasseClient) {
        $this->sMotPasseClient = sha1($sMotPasseClient);
    }

    function setIPointsClient($iPointsClient) {
        TypeException::estNumerique($iPointsClient);
        $this->iPointsClient = htmlentities($iPointsClient);
    }

    function setSActifClient($sActifClient) {
        $this->sActifClient = $sActifClient;
    }

    /**
     * 
     * @param type $emailUser
     * @param type $pass
     * @return boolean
     */
    public function connexionClient($emailUser, $pass) {
        //Faire l'encryptation du mot de passe
        $motPasse = sha1($pass);
        $sRequete = "SELECT CLI_PRENOM, CLI_ID FROM clients"
                . " WHERE CLI_EMAIL = :pEmail AND CLI_MOTPASSE = :pMotPasse";

        //Connecter à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);

        //Exécuter la requête
        $db->bindValue(":pEmail", $emailUser);
        $db->bindValue(":pMotPasse", $motPasse);
        $db->execute();
        $db->setFetchMode(PDO::FETCH_ASSOC);
        $aCli = $db->fetchall();

        if ($aCli != false) {
            if (empty($aCli[0]) != true) {
                //Affecter les propriétés de l'objet en cours avec les valeurs

                $_SESSION['client'] = $aCli[0]['CLI_PRENOM'];
                $_SESSION['idClient'] = $aCli[0]['CLI_ID'];

                //retourner true
                return true;
            }
            return false;
        }
    }

    /**
     * @access public
     * ajoute un client
     * @return boolean false si l'ajout s'est mal déroulé ou le numéro du id inséré
     */
    public function ajouterUnClient() {
        //Requete de ajout d'un client
        $sRequete = "
            INSERT INTO clients SET
                CLI_EMAIL = :pEmail,
                CLI_NOM = :pNom,
                CLI_PRENOM = :pPrenom,
                CLI_ADRESSE = :pAdresse,
                CLI_VILLE = :pVille,
                CLI_CODEPOSTAL = :pCodePostal,
                CLI_PHONE = :pPhone,
                CLI_MOTPASSE = :pMotPasse,
                CLI_ACTIF = '1',
                CLI_POINTS = :pPoints
            ;";

        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        // Données
        $db->bindValue(":pEmail", $this->sEmailClient);
        $db->bindValue(":pNom", $this->sNomClient);
        $db->bindValue(":pPrenom", $this->sPrenomClient);
        $db->bindValue(":pAdresse", $this->sAdresseClient);
        $db->bindValue(":pVille", $this->sVilleClient);
        $db->bindValue(":pCodePostal", $this->sCodePostalClient);
        $db->bindValue(":pPhone", $this->sPhoneClient);
        $db->bindValue(":pMotPasse", $this->sMotPasseClient);
        $db->bindValue(":pPoints", $this->iPointsClient);

        //Execute la requete
        return $db->execute();
    }

    /**
     * @access public
     * modifie le client à partir de son id
     * @return boolean false si la modification s'est mal déroulée ou 
     * @return integer le nombre de lignes modifiées si la modification s'est bien déroulée 
     */
    public function modifierUnClient() {
        //Requete de ajout d'un client
        $sRequete = "
            UPDATE clients SET
                CLI_EMAIL = :pEmail,
                CLI_NOM = :pNom,
                CLI_PRENOM = :pPrenom,
                CLI_ADRESSE = :pAdresse,
                CLI_VILLE = :pVille,
                CLI_CODEPOSTAL = :pCodePostal,
                CLI_PHONE = :pPhone,
                CLI_MOTPASSE = :pMotPasse,
                CLI_ACTIF = '1',
                CLI_POINTS = :pPoints
            WHERE CLI_ID = :pID
            ;";

        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        // Données
        $db->bindValue(":pEmail", $this->sEmailClient);
        $db->bindValue(":pNom", $this->sNomClient);
        $db->bindValue(":pPrenom", $this->sPrenomClient);
        $db->bindValue(":pAdresse", $this->sAdresseClient);
        $db->bindValue(":pVille", $this->sVilleClient);
        $db->bindValue(":pCodePostal", $this->sCodePostalClient);
        $db->bindValue(":pPhone", $this->sPhoneClient);
        $db->bindValue(":pMotPasse", $this->sMotPasseClient);
        $db->bindValue(":pPoints", $this->iPointsClient);
        $db->bindValue(":pID", $this->idClient);

        //Execute la requete
        return $db->execute();
    }

    /**
     * @access public
     * supprime un client par son id
     * @return boolean false si la suppression s'est mal déroulée ou 
     * @return integer le nombre de lignes supprimées si la suppression s'est bien déroulée 
     */
    public function supprimerUnClient() {
        //Requete de SQL
        $sRequete = "
            DELETE FROM clients
                WHERE CLI_ID = :pID
            ;";
        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        // Données
        $db->bindValue(":pID", $this->idClient);

        //Execute la requete
        return $db->execute();
    }

    /**
     * @access public
     * recherche un client par son id
     * @return boolean true si la recherche est fructueuse false sinon
     */
    public function rechercherClient() {
        //Réaliser la requête de recherche par le sTitreClient
        $sRequete = "SELECT * FROM clients
                    WHERE CLI_ID = :pID";
        //Connecter à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        //Exécuter la requête
        $db->bindValue(":pID", $this->idClient);
        $db->execute();
        $db->setFetchMode(PDO::FETCH_ASSOC);
        $aClient = $db->fetchall();

        if ($aClient != false) {
            //Récupérer le tableau des enregistrements s'il existe

            if (empty($aClient[0]) != true) {
                //Affecter les propriétés de l'objet en cours avec les valeurs
                //$this->setCHAMP($aClient[0]['CHAMP_DE_LA_BASE_DONNES']);
                $this->setIdClient($aClient[0]['CLI_ID']);
                $this->setSEmailClient($aClient[0]['CLI_EMAIL']);
                $this->setSNomClient($aClient[0]['CLI_NOM']);
                $this->setSPrenomClient($aClient[0]['CLI_PRENOM']);
                $this->setSAdresseClient($aClient[0]['CLI_ADRESSE']);
                $this->setSVilleClient($aClient[0]['CLI_VILLE']);
                $this->setSCodePostalClient($aClient[0]['CLI_CODEPOSTAL']);
                $this->setSPhoneClient($aClient[0]['CLI_PHONE']);
                //retourner true
                return true;
            }
            return false;
        }
    }

    /**
     * @access public
     * recherche un client par son nom
     * @return boolean true si la recherche est fructueuse false sinon
     */
    public function rechercherClientParNom() {
        //Connecter à la base de données
        $oConnexion = new MySqliLib();
        //Réaliser la requête de recherche par le sTitreClient
        $sRequete = "SELECT * FROM clients
                    WHERE CLI_NOM LIKE('%" . $oConnexion->getMySqli()->real_escape_string($this->getSNomClient()) . "%');";

        //Exécuter la requête
        $oResult = $oConnexion->executer($sRequete);
        if ($oResult != false) {
            //Récupérer le tableau des enregistrements s'il existe
            $aClient = $oConnexion->recupererTableau($oResult);

            if (empty($aClient[0]) != true) {
                //Affecter les propriétés de l'objet en cours avec les valeurs
                $this->setIdClient($aClient[0]['CLI_ID']);
                $this->setSEmailClient($aClient[0]['CLI_EMAIL']);
                $this->setSNomClient($aClient[0]['CLI_NOM']);
                $this->setSPrenomClient($aClient[0]['CLI_PRENOM']);
                $this->setSAdresseClient($aClient[0]['CLI_ADRESSE']);
                $this->setSVilleClient($aClient[0]['CLI_VILLE']);
                $this->setSCodePostalClient($aClient[0]['CLI_CODEPOSTAL']);
                $this->setSPhoneClient($aClient[0]['CLI_PHONE']);
                $this->setIPointsClient($aClient[0]['CLI_POINTS']);

                //retourner true
                return true;
            }
            return false;
        }
    }

    /**
     * @access public
     * recherche des clients par leur nom
     * @return array d'objets Client si la recherche est fructueuse false sinon
     */
    public function rechercherClientsParNom($valeur) {
        //Connecter à la base de données
        $oConnexion = new MySqliLib();
        //Réaliser la requête de recherche par le sTitreClient
        $sRequete = "SELECT * FROM clients
                    WHERE CLI_NOM LIKE('%" . $oConnexion->getMySqli()->real_escape_string($valeur) . "%');";

        //Exécuter la requête
        $oResult = $oConnexion->executer($sRequete);
        if ($oResult != false) {
            //Récupérer le tableau des enregistrements s'il existe
            $aClients = $oConnexion->recupererTableau($oResult);

            $aoClients = array();

            for ($i = 0; $i < count($aClients); $i++) {
                $aoClients[$i] = new Client($aClients[$i]['CLI_ID'], $aClients[$i]['CLI_EMAIL'], $aClients[$i]['CLI_NOM'], $aClients[$i]['CLI_PRENOM'], $aClients[$i]['CLI_ADRESSE'], $aClients[$i]['CLI_VILLE'], $aClients[$i]['CLI_CODEPOSTAL'], $aClients[$i]['CLI_PHONE'], $aClients[$i]['CLI_POINTS']);
            }
            return $aoClients;
        }
        return false;
    }

    /**
     * @access public
     * recherche tous les clients disponibles dans la base de données
     * @return array d'objets de type Client
     */
    public static function rechercherTousClients() {
        $db = dbConn::getConnection();
        //Réaliser la requête de recherche de toutes les chansons
        $sRequete = "SELECT * FROM clients;";

        //Exécuter la requête
        $aClients = dbConn::recuperer($sRequete);
        if ($aClients != false) {
            $aoClients = array();

            for ($i = 0; $i < count($aClients); $i++) {
                $aoClients[$i] = new Client($aClients[$i]['CLI_ID'], $aClients[$i]['CLI_EMAIL'], $aClients[$i]['CLI_NOM'], $aClients[$i]['CLI_PRENOM'], $aClients[$i]['CLI_ADRESSE'], $aClients[$i]['CLI_VILLE'], $aClients[$i]['CLI_CODEPOSTAL'], $aClients[$i]['CLI_PHONE'], $aClients[$i]['CLI_POINTS']);
            }

            //retourner le tableau de toutes les chansons
            return $aoClients;
        }
        return false;
    }

//fin de la fonction rechercherTousClients() 

    public function __construct($iNoClient = 1, $sEmailClient = " ", $sNomClient = " ", $sPrenomClient = " ", $sAdresseClient = " ", $sVilleClient = " ", $sCodePostalClient = " ", $sPhoneClient = " ", $sMotPasseClient = " ", $sActif = "1", $iPoints = 0) {
        $this->setIdClient($iNoClient);
        $this->setSEmailClient($sEmailClient);
        $this->setSNomClient($sNomClient);
        $this->setSPrenomClient($sPrenomClient);
        $this->setSAdresseClient($sAdresseClient);
        $this->setSVilleClient($sVilleClient);
        $this->setSCodePostalClient($sCodePostalClient);
        $this->setSPhoneClient($sPhoneClient);
        $this->setSMotPasseClient($sMotPasseClient);
        $this->setSActifClient($sActif);
        $this->setIPointsClient($iPoints);
    }

}

?>