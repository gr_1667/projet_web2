<?php

/*
 * Classe modèle pour les événements
 */

/**
 * Informations par rapport les événements
 *
 * @author Daniel Ferreira
 * @date 2015-06-23
 *
 */
class Evenement {
    
    /**
     *
     * Variables privés de la classe Evenement
    */
    private $iNoEvenement;
    private $sDescriptEvent;
    private $sImgEvent;
    private $dDateEvent;
    private $sActifEvent;
    
    
    public function __construct($iNoEvenement = 1, $sDescriptEvent = " ", $sImgEvent = " ", $dDateEvent = " ", $sActifEvent = " ")
    {
        $this->setNoEvenement($iNoEvenement);
        $this->setDescriptEvent($sDescriptEvent);
        $this->setImgEvenement($sImgEvent);
        $this->setDateEvenement($dDateEvent);
        $this->setEvenementActif($sActifEvent);
    }
    
    /**
     * Accesseurs
     */
    public function setNoEvenement($iNoEvenement)
    {
        if ($iNoEvenement != null && $iNoEvenement != '')
	{
            TypeException::estNumerique($iNoEvenement);
            $this->iNoEvenement = $iNoEvenement;
        }
    }

    public function getNoEvenement()
    {
        return $this->iNoEvenement;
    }

    
    public function setDescriptEvent($sDescriptEvent)
    { 
        if ($sDescriptEvent != null && $sDescriptEvent != '')
	{
            TypeException::estString($sDescriptEvent);
            $this->sDescriptEvent = htmlentities($sDescriptEvent);
        }
    }

    public function getDescriptEvent()
    {
        return $this->sDescriptEvent;
    }

    public function setImgEvenement($sImgEvent)
    {
        if ($sImgEvent != null && $sImgEvent != '')
	{
            TypeException::estString($sImgEvent);
            $this->sImgEvent = $sImgEvent;
        }
    }

    public function getImgEvenement()
    {
        return $this->sImgEvent;
    }

    public function setDateEvenement($dDateEvent)
    {
        //TypeException::estString($dDateEvent);
        $this->dDateEvent = $dDateEvent;
    }

    public function getDateEvenement()
    {
        return $this->dDateEvent;
    }

    public function setEvenementActif($sActifEvent)
    {
        if ($sActifEvent != null && $sActifEvent != '')
	{
            TypeException::estString($sActifEvent);
            $this->sActifEvent = $sActifEvent;
        }
    }

    public function getEvenementActif()
    {
        return $this->sActifEvent;
    }

    /**
     * @access public
     * ajouter un événement
     * @return boolean false si l'ajout s'est mal déroulé ou le numéro du id inséré
     */
    public function ajouterUnEvenement() {
        //Requete de ajout d'un evenement
        $sRequete = "
            INSERT INTO evenements (EVE_ID, EVE_DESCRIPTION, EVE_IMAGE, EVE_DATE, EVE_ACTIF) 
            VALUES (NULL, :eveDesc, :eveImg, :eveDate, :eveActif)
            ;";

        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        
        // new data
        $db->bindValue(":eveDesc" , $this->sDescriptEvent);
        $db->bindValue(":eveImg"  , $this->sImgEvent);
        $db->bindValue(":eveDate"  , $this->dDateEvent);
        $db->bindValue(":eveActif" , $this->sActifEvent);
        return $db->execute();
        
    }//fin de la fonction ajouterUnEvenement
 
    
    /**
     * @access public
     * modifie l'événement à partir de son id
     * @return boolean false si la modification s'est mal déroulée ou 
     * @return integer le nombre de lignes modifiées si la modification s'est bien déroulée 
     */
    public function modifierUnEvenement() {
        //Requete de ajout d'une playList
        $sRequete ="
			UPDATE evenements
			SET EVE_DESCRIPTION = :eveDesc,
                            EVE_IMAGE = :eveImg,
                            EVE_DATE = :eveDate
                        WHERE EVE_ID = :eveId;
                    "; 
        //Connexion à la base de données
            $db = dbConn::getConnection()->prepare($sRequete);
            
        // new data
            $db->bindValue(":eveDesc" , $this->sDescriptEvent);
            $db->bindValue(":eveImg"  , $this->sImgEvent);
            $db->bindValue(":eveDate"  , $this->dDateEvent);
            $db->bindValue(":eveId" , $this->iNoEvenement);
            return $db->execute();
    }// fin de la function modifierUnEvenement()

    /**
     * @access public
     * supprime un événement par son id
     * @return boolean false si la suppression s'est mal déroulée ou 
     * @return integer le nombre de lignes supprimées si la suppression s'est bien déroulée 
     */
    public function supprimerUnEvenement() {
    //Requete de suppression d'un evenement identifiée par son iNoEvenement
        $sRequete = "
			DELETE FROM evenements
			WHERE EVE_ID = :eveId;";
    
        //Connexion à la base de données
            $db = dbConn::getConnection()->prepare($sRequete);

        // new data
            $db->bindValue(":eveId" , $this->iNoEvenement);
				
            //Exécuter la requête
             return $db->execute();
    
    }// fin de la fonction supprimerUnEvenement

    /**
     * @access public
     * recherche un événement par son id
     * @return boolean true si la recherche est fructueuse false sinon
     */
    public function rechercherEvenement() {
        //Réaliser la requête de recherche
        $sRequete = "SELECT * FROM evenements WHERE EVE_ID = :eveId";
        
        //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        $db->bindValue(":eveId" , $this->iNoEvenement);
        $db -> execute();

        //Exécuter la requête
        $db->setFetchMode(PDO::FETCH_ASSOC);
        $aEvenement = $db ->fetchall();

        if ($aEvenement != false) {
            //Récupérer le tableau des enregistrements s'il existe
            $aoEvenements = array();

            if (empty($aEvenement[0]) != true) {
            //Affecter les propriétés de l'objet en cours avec les valeurs
                $this->setNoEvenement($aEvenement[0]['EVE_ID']);
                $this->setDescriptEvent($aEvenement[0]['EVE_DESCRIPTION']);
                $this->setImgEvenement($aEvenement[0]['EVE_IMAGE']);
                $this->setDateEvenement($aEvenement[0]['EVE_DATE']);
                //$this->setEvenementActif($aEvenement[0]['EVE_ACTIF']);

                return true;
            }
            return false;
        }
    }
//fin de la fonction rechercherEvenement

    
    /**
     * @access public
     * recherche un événement par son nom/description
     * @return boolean true si la recherche est fructueuse false sinon
     */
    public function rechercherEvenementByDescription() {
    //Réaliser la requête de recherche par le sDescriptEvent
        $sRequete = "SELECT * FROM evenements
			WHERE EVE_DESCRIPTION LIKE('%:eveDesc%');";
    
    //Connecter à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        $db->bindValue(":eveDesc" , $this->sDescriptEvent);

    //Exécuter la requête
        $db -> execute();		
        $db->setFetchMode(PDO::FETCH_OBJ);
        $aEvenement = $db ->fetchall();
            
        if ($aEvenement != false) {
    //Récupérer le tableau des enregistrements s'il existe
        $aoEvenements = array();

        if (empty($aEvenement[0]) != true) {
    //Affecter les propriétés de l'objet en cours avec les valeurs
            $this->setNoEvenement($aEvenement[0]['EVE_ID']);
            $this->setDescriptEvent($aEvenement[0]['EVE_DESCRIPTION']);
            $this->setImgEvenement($aEvenement[0]['EVE_IMAGE']);
            $this->setDateEvenement($aEvenement[0]['EVE_DATE']);
            //$this->setEvenementActif($aEvenement[0]['EVE_ACTIF']);
            
            return true;
            }
        return false;
        }
    }// fin de la fonction rechercherEvenementByDescription

    
    /**
     * @access public
     * recherche tous les événements dans la base de données
     * @return array d'objets de type Evenement
     */
    public static function rechercherTousLesEvenements() {
    //Réaliser la requête de recherche de toutes les chansons
        $sRequete = "SELECT * FROM evenements ORDER BY EVE_DATE DESC;";
    
    //Connecter à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);

    //Exécuter la requête
        $aEvenements = dbConn::recuperer($sRequete);
        
        if ($aEvenements != false) {
    //Récupérer le tableau des enregistrements s'il existe
        $aoEvenements = array();
        
        for ($i = 0; $i < count($aEvenements); $i++)
        {
            $aoEvenements[$i] = new Evenement($aEvenements[$i]['EVE_ID'], $aEvenements[$i]['EVE_DESCRIPTION'], $aEvenements[$i]['EVE_IMAGE'], $aEvenements[$i]['EVE_DATE']/*, $aEvenements[$i]['EVE_ACTIF']*/);
        }

    //retourner le tableau de toutes les événements
        return $aoEvenements;
        }
    return false;
    }//fin de la fonction rechercherTousLesEvenements() 
}

?>