<?php

/*
 * Classe modèle pour les réservation */

/**
 * Informations par rapport les réservations
 *
 * @author Daniel Ferreira
 * @date 2015-06-23
 *
 */

class Reservation {
    
    /**
     *
     * Variables privés de la classe Reservation
    */
    private $iNoReserv;
    private $dDateReserv;
    private $hHeureDebutReserv;
    private $hHeureFinReserv;
    
    private $idClientReserv;    
    private $idPersonReserv;
    private $iNoTable;
    //private $sActifReserv;
   
    
    public function __construct($iNoReserv = 1, $dDateReserv = " ", $hHeureDebutReserv = " ", $hHeureFinReserv = " ", $idClientReserv="", $idPersonReserv="", $iNoTable="")
    {
        $this->setNoReserv($iNoReserv);
        $this->setDateReserv($dDateReserv);
        $this->setHeureDebutReserv($hHeureDebutReserv);
        $this->setHeureFinReserv($hHeureFinReserv);
        $this->setIdClientReserv($idClientReserv);
        $this->setIdPersonReserv($idPersonReserv);
        $this->setNoTable($iNoTable);
        //$this->setNoClient($iNoClient);
        //$this->setActifReserv($sActifReserv);
    }
    
    /**
     * Accesseurs
     */
    public function setNoReserv($iNoReserv)
    {
        if ($iNoReserv != null && $iNoReserv != '')
	{
            //TypeException::estNumerique($iNoReserv);
            $this->iNoReserv = $iNoReserv;
        }
    }

    public function getNoReserv()
    {
        return $this->iNoReserv;
    }

    
    public function setDateReserv($dDateReserv)
    { 
        if ($dDateReserv != null && $dDateReserv != '')
	{
            //TypeException::estString($dDateReserv);
            $this->dDateReserv = $dDateReserv;
        }
    }

    public function getDateReserv()
    {
        return $this->dDateReserv;
    }

    public function setHeureDebutReserv($hHeureDebutReserv)
    {
        if ($hHeureDebutReserv != null && $hHeureDebutReserv != '')
	{
            //TypeException::estString($hHeureDebutReserv);
            $this->hHeureDebutReserv = $hHeureDebutReserv;
        }
    }

    public function getHeureDebutReserv()
    {
        return $this->hHeureDebutReserv;
    }

    public function setHeureFinReserv($hHeureFinReserv)
    {
        //TypeException::estString($dDateEvent);
        $this->hHeureFinReserv = $hHeureFinReserv;
    }

    public function getHeureFinReserv()
    {
        return $this->hHeureFinReserv;
    }

    public function setNoTable($iNoTable)
    {
        if ($iNoTable != null && $iNoTable != '')
	{
            //TypeException::estNumerique($iNoTable);
            $this->iNoTable = $iNoTable;
        }
    }
    
    public function getNoTable()
    {
        return $this->iNoTable;
    }
    
    public function setIdClientReserv($idClientReserv)
    {
        $this->idClientReserv = $idClientReserv;
    }

    public function getIdClientReserv()
    {
        return $this->idClientReserv;
    }

    function getIdPersonReserv() {
        return $this->idPersonReserv;
    }

    function setIdPersonReserv($idPersonReserv) {
        $this->idPersonReserv = $idPersonReserv;
    }

    
    /**
     * @access public
     * ajouter une réservation
     * @return boolean false si l'ajout s'est mal déroulé ou le numéro du id inséré
     */
    public function ajouterUneReservation($idTable)
    {   
         //Connexion à la base de données
         $db = dbConn::getConnection();
        if(empty($this->idClientReserv)){
            $sRequete ="
            INSERT INTO reservation (RES_DATE, RES_HEURE_DEBUT, RES_HEURE_FIN, PERS_ID, TABLES_TAB_ID) 
            VALUES (:resDate, :resHDebut, :resHFin, :resIdPersonne, :resTab)
            ;"; 
            $db = dbConn::getConnection()->prepare($sRequete); 
            $db->bindValue(":resIdPersonne", $this->idPersonReserv );            
            $db->bindValue(":resDate" , $this->dDateReserv);
            $db->bindValue(":resHDebut"  , $this->hHeureDebutReserv);
            $db->bindValue(":resHFin"  , $this->hHeureFinReserv);
            $db->bindValue(":resTab" , $idTable);
            return $db->execute();
        }else{
                   //Requete de ajout d'une réservation
            $sRequete ="
            INSERT INTO reservation (RES_DATE, RES_HEURE_DEBUT, RES_HEURE_FIN, CLIENTS_CLI_ID,TABLES_TAB_ID) 
            VALUES (:resDate, :resHDebut, :resHFin, :resIdClient, :resTab)
            ;";  
            $db = dbConn::getConnection()->prepare($sRequete); 
            $db->bindValue(":resIdClient" , $this->idClientReserv);            
            $db->bindValue(":resDate" , $this->dDateReserv);
            $db->bindValue(":resHDebut"  , $this->hHeureDebutReserv);
            $db->bindValue(":resHFin"  , $this->hHeureFinReserv);
            $db->bindValue(":resTab" , $idTable);
            return $db->execute();          
            }        
    }//fin de la fonction ajouterUneReservation

    
    /**
     * @access public
     * modifie la réservation à partir de son id
     * @return boolean false si la modification s'est mal déroulée ou 
     * @return integer le nombre de lignes modifiées si la modification s'est bien déroulée 
     */
    public function modifierUneReservation() {
        if(empty($this->idClientReserv))
        {
            //Requete de ajout d'une playList
            $sRequete = "
			UPDATE reservation
			SET RES_DATE = :resDate,
                            RES_HEURE_DEBUT = :resHDebut,
                            RES_HEURE_FIN = :resHFin,
                            PERS_ID = :rIdPerson,
                            TABLES_TAB_ID = :resTab
                        WHERE RES_ID = :eveId;
                    "; 
            //Connexion à la base de données
            $db = dbConn::getConnection()->prepare($sRequete);        
            // new data
            $db->bindValue(":resDate" , $this->dDateReserv);
            $db->bindValue(":resHDebut"  , $this->hHeureDebutReserv);
            $db->bindValue(":resHFin"  , $this->hHeureFinReserv);
            $db->bindValue(":rIdPerson", $this->idPersonReserv);
            $db->bindValue(":resTab" , $this->iNoTable);
            $db->bindValue(":eveId" , $this->iNoReserv);
            return $db->execute();
        }
        else{
            //Requete de ajout d'une playList
            $sRequete = "
			UPDATE reservation
			SET RES_DATE = :resDate,
                            RES_HEURE_DEBUT = :resHDebut,
                            RES_HEURE_FIN = :resHFin,
                            CLIENTS_CLI_ID = :rIdClient,
                            TABLES_TAB_ID = :resTab
                        WHERE RES_ID = :eveId;
                    "; 
            //Connexion à la base de données
            $db = dbConn::getConnection()->prepare($sRequete);        
            // new data
            $db->bindValue(":resDate" , $this->dDateReserv);
            $db->bindValue(":resHDebut"  , $this->hHeureDebutReserv);
            $db->bindValue(":resHFin"  , $this->hHeureFinReserv);
            $db->bindValue(":rIdClient" , $this->idClientReserv);
            $db->bindValue(":resTab" , $this->iNoTable);
            $db->bindValue(":eveId" , $this->iNoReserv);
            return $db->execute();            
        }
        //Requete de ajout d'une playList
//        $sRequete = "
//			UPDATE reservation
//			SET RES_DATE = :resDate,
//                            RES_HEURE_DEBUT = :resHDebut,
//                            RES_HEURE_FIN = :resHFin,
//                            CLIENTS_CLI_ID = :rIdClient,
//                            PERS_ID = :rIdPerson,
//                            TABLES_TAB_ID = :resTab
//                        WHERE RES_ID = :eveId;
//                    "; 
        //Connexion à la base de données
            $db = dbConn::getConnection()->prepare($sRequete);        
        // new data
            $db->bindValue(":resDate" , $this->dDateReserv);
            $db->bindValue(":resHDebut"  , $this->hHeureDebutReserv);
            $db->bindValue(":resHFin"  , $this->hHeureFinReserv);
            $db->bindValue(":rIdClient" , $this->idClientReserv);
            $db->bindValue(":rIdPerson", $this->idPersonReserv);
            $db->bindValue(":resTab" , $this->iNoTable);
            $db->bindValue(":eveId" , $this->iNoReserv);
            return $db->execute();
        
    }// fin de la function modifierUneReservation

    
    /**
     * @access public
     * supprime une réservation par son id
     * @return boolean false si la suppression s'est mal déroulée ou 
     * @return integer le nombre de lignes supprimées si la suppression s'est bien déroulée 
     */
    public function supprimerUneReservation() {
    //Requete de suppression d'un evenement identifiée par son iNoEvenement
        $sRequete = "
			DELETE FROM reservation
			WHERE RES_ID = :resId;";
    //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);

    // new data
        $db->bindValue(":resId" , $this->iNoReserv);

    //Exécuter la requête
    echo $this->iNoReserv;
        return $db->execute();

    }// fin de la fonction supprimerUneReservation

    
    /**
     * @access public
     * recherche une réservation par son id
     * @return boolean true si la recherche est fructueuse false sinon
     */
    public function rechercherReservation()
    {
    //Réaliser la requête de recherche
        $sRequete = "SELECT * FROM reservation WHERE RES_ID = :resId";
        
    //Connexion à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        $db->bindValue(":resId" , $this->iNoReserv);
        $db -> execute();

    //Exécuter la requête
        $db->setFetchMode(PDO::FETCH_ASSOC);
        $aReservation = $db ->fetchall();
        
        if ($aReservation != false) {
            //Récupérer le tableau des enregistrements s'il existe
            $aoReservations = array();

            if (empty($aReservation[0]) != true) {
            //Affecter les propriétés de l'objet en cours avec les valeurs
                $this->setNoReserv($aReservation[0]['RES_ID']);
                $this->setDateReserv($aReservation[0]['RES_DATE']);
                $this->setHeureDebutReserv($aReservation[0]['RES_HEURE_DEBUT']);
                $this->setHeureFinReserv($aReservation[0]['RES_HEURE_FIN']);
                $this->setIdClientReserv($aReservation[0]['CLIENTS_CLI_ID']);
                $this->setIdPersonReserv($aReservation[0]['PERS_ID']);
                $this->setNoTable($aReservation[0]['TABLES_TAB_ID']);

                return true;
            }
            return false;
        }
    }
//fin de la fonction rechercherReservation

    /**
     * @access public
     * recherche une réservation par date
     * @return boolean true si la recherche est fructueuse false sinon
     */
    public function rechercherReservationByDate()
    {
    //Réaliser la requête de recherche par le sDescriptEvent
        $sRequete = "SELECT * FROM reservation
			WHERE RES_DATE LIKE('%:resDate%');";
        
    //Connecter à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        $db->bindValue(":resDate" , $this->dDateReserv);

    //Exécuter la requête
        $db -> execute();		
        $db->setFetchMode(PDO::FETCH_OBJ);
        $aReservation = $db ->fetchall();
        
        if ($aReservation != false) {
        //Récupérer le tableau des enregistrements s'il existe
        $aoReservations = array();

            if (empty($aReservation[0]) != true) {
            //Affecter les propriétés de l'objet en cours avec les valeurs
                $this->setNoReserv($aReservation[0]['RES_ID']);
                $this->setDateReserv($aReservation[0]['RES_DATE']);
                $this->setHeureDebutReserv($aReservation[0]['RES_HEURE_DEBUT']);
                $this->setHeureFinReserv($aReservation[0]['RES_HEURE_FIN']);
                $this->setIdClientReserv($aReservation[0]['CLIENTS_CLI_ID']);
                $this->setIdPersonReserv($aReservation[0]['PERS_ID']);
                $this->setNoTable($aReservation[0]['TABLES_TAB_ID']);
                
            //retourner true
            return true;
            }
        return false;
        }
    }// fin de la fonction rechercherReservationByDate   
    
    /**
     * @access public
     * recherche une réservation par l'heure début
     * @return boolean true si la recherche est fructueuse false sinon
     */
    public function rechercherReservationByHeureDebut()
    {
    //Réaliser la requête de recherche par le sDescriptEvent
        $sRequete = "SELECT * FROM reservation
			WHERE RES_HEURE_DEBUT LIKE('%:resHDebut');";
        
    //Connecter à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);
        $db->bindValue(":resHDebut" , $this->hHeureDebutReserv);

    //Exécuter la requête
        $db -> execute();		
        $db->setFetchMode(PDO::FETCH_OBJ);
        $aReservation = $db ->fetchall();
        
        if ($aReservation != false) {
        //Récupérer le tableau des enregistrements s'il existe
        $aoReservations = array();

            if (empty($aReservation[0]) != true) {
            //Affecter les propriétés de l'objet en cours avec les valeurs
                $this->setNoReserv($aReservation[0]['RES_ID']);
                $this->setDateReserv($aReservation[0]['RES_DATE']);
                $this->setHeureDebutReserv($aReservation[0]['RES_HEURE_DEBUT']);
                $this->setHeureFinReserv($aReservation[0]['RES_HEURE_FIN']);
                $this->setIdClientReserv($aReservation[0]['CLIENTS_CLI_ID']);
                $this->setIdPersonReserv($aReservation[0]['PERS_ID']);
                $this->setNoTable($aReservation[0]['TABLES_TAB_ID']);

            //retourner true
            return true;
            }
        return false;
        }
    }// fin de la fonction rechercherReservationByHeureDebut

    
    /**
     * @access public
     * recherche tous les réservations dans la base de données
     * @return array d'objets de type Reservation
     */
    public static function rechercherToutesLesReservations() {
    //Réaliser la requête de recherche de toutes les chansons
        $sRequete = "SELECT * FROM reservation;";
            
    //Connecter à la base de données
        $db = dbConn::getConnection()->prepare($sRequete);

    //Exécuter la requête
        $aReservations = dbConn::recuperer($sRequete);
        
        if ($aReservations != false) {
        //Récupérer le tableau des enregistrements s'il existe
            $aoReservations = array();

            for ($i = 0; $i < count($aReservations); $i++)
            {
                $aoReservations[$i] = new Reservation($aReservations[$i]['RES_ID'], $aReservations[$i]['RES_DATE'], $aReservations[$i]['RES_HEURE_DEBUT'], $aReservations[$i]['RES_HEURE_FIN'], $aReservations[$i]['CLIENTS_CLI_ID'], $aReservations[$i]['PERS_ID'],$aReservations[$i]['TABLES_TAB_ID']);
            }

    //retourner le tableau de toutes les chansons
            return $aoReservations;
        }
        return false;
    }//fin de la fonction rechercherToutesLesReservations() 

    /**
     * @access public
     * recherche tous les réservations dans la base de données
     * @return array d'objets de type Reservation
     */
    public function rechercherToutesLesReservationsParClient() {
    //Réaliser la requête de recherche de toutes les chansons
        $sRequete = "SELECT * FROM reservation WHERE CLIENTS_CLI_ID = :rIdClient;";
//        echo  $_SESSION['idClient'] ;
    //Connecter à la base de données
        $id=3;
        $db = dbConn::getConnection()->prepare($sRequete);
        $db->bindValue(":rIdClient" , $this->idClientReserv);
         $db -> execute();
            //Récupérer le tableau des enregistrements s'il existe
            $db->setFetchMode(PDO::FETCH_ASSOC);
            $aReservations = $db ->fetchall();
		if($aReservations != false){			
                    $aoReservations = array();			
                    for($i=0; $i < count($aReservations); $i++){
                            $aoReservations[$i] = new Reservation($aReservations[$i]['RES_ID'], $aReservations[$i]['RES_DATE'], $aReservations[$i]['RES_HEURE_DEBUT'],$aReservations[$i]['RES_HEURE_FIN'],$aReservations[$i]['CLIENTS_CLI_ID'],$aReservations[$i]['PERS_ID'],$aReservations[$i]['TABLES_TAB_ID']);
                    }
                    return $aoReservations;			
		}
		return false;
        }//fin de la fonction rechercherToutesLesReservationsParClient() 

}

?>