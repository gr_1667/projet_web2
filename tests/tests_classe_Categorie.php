<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">		
        <title>tests_classe_Categorie</title>
        <meta name="description" content="">
        <meta name="author" content="cmartin;mpolerca">		
    </head>
    <!--/* 
     * @author Polerca Mihai
     * @version 2015-06-25
     * 582_N61_MA_Projet_Web_2 
     *  Resto Projet Web 2
     */-->
    <body>
        <div>
            <header>
                    <h1>tests_classe_Categorie</h1>
            </header>
            <div>
            <?php
            /* include des fichiers de librairie */
            require("../lib/MySqliLib.class.php");
            require("../lib/dbConn.class.php");
            require("../lib/TypeException.class.php");                
            /* include des fichiers modeles */
            require("../modeles/Categorie.class.php");
            ?>
                
            <h2><hr>setIdCategorie() avec ""</h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie("","a","a");
                 echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
                echo "<p>Le id de categorie est: ".$oCategorie->getIdCategorie()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
            
            <h2><hr>setIdCategorie() avec 3</h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie(3,"a","a");
                 echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
                echo "<pre> Le id de categorie est: ".$oCategorie->getIdCategorie()."</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
            <h2><hr>setIdCategorie() avec "text"</h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie();
                $oCategorie ->setIdCategorie("text");
                 echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
                echo "<pre> Le id de categorie est: ".$oCategorie->getIdCategorie()."</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>   

            <h2><hr>setNomCategorie() avec "Nom Categorie"</h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie(1,"Nom Categorie","descript");
                 echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
                //$oCategorie->setSTitreCategorie("abc");
                echo "<p>Le nom de categorie est: ".$oCategorie->getNomCategorie()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            } 
            ?>  

             <h2><hr>setNomCategorie() avec ""</h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie(1,"","");
                 echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
                //$oCategorie->setSTitreCategorie("abc");
                echo "<p> Le nom de categorie est: ".$oCategorie->getNomCategorie()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            } 
            ?>
              <h2><hr>setNomCategorie() avec 4</h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie();
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
                $oCategorie->setNomCategorie(4);
                echo "<p>  Le nom de categorie est: ".$oCategorie->getNomCategorie()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            } 
            ?>

       
            <h2><hr>setNomCategorie() avec "Diner" </h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie();
                $oCategorie -> setNomCategorie("Diner");
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";

                //$oCategorie->setSUrlCategorie("");
                echo "<p>La description de categorie est: ".$oCategorie->getNomCategorie()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
            <h2><hr>setNomCategorie() avec 173 </h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie();
                $oCategorie -> setNomCategorie(173);
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";

                //$oCategorie->setSUrlCategorie("");
                echo "<p>La description de categorie est: ".$oCategorie->getNomCategorie()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>            
           
            <h2><hr>__construct() avec un paramètre - $idCategorie</h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie(45);
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }  
            ?>
            <h2><hr>__construct() avec deux paramètres - $idCategorie et $sNomCategorie</h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie(45, "a");
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }  
            ?>
            <h2><hr>__construct() avec trois paramètres - $idCategorie, $sNomCategorie et $sActifCategorie</h2>
            <?php
            try{
                /* Instancier un objet de la classe Categorie */
                $oCategorie = new Categorie(45, "nom", "a");
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }  
            ?>

            <h2><hr>ajouterUnCategorie() avec setNomCategorie() = "BBQ"</h2>
            <?php 
            try{
                $oCategorie = new Categorie();
                $oCategorie -> setNomCategorie("Diner");               
//                echo $oCategorie -> getNomCategorie();
//                $iIdInsere = $oCategorie->ajouterUnCategorie();
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
                echo "<pre>";
//                var_dump($iIdInsere);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            <h2><hr>modifierUnCategorie() avec $sNomCategorie = "Diner"</h2>
            <?php 
                try{
                    
                    $oCategorie = new Categorie();
                    $oCategorie ->setIdCategorie(3);
                    $oCategorie ->rechercherCategorie();                    
                    $oCategorie2 = new Categorie($oCategorie ->getIdCategorie(), $oCategorie ->getNomCategorie(), $oCategorie ->getActifCategorie());
                    $oCategorie2 ->setNomCategorie("Dejeuners");                    
                    $bResult = $oCategorie2->modifierUneCategorie();

                    echo "<pre>";   
                    var_dump($bResult);
                    echo "</pre>";
                }catch(Exception $oExcep){
                    echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
                }
            ?>

            <h2><hr>supprimerUnCategorie() </h2>
            <?php 
                try{
                    $oCategorie = new Categorie(4);

                    $bResult = $oCategorie->supprimerUneCategorie();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                }catch(Exception $oExcep){
                    echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
                }
            ?>

            <h2><hr>rechercherCategorie() avec $idCategorie = 17 N'EXISTE PAS</h2>
            <?php 
            try{
                $oCategorie= new Categorie(17,"a","a");

                $bResult = $oCategorie->rechercherCategorie();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherCategorie() avec $idCategorie = 5 EXISTE </h2>
            <?php 
            try{
                $oCategorie= new Categorie();
                $oCategorie ->setIdCategorie(5);
                $bResult = $oCategorie->rechercherCategorie();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherCategorie() avec $sNomCategorie = "OMG" N'EXISTE PAS </h2>
            <?php 
            try{
                $oCategorie= new Categorie();
                $oCategorie ->setNomCategorie("OMG");

                $bResult = $oCategorie->rechercherCategorieByNom();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherCategorie() avec $sNomCategorie = "Diner" EXISTE </h2>
            <?php 
            try{
                $oCategorie= new Categorie();
                $oCategorie ->setNomCategorie("Diner");
                $bResult = $oCategorie->rechercherCategorieByNom();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherAllCategorie()</h2>
            <?php 
            try{
                $oCategorie= new Categorie();

                $bResult = $oCategorie->rechercherAllCategories();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
                print_r($bResult);
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2><hr>rechercherCategoriesByNom()</h2>
            <?php 
            try{
                $oCategorie= new Categorie();
                $oCategorie ->setNomCategorie("Diner");
                $bResult = $oCategorie->rechercherCategoriesByNom();
                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oCategorie);
                echo "</pre>";
                print_r($bResult);
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            </div>
            <footer>
                <p>
                    &copy; Copyright  by mpolerca
                </p>
            </footer>
        </div>
    </body>
</html>


