<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">		
        <title>tests_classe_Produit</title>
        <meta name="description" content="">
        <meta name="author" content="mpolerca">		
    </head>
    <!--/* 
     * @author Polerca Mihai
     * @version 2015-06-25
     * 582_N61_MA_Projet_Web_2 
     *  Resto Projet Web 2
     */-->
    <body>
        <div>
            <header>
                    <h1>tests_classe_PDO</h1>
            </header>
            <div>
            <?php
            /* include des fichiers de librairie */
            require("../lib/MySqliLib.class.php");
            require("../lib/TableauLib.class.php");
            require("../lib/dbConn.class.php");
            require("../lib/TypeException.class.php");                
            /* include des fichiers modeles */       
            ?>          
            <h2><hr>Création d'une connexion avec PDO</h2>
           <?php
           /*
                $db = dbConn::getConnection(); // connexion à la BDD
                               
                $resultats = $db->prepare('SELECT * FROM categories');
                $resultats->execute();                
                $resultats->setFetchMode(PDO::FETCH_OBJ);
                $ligne = $resultats ->fetchall();
                print_r($ligne);
            * 
            */
            $sRequete = " SELECT * FROM categories";
            $db = dbConn::getConnection(); // connexion à la BDD
            $aRes = dbConn::executer($sRequete);
            print_r($aRes);
            
            ?>
            </div>
            <footer>
                <p>
                    &copy; Copyright  by cmartin et mpolerca
                </p>
            </footer>
        </div>
    </body>
</html>


