<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">		
        <title>tests_classe_Produit</title>
        <meta name="description" content="">
        <meta name="author" content="cmartin;mpolerca">		
    </head>
    <!--/* 
     * @author Polerca Mihai
     * @version 2015-06-25
     * 582_N61_MA_Projet_Web_2 
     *  Resto Projet Web 2
     */-->
    <body>
        <div>
            <header>
                    <h1>tests_classe_Produit</h1>
            </header>
            <div>
            <?php
            /* include des fichiers de librairie */
            require("../lib/MySqliLib.class.php");
            require("../lib/dbConn.class.php");
            require("../lib/TypeException.class.php");                
            /* include des fichiers modeles */
            require("../modeles/Produit.class.php");
            ?>
                
            <h2><hr>setIdProduit() avec ""</h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit("","a","a");
                 echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
                echo "<p>Le id de produit est: ".$oProduit->getIdProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
            
            <h2><hr>setIdProduit() avec 3</h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit(3,"a","a");
                 echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
                echo "<pre> Le id de produit est: ".$oProduit->getIdProduit()."</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
            <h2><hr>setIdProduit() avec "text"</h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit ->setIdProduit("text");
                 echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
                echo "<pre> Le id de produit est: ".$oProduit->getIdProduit()."</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>   

            <h2><hr>setNomProduit() avec "Nom Produit"</h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit(1,"Nom Produit","descript");
                 echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
                //$oProduit->setSTitreProduit("abc");
                echo "<p>Le nom de produit est: ".$oProduit->getNomProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            } 
            ?>  

             <h2><hr>setNomProduit() avec ""</h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit(1,"","");
                 echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
                //$oProduit->setSTitreProduit("abc");
                echo "<p> Le nom de produit est: ".$oProduit->getNomProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            } 
            ?>
              <h2><hr>setNomProduit() avec 4</h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
                $oProduit->setNomProduit(4);
                echo "<p>  Le nom de produit est: ".$oProduit->getNomProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            } 
            ?>

            <h2><hr>setDescProduit() avec "" </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit -> setDescProduit("");
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>La description de produit est: ".$oProduit->getDescProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>

            <h2><hr>setDescProduit() avec "desc" </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit -> setDescProduit("desc");
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>La description de produit est: ".$oProduit->getDescProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
            <h2><hr>setDescProduit() avec 173 </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit -> setDescProduit(173);
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>La description de produit est: ".$oProduit->getDescProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?> 
            <h2><hr>setPrixProduit() avec null </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit -> setPrixProduit(0);
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>Le prix est: ".$oProduit->getPrixProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>           
            <h2><hr>setPrixProduit() avec 2.99 </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit -> setPrixProduit(2.99);
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>Le prix est: ".$oProduit->getPrixProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?> 
             <h2><hr>setPrixProduit() avec "text" </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit -> setPrixProduit("text");
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>Le prix est: ".$oProduit->getPrixProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?> 
            <h2><hr>setImageProduit() avec "" </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit ->setImageProduit("");
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>Le url de produit est: ".$oProduit->getImageProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
             <h2><hr>setImageProduit() avec "url" </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit ->setImageProduit("url");
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>Le url de produit est: ".$oProduit->getImageProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
             <h2><hr>setImageProduit() avec 173 </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit ->setImageProduit(173);
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>Le url de produit est: ".$oProduit->getImageProduit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?> 
            <h2><hr>setCat_id_Produit() avec "" </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit ->setCat_id_Produit("");
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>Le categorie de produit est: ".$oProduit->getCat_id_Produit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
            <h2><hr>setCat_id_Produit() avec 12 </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit ->setCat_id_Produit(12);
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>Le categorie de produit est: ".$oProduit->getCat_id_Produit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?> 
            <h2><hr>setCat_id_Produit() avec "text" </h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit();
                $oProduit ->setCat_id_Produit("text");
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";

                //$oProduit->setSUrlProduit("");
                echo "<p>Le categorie de produit est: ".$oProduit->getCat_id_Produit()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?> 
            <h2><hr>__construct() avec un paramètre - $idProduit</h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit(45);
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }  
            ?>
            <h2><hr>__construct() avec deux paramètres - $idProduit et $sNomProduit</h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit(45, "a");
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }  
            ?>
            <h2><hr>__construct() avec trois paramètres - $idProduit, $sNomProduit et $sDescriptionProduit</h2>
            <?php
            try{
                /* Instancier un objet de la classe Produit */
                $oProduit = new Produit(45, "nom", "description");
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }  
            ?>

            <h2><hr>ajouterUnProduit() avec setNomProduit() = "BBQ"</h2>
            <?php 
            try{
                $oProduit = new Produit();
                $oProduit -> setNomProduit("BBQ");
                $oProduit -> setDescProduit("Delicieux");
                $oProduit -> setImageProduit("urlImage");
                $oProduit -> setPrixProduit(3.99);
                $oProduit ->setCat_id_Produit(2);
//                echo $oProduit -> getNomProduit();
//                $iIdInsere = $oProduit->ajouterUnProduit();
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
                echo "<pre>";
//                var_dump($iIdInsere);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            <h2><hr>modifierUnProduit() avec $sImageProduit = "lasagne_url.png"</h2>
            <?php 
                try{
                    
                    $oProduit = new Produit();
                    $oProduit ->setIdProduit(5);
                    $oProduit ->rechercherProduit();                    
                    $oProduit2 = new Produit($oProduit ->getIdProduit(), $oProduit ->getNomProduit(), $oProduit ->getDescProduit(), $oProduit ->getPrixProduit(), $oProduit ->getImageProduit(), $oProduit ->getActifProduit(), $oProduit ->getCat_id_Produit());
                    $oProduit2 ->setNomProduit("Poutine");
                    //en commentaire pour ne pas modifier les donnees;
//                    $oProduit2 ->setImageProduit("bordeaux_url.png");
//                    $oProduit2 ->setDescProduit("vin rouge sec");
//                    $oProduit2 ->setPrixProduit(13.99);
                    $oProduit2 ->setCat_id_Produit(1);
                    $bResult = $oProduit2->modifierUnProduit();

                    echo "<pre>";   
                    var_dump($bResult);
                    echo "</pre>";
                }catch(Exception $oExcep){
                    echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
                }
            ?>

            <h2><hr>supprimerUnProduit() </h2>
            <?php 
                try{
                    $oProduit = new Produit(6);

                    $bResult = $oProduit->supprimerUnProduit();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                }catch(Exception $oExcep){
                    echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
                }
            ?>

            <h2><hr>rechercherProduit() avec $idProduit = 17 N'EXISTE PAS</h2>
            <?php 
            try{
                $oProduit= new Produit(17,"a","a");

                $bResult = $oProduit->rechercherProduit();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherProduit() avec $idProduit = 5 EXISTE </h2>
            <?php 
            try{
                $oProduit= new Produit();
                $oProduit ->setIdProduit(5);
                $bResult = $oProduit->rechercherProduit();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherProduit() avec $sNomProduit = "OMG" N'EXISTE PAS </h2>
            <?php 
            try{
                $oProduit= new Produit();
                $oProduit ->setNomProduit("OMG");

                $bResult = $oProduit->rechercherProduitByNom();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherProduit() avec $sNomProduit = "BBQ" EXISTE </h2>
            <?php 
            try{
                $oProduit= new Produit();
                $oProduit ->setNomProduit("BBQ");
                $bResult = $oProduit->rechercherProduitByNom();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherAllProduit()</h2>
            <?php 
            try{
                $oProduit= new Produit();

                $bResult = $oProduit->rechercherAllProduits();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
//                var_dump($oProduit);
                echo "</pre>";
//                print_r($bResult);
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2><hr>rechercherProduitsByNom()</h2>
            <?php 
            try{
                $oProduit= new Produit();
                $oProduit ->setNomProduit("BBQ");
                $bResult = $oProduit->rechercherProduitsByNom();
                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
                print_r($bResult);
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            <h2><hr>rechercherProduitsByPrix()</h2>
            <?php 
            try{
                $oProduit= new Produit();
                $min = 5.98;
                $max = 14.00;
                $bResult = $oProduit->rechercherProduitsByPrix($min,$max);
                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oProduit);
                echo "</pre>";
                print_r($bResult);
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            </div>
            <footer>
                <p>
                    &copy; Copyright  by cmartin et mpolerca
                </p>
            </footer>
        </div>
    </body>
</html>


