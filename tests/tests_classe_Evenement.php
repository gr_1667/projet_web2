<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">		
        <title>test_classe_Evenement</title>
        <meta name="description" content="">
        <meta name="author" content="">		
    </head>
    <!--/* 
     * @author Caroline Martin, Polerca Mihai
     * @version 2015-02-11
     * 582-P41-MA Programmation Web Dynamique III 
     *  Spotify Volet 1
     */-->
    
    <body>
        <div>
            <header>
                    <h1>test_classe_Evenement</h1>
            </header>
            <div>
            <?php
            /* include des fichiers de librairie */
            include("../lib/MySqliLib.class.php");
            include("../lib/TypeException.class.php");                
            /* include des fichiers modeles */
            include("../modeles/Evenement.class.php");
            ?>
                
            <h2>setNoEvenement() avec " "</h2>
            <?php
            try
            {
                $oEvenement = new Evenement();
                $oEvenement->setNoEvenement(" ");
                $iIdInsere = $oEvenement->ajouterUnEvenement();

                echo "<pre>";
                var_dump($iIdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2>setNoEvenement() avec 3</h2>
            <?php
            try
            {
                $oEvenement = new Evenement();
                $oEvenement->setNoEvenement(2);
                $iIdInsere = $oEvenement->ajouterUnEvenement();

                echo "<pre>";
                var_dump($iIdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2>setDescriptEvent() avec ""</h2>
            <?php
            try
            {
                $oEvenement = new Evenement();
                $oEvenement->setDescriptEvent("");
                $sIdInsere = $oEvenement->ajouterUnEvenement();

                echo "<pre>";
                var_dump($sIdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2>setDescriptEvent() avec "Description de l'événement"</h2>
            <?php
            try
            {
                $oEvenement = new Evenement();
                $oEvenement->setDescriptEvent("Description de l'événement");
                $sIdInsere = $oEvenement->ajouterUnEvenement();

                echo "<pre>";
                var_dump($sIdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2>setDescriptEvent() avec 4</h2>
            <?php
            try
            {
                $oEvenement = new Evenement();
                $oEvenement->setDescriptEvent(4);
                $sIdInsere = $oEvenement->ajouterUnEvenement();

                echo "<pre>";
                var_dump($sIdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2>setImgEvenement() avec "" </h2>
            <?php
            try
            {
                $oEvenement = new Evenement();
                $oEvenement->setImgEvenement(4);
                $sIdInsere = $oEvenement->ajouterUnEvenement();

                echo "<pre>";
                var_dump($sIdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2>setImgEvenement() avec "url"</h2>
            <?php
            try
            {
                $oEvenement = new Evenement();
                $oEvenement->setImgEvenement("../site/medias/musique.png");
                $sIdInsere = $oEvenement->ajouterUnEvenement();

                echo "<pre>";
                var_dump($sIdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2>setImgEvenement() avec 007</h2>
            <?php
            try
            {
                $oEvenement = new Evenement();
                $oEvenement->setImgEvenement(007);
                $sIdInsere = $oEvenement->ajouterUnEvenement();

                echo "<pre>";
                var_dump($sIdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }  
            ?>
            
            <h2>__construct() avec un paramètre - $iNoEvenement</h2>
            <?php
            try
            {
                $oEvenement = new Evenement(123);
                /*$oEvenement->setNoEvenement(35);*/
                $iIdInsere = $oEvenement->ajouterUnEvenement();

                echo "<pre>";
                var_dump($iIdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2>__construct() avec deux paramètres - $iNoEvenement et $sDescriptEvent</h2>
            <?php
            try{
                $oEvenement = new Evenement(123,"a");
                $IdInsere = $oEvenement->ajouterUnEvenement();
            
                echo "<pre>";
                var_dump($IdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            } 
            ?>
            
            <h2>__construct() avec trois paramètres - $iNoEvenement, $sDescriptEvent et $sImgEvent</h2>
            <?php
            try{
                $oEvenement = new Evenement(123,"a","b");
                /*$oEvenement->setDescriptEvent("La description");
                $oEvenement->setImgEvenement("../bonsoir");*/
                $IdInsere = $oEvenement->ajouterUnEvenement();
            
                echo "<pre>";
                var_dump($IdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            } 
            ?>
            
            <h2>ajouterUnEvenement() avec setDescriptEvent = "Tango"</h2>
            <?php 
            try{
                $oEvenement = new Evenement();
                $oEvenement->setDescriptEvent("Tango");
                $IdInsere = $oEvenement->ajouterUnEvenement();
            
                echo "<pre>";
                var_dump($IdInsere);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            } 
            ?>
            
            <h2>modifierUnEvenement() avec setDescriptEvent = "Kuduro"</h2>
            <?php 
            try{
                $oEvenement = new Evenement($iIdInsere, "Kuduro");
                /*$oEvenement->setDescriptEvent("Kuduro");*/
                $IdModifie = $oEvenement->modifierUnEvenement();
            
                echo "<pre>";
                var_dump($IdModifie);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            } 
            ?>

            <h2>supprimerUnEvenement() avec $iNoEvenement = "250"</h2>
            <?php 
            try{
                $oEvenement = new Evenement(250);
                $IdSuprime = $oEvenement->supprimerUnEvenement();

                echo "<pre>";
                var_dump($IdSuprime);
                echo "</pre>";
            }
            catch(Exception $oExcep)
            {
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2>rechercherEvenement() avec $iNoEvenement = 17 N'EXISTE PAS</h2>
            <?php
            try
            {
                $oEvenement = new Evenement(17, "a", "a");
                $IdRecherche = $oEvenement->rechercherEvenement();

                echo "<pre>";
                var_dump($IdRecherche);
                echo "</pre>";
            }
            catch (Exception $oExcep)
            {
                echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
            }
            ?>

            <h2>rechercherEvenement() avec $iNoEvenement = 450 EXISTE </h2>
            <?php
            try {
                $oEvenement = new Evenement(450, "a", "a");
                $IdRecherche = $oEvenement->rechercherEvenement();

                echo "<pre>";
                var_dump($IdRecherche);
                echo "</pre>";
                
                print_r($oEvenement);
            }
            catch (Exception $oExcep)
            {
                echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
            }
            ?>

            <h2>rechercherEvenement() avec $sDescriptEvent = "test" N'EXISTE PAS </h2>
            <?php
            try {
                $oEvenement = new Evenement();
                $oEvenement->setDescriptEvent("test"); 
                $IdRecherche = $oEvenement->rechercherEvenementByDescription();

                echo "<pre>";
                var_dump($IdRecherche);
                echo "</pre>";
                
                print_r($oEvenement);
            }
            catch (Exception $oExcep)
            {
                echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
            }
            ?>
            
            <h2>rechercherEvenement() avec $sDescriptEvent = "Tango" EXISTE </h2>
            <?php
            try {
                $oEvenement = new Evenement();
                $oEvenement->setDescriptEvent("Tango");
                $IdRecherche = $oEvenement->rechercherEvenementByDescription();

                echo "<pre>";
                var_dump($IdRecherche);
                echo "</pre>";
                
                print_r($oEvenement);
            }
            catch (Exception $oExcep)
            {
                echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
            }
            ?>

            <h2>rechercherTousLesEvenements()</h2>
            <?php
            try {
                $oEvenement = new Evenement();
                $IdRecherche = $oEvenement->rechercherTousLesEvenements();

                echo "<pre>";
                var_dump($IdRecherche);
                echo "</pre>";
                echo "<pre>";
                var_dump($oEvenement);
                echo "</pre>";  
            }
            catch (Exception $oExcep)
            {
                echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
            }
            ?> 
           
            </div>
            <footer>
                <p>
                    &copy; Copyright  by cmartin et dferreira
                </p>
            </footer>
        </div>
    </body>
</html>


