<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">		
        <title>tests_classe_Administrateur</title>
        <meta name="description" content="">
    </head>
    <!--/* 
     * @author Marcio Pelegrini
     * @version 2015-06-25
     * 582_N61_MA_Projet_Web_2 
     *  Resto Projet Web 2
     */-->
    <body>
        <div>
            <header>
                <h1>Test de la classe Administrateur</h1>
            </header>
            <div>
                <?php
                /* include des fichiers de librairie */
                require("../lib/MySqliLib.class.php");
                require("../lib/TypeException.class.php");
                /* include des fichiers modeles */
                require("../modeles/Administrateur.class.php");
                ?>

                <h2>setIdAdministrateur() avec ""</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Administrateur */
                    $oAdministrateur = new Administrateur("", "a", "a", "a", "a", "a");
                    echo "<pre>";
                    var_dump($oAdministrateur);
                    echo "</pre>";
                    echo "<p>Le id du administrateur est: " . $oAdministrateur->getIdAdm() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>

                <h2>setIdAdministrateur() avec 3</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Administrateur */
                    $oAdministrateur = new Administrateur(3, "a", "a");
                    echo "<pre>";
                    var_dump($oAdministrateur);
                    echo "</pre>";
                    echo "<pre> Le id du administrateur est: " . $oAdministrateur->getIdAdm() . "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>   

                <h2>setNomAdministrateur() avec "Nom Administrateur"</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Administrateur */
                    $oAdministrateur = new Administrateur(1, "Email Administrateur", "Nom Administrateur", "Prenom");
                    echo "<pre>";
                    var_dump($oAdministrateur);
                    echo "</pre>";
                    //$oAdministrateur->setSTitreAdministrateur("abc");
                    echo "<p>Le nom de produit est: " . $oAdministrateur->getSNomAdm() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>  

                <h2>setNomAdministrateur() avec ""</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Administrateur */
                    $oAdministrateur = new Administrateur(1, "", "", "");
                    echo "<pre>";
                    var_dump($oAdministrateur);
                    echo "</pre>";
                    //$oAdministrateur->setSTitreAdministrateur("abc");
                    echo "<p> Le nom du administrateur est: " . $oAdministrateur->getSNomAdm() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>
                <h2>setNomAdministrateur() avec 4</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Administrateur */
                    $oAdministrateur = new Administrateur();
                    echo "<pre>";
                    var_dump($oAdministrateur);
                    echo "</pre>";
                    $oAdministrateur->setSNomAdm(4);
                    echo "<p>  Le nom de produit est: " . $oAdministrateur->getSNomAdm() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>          
                <h2>ajouterUnAdministrateur() avec setSNomAdministrateur = "Champlain"</h2>
                <?php
                try {
                    $oAdministrateur = new Administrateur();
                    $oAdministrateur->setSEmailAdm("Champlain@test.com");
                    $oAdministrateur->setSNomAdm("Champlain");

                    $iIdInsere = $oAdministrateur->ajouterUnAdministrateur();
                    echo "<pre>";
                    var_dump($oAdministrateur);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($iIdInsere);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>
                <h2>modifierUnAdministrateur() avec setSNomAdministrateur = "Drapeau"</h2>
                <?php /*
                try {
                    $oAdministrateur = new Administrateur(1, "Champlain@test.com", "Drapeau");

                    $bResult = $oAdministrateur->modifierUnAdministrateur();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }*/
                ?>
                <h2>supprimerUnAdministrateur() </h2>
                <?php /*
                try {
                    $oAdministrateur = new Administrateur(15);

                    $bResult = $oAdministrateur->supprimerUnAdministrateur();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }*/
                ?>

                <h2>rechercherAdministrateur() avec $idAdministrateur = 17 N'EXISTE PAS</h2>
                <?php
                try {
                    $oAdministrateur = new Administrateur(17, "a", "a");

                    $bResult = $oAdministrateur->rechercherUnAdministrateur();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($oAdministrateur);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>

                <h2>rechercherAdministrateur() avec $idAdministrateur = 1 EXISTE </h2>
                <?php
                try {
                    $oAdministrateur = new Administrateur(1, "a", "a");

                    $bResult = $oAdministrateur->rechercherUnAdministrateur();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($oAdministrateur);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>

                <h2>rechercherAdministrateur() avec $sNomAdministrateur = "asa" N'EXISTE PAS </h2>
                <?php
                try {
                    $oAdministrateur = new Administrateur(1, "a", "asa");

                    $bResult = $oAdministrateur->rechercherAdministrateurParNom();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($oAdministrateur);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>

                <h2>rechercherTousAdministrateur()</h2>
                <?php
                try {
                    $oAdministrateur = new Administrateur();

                    $bResult = $oAdministrateur->rechercherTousAdministrateurs();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($oAdministrateur);
                    echo "</pre>";
                    print_r($bResult);
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>          
            </div>
            <footer>
                <p>
                    &copy; Copyright  by 
                </p>
            </footer>
        </div>
    </body>
</html>