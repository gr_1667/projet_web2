<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">		
        <title>tests_classe_Table</title>
        <meta name="description" content="">
        <meta name="author" content="cmartin;mpolerca">		
    </head>
    <!--/* 
     * @author Polerca Mihai
     * @version 2015-06-25
     * 582_N61_MA_Projet_Web_2 
     *  Resto Projet Web 2
     */-->
    <body>
        <div>
            <header>
                    <h1>tests_classe_Table</h1>
            </header>
            <div>
            <?php
            /* include des fichiers de librairie */
            require("../lib/MySqliLib.class.php");
            require("../lib/TypeException.class.php");                
            /* include des fichiers modeles */
            require("../modeles/Table.class.php");
            ?>
                
            <h2><hr>setIdTable() avec ""</h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table("","a","a");
                 echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
                echo "<p>Le id de table est: ".$oTable->getIdTable()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
            
            <h2><hr>setIdTable() avec 3</h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table(3,"a","a");
                 echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
                echo "<pre> Le id de table est: ".$oTable->getIdTable()."</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
            <h2><hr>setIdTable() avec "text"</h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table();
                $oTable ->setIdTable("text");
                 echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
                echo "<pre> Le id de table est: ".$oTable->getIdTable()."</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>   

            <h2><hr>setCapacTable() avec "capac Table"</h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table(1,"capac Table","descript");
                 echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
                //$oTable->setSTitreTable("abc");
                echo "<p>Le nom de table est: ".$oTable->getCapacTable()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            } 
            ?>  

             <h2><hr>setCapacTable() avec ""</h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table(1,"","");
                 echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
                //$oTable->setSTitreTable("abc");
                echo "<p> Le nom de table est: ".$oTable->getCapacTable()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            } 
            ?>
              <h2><hr>setCapacTable() avec 4</h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table();
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
                $oTable->setCapacTable(4);
                echo "<p>  Le nom de table est: ".$oTable->getCapacTable()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            } 
            ?>

            <h2><hr>setActifTable() avec "" </h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table();
                $oTable -> setActifTable("");
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";

                //$oTable->setSUrlTable("");
                echo "<p>La description de table est: ".$oTable->getActifTable()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>

            <h2><hr>setActifTable() avec "desc" </h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table();
                $oTable -> setActifTable("desc");
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";

                //$oTable->setSUrlTable("");
                echo "<p>La description de table est: ".$oTable->getActifTable()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?>
            <h2><hr>setActifTable() avec 173 </h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table();
                $oTable -> setActifTable(173);
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";

                //$oTable->setSUrlTable("");
                echo "<p>La description de table est: ".$oTable->getActifTable()."</p>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }
            ?> 
            
            <h2><hr>__construct() avec un paramètre - $idTable</h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table(45);
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }  
            ?>
            <h2><hr>__construct() avec deux paramètres - $idTable et $iCapacTable</h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table(45, 4);
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }  
            ?>
            <h2><hr>__construct() avec trois paramètres - $idTable, $iCapacTable et $sActifTable</h2>
            <?php
            try{
                /* Instancier un objet de la classe Table */
                $oTable = new Table(45, 4, "a");
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";   
            }  
            ?>

            <h2><hr>ajouterUnTable() avec setCapacTable() = 8</h2>
            <?php 
            try{
                $oTable = new Table();
                $oTable -> setCapacTable(8);               
//                echo $oTable -> getCapacTable();
                $iIdInsere = $oTable->ajouterUnTable();
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
                echo "<pre>";
                var_dump($iIdInsere);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            <h2><hr>modifierUnTable() avec $iCapacTable = 4</h2>
            <?php 
                try{
                    
                    $oTable = new Table();
                    $oTable ->setIdTable(2);
                    $oTable ->rechercherTable();                    
                    $oTable2 = new Table($oTable ->getIdTable(), $oTable ->getCapacTable(), $oTable ->getActifTable());
                    $oTable2 ->setIdTable(2);
                    //en commentaire pour ne pas modifier les donnees;
//                    $oTable2 ->setImageTable("bordeaux_url.png");
//                    $oTable2 ->setActifTable("vin rouge sec");
//                    $oTable2 ->setPrixTable(13.99);
                    $oTable2 ->setCapacTable(4);
                    $bResult = $oTable2->modifierUnTable();

                    echo "<pre>";   
                    var_dump($bResult);
                    echo "</pre>";
                }catch(Exception $oExcep){
                    echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
                }
            ?>

            <h2><hr>supprimerUnTable() </h2>
            <?php 
                try{
                    $oTable = new Table(6);

                    $bResult = $oTable->supprimerUnTable();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                }catch(Exception $oExcep){
                    echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
                }
            ?>

            <h2><hr>rechercherTable() avec $idTable = 17 N'EXISTE PAS</h2>
            <?php 
            try{
                $oTable= new Table(17,"a","a");

                $bResult = $oTable->rechercherTable();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherTable() avec $idTable = 2 EXISTE </h2>
            <?php 
            try{
                $oTable= new Table();
                $oTable ->setIdTable(2);
                $bResult = $oTable->rechercherTable();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherTable() avec $iCapacTable = 23 N'EXISTE PAS </h2>
            <?php 
            try{
                $oTable= new Table();
                $oTable ->setCapacTable(23);

                $bResult = $oTable->rechercherTableByCapacite();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherTable() avec $iCapacTable = 4 EXISTE </h2>
            <?php 
            try{
                $oTable= new Table();
                $oTable ->setCapacTable(4);
                $bResult = $oTable->rechercherTableByCapacite();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>

            <h2><hr>rechercherAllTable()</h2>
            <?php 
            try{
                $oTable= new Table();

                $bResult = $oTable->rechercherAllTables();

                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
                print_r($bResult);
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            
            <h2><hr>rechercherTablesByCapacite()</h2>
            <?php 
            try{
                $oTable= new Table();
                $oTable ->setCapacTable(4);
                $bResult = $oTable->rechercherTablesByCapacite();
                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
                print_r($bResult);
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            <h2><hr>rechercherTablesByCapacites($min,$max)</h2>
            <?php 
            try{
                $oTable= new Table();
                $min = 4;
                $max = 8;
                $bResult = $oTable->rechercherTablesByCapacites($min,$max);
                echo "<pre>";
                var_dump($bResult);
                echo "</pre>";
                echo "<pre>";
                var_dump($oTable);
                echo "</pre>";
                print_r($bResult);
            }catch(Exception $oExcep){
                echo "<p style=\"color:red;\">".$oExcep->getMessage()."</p>";
            }
            ?>
            </div>
            <footer>
                <p>
                    &copy; Copyright by mpolerca
                </p>
            </footer>
        </div>
    </body>
</html>


