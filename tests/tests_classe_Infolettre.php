<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">		
        <title>tests_classe_Infolettre</title>
        <meta name="description" content="">
    </head>
    <!--/* 
     * @author Marcio Pelegrini
     * @version 2015-06-25
     * 582_N61_MA_Projet_Web_2 
     *  Resto Projet Web 2
     */-->
    <body>
        <div>
            <header>
                <h1>Test de la classe Infolettre</h1>
            </header>
            <div>
                <?php
                /* include des fichiers de librairie */
                require("../lib/MySqliLib.class.php");
                require("../lib/TypeException.class.php");
                /* include des fichiers modeles */
                require("../modeles/Infolettre.class.php");
                ?>

                <h2>setCourriel() avec "email@email.com"</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Infolettre */
                    $oInfo = new Infolettre("email@email.com");
                    echo "<pre>";
                    var_dump($oInfo);
                    echo "</pre>";
                    //$oAdministrateur->setSTitreAdministrateur("abc");
                    echo "<p>Le nom de produit est: " . $oInfo->getSEmailInfo() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>  

                <h2>setCourriel() avec ""</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Infolettre */
                    $oInfo = new Infolettre("");
                    echo "<pre>";
                    var_dump($oInfo);
                    echo "</pre>";
                    //$oAdministrateur->setSTitreAdministrateur("abc");
                    echo "<p> Le courriel est: " . $oInfo->getSEmailInfo() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>
                <h2>setCourriel() avec 4</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Administrateur */
                    $oInfo = new Infolettre();
                    echo "<pre>";
                    var_dump($oInfo);
                    echo "</pre>";
                    $oInfo->setSEmailInfo(4);
                    echo "<p>  Le courriel est: " . $oInfo->getSEmailInfo() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>          
                <h2>ajouterUnCourriel() avec setSEmailInfo = "Champlain@champlain.com"</h2>
                <?php
                try {
                    $oInfo = new Infolettre("Champlain@champlain.com");

                    $iIdInsere = $oInfo->ajouterUnCourriel();
                    echo "<pre>";
                    var_dump($oInfo);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($iIdInsere);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>
                <h2>modifierUnCourriel() avec setSEmailInfo = "Drapeau@test.com"</h2>
                <?php /*
                  try {
                  $oInfo = new Infolettre("Champlain@test.com");

                  $bResult = $oInfo->modifierUnCourriel():

                  echo "<pre>";
                  var_dump($bResult);
                  echo "</pre>";
                  } catch (Exception $oExcep) {
                  echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                  } */
                ?>
                <h2>supprimerUnCourriel() </h2>
                <?php /*
                  try {
                  $oInfo = new Infolettre("Test@test.com");

                  $bResult = $oInfo->supprimerUnCourriel():
                  echo "<pre>";
                  var_dump($bResult);
                  echo "</pre>";
                  } catch (Exception $oExcep) {
                  echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                  } */
                ?>

                <h2>rechercherCourriel() avec $courriel = "Test@test.com" EXISTE</h2>
                <?php
                try {
                    $oInfo = new Infolettre("Test@test.com");

                    $bResult = $oInfo->rechercherUnCourriel();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($oInfo);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>

                <h2>rechercherTousCourriels()</h2>
                <?php
                try {
                    $oInfo = new Infolettre();

                    $bResult = $oInfo->rechercherTousCourriels();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($oInfo);
                    echo "</pre>";
                    print_r($bResult);
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>          
            </div>
            <footer>
                <p>
                    &copy; Copyright  by Marcio Pelegrini
                </p>
            </footer>
        </div>
    </body>
</html>