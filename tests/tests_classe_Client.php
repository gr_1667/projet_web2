<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">		
        <title>tests_classe_Client</title>
        <meta name="description" content="">
    </head>
    <!--/* 
     * @author Marcio Pelegrini
     * @version 2015-06-25
     * 582_N61_MA_Projet_Web_2 
     *  Resto Projet Web 2
     */-->
    <body>
        <div>
            <header>
                <h1>Test de la classe Client</h1>
            </header>
            <div>
                <?php
                /* include des fichiers de librairie */
                require("../lib/MySqliLib.class.php");
                require("../lib/TypeException.class.php");
                /* include des fichiers modeles */
                require("../modeles/Client.class.php");
                ?>

                <h2>setIdClient() avec ""</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Client */
                    $oClient = new Client("", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a");
                    echo "<pre>";
                    var_dump($oClient);
                    echo "</pre>";
                    echo "<p>Le id du client est: " . $oClient->getIdClient() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>

                <h2>setIdClient() avec 3</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Client */
                    $oClient = new Client(3, "a", "a");
                    echo "<pre>";
                    var_dump($oClient);
                    echo "</pre>";
                    echo "<pre> Le id du client est: " . $oClient->getIdClient() . "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>   

                <h2>setNomClient() avec "Nom Client"</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Client */
                    $oClient = new Client(1, "Email Client", "Nom Client", "Prenom");
                    echo "<pre>";
                    var_dump($oClient);
                    echo "</pre>";
                    //$oClient->setSTitreClient("abc");
                    echo "<p>Le nom de produit est: " . $oClient->getSNomClient() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>  

                <h2>setNomClient() avec ""</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Client */
                    $oClient = new Client(1, "", "", "");
                    echo "<pre>";
                    var_dump($oClient);
                    echo "</pre>";
                    //$oClient->setSTitreClient("abc");
                    echo "<p> Le nom de produit est: " . $oClient->getSNomClient() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>
                <h2>setNomClient() avec 4</h2>
                <?php
                try {
                    /* Instancier un objet de la classe Client */
                    $oClient = new Client();
                    echo "<pre>";
                    var_dump($oClient);
                    echo "</pre>";
                    $oClient->setSNomClient(4);
                    echo "<p>  Le nom de produit est: " . $oClient->getSNomClient() . "</p>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>          
                <h2>ajouterUnClient() avec setSNomClient = "Champlain"</h2>
                <?php
                try {
                    $oClient = new Client();
                    $oClient->setSEmailClient("Champlain@test.com");
                    $oClient->setSNomClient("Champlain");

                    $iIdInsere = $oClient->ajouterUnClient();
                    echo "<pre>";
                    var_dump($oClient);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($iIdInsere);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>
                <h2>modifierUnClient() avec setSNomClient = "Drapeau"</h2>
                <?php
                try {
                    $oClient = new Client(1, "Champlain@test.com", "Drapeau");

                    $bResult = $oClient->modifierUnClient();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>
                <h2>supprimerUnClient() </h2>
                <?php
                try {
                    $oClient = new Client(15);

                    $bResult = $oClient->supprimerUnClient();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>

                <h2>rechercherClient() avec $idClient = 17 N'EXISTE PAS</h2>
                <?php
                try {
                    $oClient = new Client(17, "a", "a");

                    $bResult = $oClient->rechercherClient();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($oClient);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>

                <h2>rechercherClient() avec $idClient = 14 EXISTE </h2>
                <?php
                try {
                    $oClient = new Client(14, "a", "a");

                    $bResult = $oClient->rechercherClient();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($oClient);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>

                <h2>rechercherClient() avec $sNomClient = "asa" N'EXISTE PAS </h2>
                <?php
                try {
                    $oClient = new Client(1, "a", "asa");

                    $bResult = $oClient->rechercherClientParNom();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($oClient);
                    echo "</pre>";
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>

                <h2>rechercherTousClient()</h2>
                <?php
                try {
                    $oClient = new Client();

                    $bResult = $oClient->rechercherTousClients();

                    echo "<pre>";
                    var_dump($bResult);
                    echo "</pre>";
                    echo "<pre>";
                    var_dump($oClient);
                    echo "</pre>";
                    print_r($bResult);
                } catch (Exception $oExcep) {
                    echo "<p style=\"color:red;\">" . $oExcep->getMessage() . "</p>";
                }
                ?>          
            </div>
            <footer>
                <p>
                    &copy; Copyright  by 
                </p>
            </footer>
        </div>
    </body>
</html>


