<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">		
        <title>tests_classe_Produit</title>
        <meta name="description" content="">
        <meta name="author" content="cmartin;mpolerca">		
    </head>
    <!--/* 
     * @author Polerca Mihai
     * @version 2015-06-25
     * 582_N61_MA_Projet_Web_2 
     *  Resto Projet Web 2
     */-->
    <body>
        <div>
            <header>
                    <h1>tests_classe_Produit</h1>
            </header>
            <div>
            <?php
            /* include des fichiers de librairie */
            require("../lib/MySqliLib.class.php");
            require("../lib/dbConn.class.php");
            require("../lib/TypeException.class.php");                
            /* include des fichiers modeles */
            require("../modeles/Produit.class.php");
            require("../admin/vues/VueProduit.class.php");
            require("../modeles/Categorie.class.php");
            ?>
                
                <div>
                    <?php
                    try {
            $oCategorie = new Categorie();
            $aOCategories = $oCategorie->rechercherAllCategories();
            if (isset($_POST['cmd']) == false) {
                //Afficher le formulaire d'ajout
                VueProduit::adm_afficherFormAdd($sMsg = "",$aOCategories);
            } else {
               
                    //sinon sauvegarder
                    $oProduit = new Produit(1, trim($_POST['txtNomProduit']), trim($_POST['txtDescProduit']), trim($_POST['txtPrixProduit']), trim($_POST['txtImageProduit']), " ", trim($_POST['txtOCat_Produit']));
                    $oProduit->ajouterUnProduit();
                    
                    $sMsg = "L'ajout du produit - " . $oProduit->getNomProduit() . " - s'est bien déroulé.";
                    VueClient::adm_afficherFormAdd($sMsg);
               
            }
        } catch (Exception $oExcep) {
            VueProduit::adm_afficherFormAdd($oExcep->getMessage());
        }?>
            </div>
            <footer>
                <p>
                    &copy; Copyright  by cmartin et mpolerca
                </p>
            </footer>
        </div>
    </body>
</html>


