-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2015 at 02:44 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrateurs`
--

CREATE TABLE IF NOT EXISTS `administrateurs` (
  `ADM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADM_EMAIL` varchar(255) NOT NULL,
  `ADM_NOM` varchar(45) NOT NULL,
  `ADM_PRENOM` varchar(45) NOT NULL,
  `ADM_MOTPASSE` varchar(255) DEFAULT NULL,
  `ADM_ACTIF` char(1) DEFAULT NULL,
  PRIMARY KEY (`ADM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `CAT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAT_NOM` varchar(45) DEFAULT NULL,
  `CAT_ACTIF` char(1) DEFAULT NULL,
  PRIMARY KEY (`CAT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `CLI_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CLI_EMAIL` varchar(255) NOT NULL,
  `CLI_NOM` varchar(45) NOT NULL,
  `CLI_PRENOM` varchar(45) NOT NULL,
  `CLI_ADRESSE` varchar(45) NOT NULL,
  `CLI_VILLE` varchar(45) NOT NULL,
  `CLI_CODEPOSTAL` varchar(45) NOT NULL,
  `CLI_PHONE` varchar(45) NOT NULL,
  `CLI_MOTPASSE` varchar(255) NOT NULL,
  `CLI_ACTIF` char(1) DEFAULT NULL,
  `CLI_POINTS` int(11) DEFAULT NULL,
  PRIMARY KEY (`CLI_ID`,`CLI_EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `commandes`
--

CREATE TABLE IF NOT EXISTS `commandes` (
  `COM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `COM_DATE` date NOT NULL,
  `COM_TOTAL` int(11) NOT NULL,
  `COM_RABAIS` int(11) DEFAULT NULL,
  `COM_MOD_PAIEMENT` varchar(45) NOT NULL,
  `COM_LIVRAISON` int(11) NOT NULL,
  `COM_ACTIF` char(1) DEFAULT NULL,
  `CLIENTS_CLI_ID` int(11) NOT NULL,
  PRIMARY KEY (`COM_ID`),
  KEY `fk_COMMANDES_CLIENTS1_idx` (`CLIENTS_CLI_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `commandes_produits`
--

CREATE TABLE IF NOT EXISTS `commandes_produits` (
  `COMMANDES_COM_ID` int(11) NOT NULL,
  `PRODUITS_PROD_ID` int(11) NOT NULL,
  `PROD_QTT` int(11) NOT NULL,
  KEY `fk_table1_COMMANDES1_idx` (`COMMANDES_COM_ID`),
  KEY `fk_table1_PRODUITS1_idx` (`PRODUITS_PROD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `evenements`
--

CREATE TABLE IF NOT EXISTS `evenements` (
  `EVE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVE_DESCRIPTION` varchar(45) NOT NULL,
  `EVE_IMAGE` varchar(255) DEFAULT NULL,
  `EVE_DATE` date NOT NULL,
  `EVE_ACTIF` char(1) DEFAULT NULL,
  PRIMARY KEY (`EVE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `infolettre`
--

CREATE TABLE IF NOT EXISTS `infolettre` (
  `INFO_EMAIL` varchar(255) NOT NULL,
  PRIMARY KEY (`INFO_EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produits`
--

CREATE TABLE IF NOT EXISTS `produits` (
  `PROD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROD_NOM` varchar(100) DEFAULT NULL,
  `PROD_DESCRIPTION` varchar(255) DEFAULT NULL,
  `PROD_PRIX` double NOT NULL,
  `PROD_IMAGE` varchar(255) NOT NULL,
  `PROD_ACTIF` char(1) DEFAULT NULL,
  `CATEGORIES_CAT_ID` int(11) NOT NULL,
  PRIMARY KEY (`PROD_ID`),
  KEY `fk_PRODUITS_CATEGORIES1_idx` (`CATEGORIES_CAT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `RES_ID` int(11) NOT NULL,
  `RES_DATE` date NOT NULL,
  `RES_HEURE_DEBUT` char(5) NOT NULL,
  `RES_HEURE_FIN` char(5) NOT NULL,
  `RES_PERSONNES` int(11) NOT NULL,
  `RES_TYPE` enum('table','salle') NOT NULL,
  `RES_ACTIF` char(1) DEFAULT NULL,
  `CLIENTS_CLI_ID` int(11) NOT NULL,
  `TABLES_TAB_ID` int(11) NOT NULL,
  PRIMARY KEY (`RES_ID`),
  KEY `fk_RESERVATION_CLIENTS1_idx` (`CLIENTS_CLI_ID`),
  KEY `fk_RESERVATION_TABLES1_idx` (`TABLES_TAB_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE IF NOT EXISTS `tables` (
  `TAB_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TAB_CAPACITE` int(11) NOT NULL,
  `TAB_ACTIF` char(1) DEFAULT NULL,
  PRIMARY KEY (`TAB_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `fk_COMMANDES_CLIENTS1` FOREIGN KEY (`CLIENTS_CLI_ID`) REFERENCES `clients` (`CLI_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `commandes_produits`
--
ALTER TABLE `commandes_produits`
  ADD CONSTRAINT `fk_table1_COMMANDES1` FOREIGN KEY (`COMMANDES_COM_ID`) REFERENCES `commandes` (`COM_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_table1_PRODUITS1` FOREIGN KEY (`PRODUITS_PROD_ID`) REFERENCES `produits` (`PROD_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `fk_PRODUITS_CATEGORIES1` FOREIGN KEY (`CATEGORIES_CAT_ID`) REFERENCES `categories` (`CAT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_RESERVATION_CLIENTS1` FOREIGN KEY (`CLIENTS_CLI_ID`) REFERENCES `clients` (`CLI_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_RESERVATION_TABLES1` FOREIGN KEY (`TABLES_TAB_ID`) REFERENCES `tables` (`TAB_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
