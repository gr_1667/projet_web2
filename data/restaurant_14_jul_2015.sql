-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2015 at 04:34 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrateurs`
--

CREATE TABLE IF NOT EXISTS `administrateurs` (
  `ADM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADM_EMAIL` varchar(255) NOT NULL,
  `ADM_NOM` varchar(45) NOT NULL,
  `ADM_PRENOM` varchar(45) NOT NULL,
  `ADM_MOTPASSE` varchar(255) DEFAULT NULL,
  `ADM_ROLE` enum('employe','admin') NOT NULL,
  PRIMARY KEY (`ADM_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `administrateurs`
--

INSERT INTO `administrateurs` (`ADM_ID`, `ADM_EMAIL`, `ADM_NOM`, `ADM_PRENOM`, `ADM_MOTPASSE`, `ADM_ROLE`) VALUES
(3, 'marcio.pelegrini@gmail.com', 'Pelegrini', 'Marcio Andrei', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'admin'),
(4, 'bob@bob.com', 'Bob', 'Bob', 'c1133c5e2c3ae838c72bec71ae5dc2bad3ca2adf', 'admin'),
(11, 'drapeau@jacques.com', 'Drapeau', 'Jacques', '982fd8b711279888a3b54f5af24f185041d22ee6', 'employe'),
(12, 'mandrei@mandrei.ca', 'Pelegrini', 'Marcio', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'employe'),
(13, 'fulano@fulano.com.br', 'Fulano da silva', 'Ciclano', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `CAT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAT_NOM` varchar(45) DEFAULT NULL,
  `CAT_DESC` varchar(600) DEFAULT NULL,
  `CAT_IMAGE` varchar(100) NOT NULL,
  `CAT_ACTIF` char(1) DEFAULT NULL,
  PRIMARY KEY (`CAT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CAT_ID`, `CAT_NOM`, `CAT_DESC`, `CAT_IMAGE`, `CAT_ACTIF`) VALUES
(1, 'Viande', 'ssssssssssssssssssssssss', 'tango.jpg', NULL),
(2, 'Vins', 'Une large gamme de vins haut qualite', 'tango.jpg', NULL),
(3, 'Dejeuner', 'description', 'tango.jpg', NULL),
(5, 'Aperitives', 'frech', 'tango.jpg', NULL),
(7, 'Desserts', 'doux', 'tango.jpg', NULL),
(9, 'Salades', 'Vitamineux', 'tango.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `CLI_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CLI_EMAIL` varchar(255) NOT NULL,
  `CLI_NOM` varchar(45) NOT NULL,
  `CLI_PRENOM` varchar(45) NOT NULL,
  `CLI_ADRESSE` varchar(45) NOT NULL,
  `CLI_VILLE` varchar(45) NOT NULL,
  `CLI_CODEPOSTAL` varchar(45) NOT NULL,
  `CLI_PHONE` varchar(45) NOT NULL,
  `CLI_MOTPASSE` varchar(255) NOT NULL,
  `CLI_ACTIF` char(1) NOT NULL,
  `CLI_POINTS` int(11) DEFAULT NULL,
  PRIMARY KEY (`CLI_ID`,`CLI_EMAIL`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`CLI_ID`, `CLI_EMAIL`, `CLI_NOM`, `CLI_PRENOM`, `CLI_ADRESSE`, `CLI_VILLE`, `CLI_CODEPOSTAL`, `CLI_PHONE`, `CLI_MOTPASSE`, `CLI_ACTIF`, `CLI_POINTS`) VALUES
(3, 'bob@bob.com', 'bob', 'bob', 'bob', 'bob', 'bob', 'bob', 'c1133c5e2c3ae838c72bec71ae5dc2bad3ca2adf', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `commandes`
--

CREATE TABLE IF NOT EXISTS `commandes` (
  `COM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `COM_DATE` date NOT NULL,
  `COM_TOTAL` int(11) NOT NULL,
  `COM_RABAIS` int(11) DEFAULT NULL,
  `COM_MOD_PAIEMENT` varchar(45) NOT NULL,
  `COM_LIVRAISON` int(11) NOT NULL,
  `COM_ACTIF` char(1) NOT NULL,
  `CLIENTS_CLI_ID` int(11) NOT NULL,
  PRIMARY KEY (`COM_ID`),
  KEY `fk_COMMANDES_CLIENTS1_idx` (`CLIENTS_CLI_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `commandes_produits`
--

CREATE TABLE IF NOT EXISTS `commandes_produits` (
  `COMMANDES_COM_ID` int(11) NOT NULL,
  `PRODUITS_PROD_ID` int(11) NOT NULL,
  `PROD_QTT` int(11) NOT NULL,
  KEY `fk_table1_COMMANDES1_idx` (`COMMANDES_COM_ID`),
  KEY `fk_table1_PRODUITS1_idx` (`PRODUITS_PROD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `evenements`
--

CREATE TABLE IF NOT EXISTS `evenements` (
  `EVE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVE_DESCRIPTION` varchar(45) NOT NULL,
  `EVE_IMAGE` varchar(255) DEFAULT NULL,
  `EVE_DATE` date NOT NULL,
  `EVE_ACTIF` char(1) NOT NULL,
  PRIMARY KEY (`EVE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `evenements`
--

INSERT INTO `evenements` (`EVE_ID`, `EVE_DESCRIPTION`, `EVE_IMAGE`, `EVE_DATE`, `EVE_ACTIF`) VALUES
(1, 'Tangoo', 'Le2030.png', '2015-07-22', ''),
(2, 'bouffe', 'Le2030.png', '2015-07-16', ''),
(3, 'Tango', 'tango.jpg', '2015-07-23', '');

-- --------------------------------------------------------

--
-- Table structure for table `infolettre`
--

CREATE TABLE IF NOT EXISTS `infolettre` (
  `INFO_EMAIL` varchar(255) NOT NULL,
  PRIMARY KEY (`INFO_EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `infolettre`
--

INSERT INTO `infolettre` (`INFO_EMAIL`) VALUES
('admin2@admin.com'),
('admin@admin.com');

-- --------------------------------------------------------

--
-- Table structure for table `personnes`
--

CREATE TABLE IF NOT EXISTS `personnes` (
  `PERS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERS_NOM` varchar(100) NOT NULL,
  `PERS_PRENOM` varchar(100) NOT NULL,
  `PERS_PHONE` int(11) NOT NULL,
  PRIMARY KEY (`PERS_ID`),
  KEY `PERS_ID` (`PERS_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `personnes`
--

INSERT INTO `personnes` (`PERS_ID`, `PERS_NOM`, `PERS_PRENOM`, `PERS_PHONE`) VALUES
(3, 'asasa', 'fds', 123),
(4, 'Travolta', 'John', 2147483647),
(5, 'Travolta', 'John', 2147483647),
(6, 'Polerca', 'Mihai', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `produits`
--

CREATE TABLE IF NOT EXISTS `produits` (
  `PROD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROD_NOM` varchar(100) DEFAULT NULL,
  `PROD_DESCRIPTION` varchar(255) DEFAULT NULL,
  `PROD_PRIX` double NOT NULL,
  `PROD_IMAGE` varchar(255) NOT NULL,
  `PROD_ACTIF` char(1) DEFAULT NULL,
  `CATEGORIES_CAT_ID` int(11) NOT NULL,
  PRIMARY KEY (`PROD_ID`),
  KEY `fk_PRODUITS_CATEGORIES1_idx` (`CATEGORIES_CAT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `produits`
--

INSERT INTO `produits` (`PROD_ID`, `PROD_NOM`, `PROD_DESCRIPTION`, `PROD_PRIX`, `PROD_IMAGE`, `PROD_ACTIF`, `CATEGORIES_CAT_ID`) VALUES
(4, 'BBQ', 'Delicieux. Vestibulum volutpatturpis ut massa commodo', 3.99, 'urlImage', NULL, 1),
(18, 'Chardonnay', 'Vin blanc sec', 12.99, 'url/char.png', NULL, 2),
(19, 'Pinnot', 'Vin rouge sec', 13.99, 'url/pinnot.png', NULL, 2),
(20, 'prod', 'desc2', 12.47, 'sdkj.png', NULL, 5),
(21, 'Cabernet', 'une description un vin de 1982', 1.99, 'url.png', NULL, 2),
(22, 'prod2', 'desc2', 2.99, 'Le2030.png', NULL, 1),
(23, 'Caffe', 'caffeeeeee', 2.33, 'back.jpg', NULL, 5),
(24, 'Caffe', 'caffeeeeee', 2.33, '', NULL, 5),
(25, 'saks', 'sdkj', 21, '', NULL, 1),
(27, 'ahahah', 'mmmmmmm', 123, 'tango.jpg', NULL, 1),
(29, 'THE', 'THE THE', 320, 'tanfi.jpg', NULL, 1),
(31, 'THE', 'sasla;ka;lsk', 1234, 'add_24.png', NULL, 1),
(35, 'NOM', 'DESC', 1234, 'tango.jpg', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `RES_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RES_DATE` varchar(20) NOT NULL,
  `RES_HEURE_DEBUT` char(20) NOT NULL,
  `RES_HEURE_FIN` char(20) NOT NULL,
  `RES_TYPE` enum('table','salle') NOT NULL,
  `CLIENTS_CLI_ID` int(11) DEFAULT NULL,
  `PERS_ID` int(11) DEFAULT NULL,
  `TABLES_TAB_ID` int(11) NOT NULL,
  PRIMARY KEY (`RES_ID`),
  KEY `fk_RESERVATION_CLIENTS1_idx` (`CLIENTS_CLI_ID`),
  KEY `fk_RESERVATION_TABLES1_idx` (`TABLES_TAB_ID`),
  KEY `fk_RESERVATION_PERSONNES1_idx` (`PERS_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`RES_ID`, `RES_DATE`, `RES_HEURE_DEBUT`, `RES_HEURE_FIN`, `RES_TYPE`, `CLIENTS_CLI_ID`, `PERS_ID`, `TABLES_TAB_ID`) VALUES
(1, '2015-07-16', '22', '22', 'table', NULL, 3, 2),
(4, '2015-07-22', '14:02', '14:02', 'table', NULL, 5, 2),
(5, '2015-07-24', '14:02', '14:02', 'table', NULL, 6, 8),
(6, '22 sep', '1', '4', 'table', 3, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE IF NOT EXISTS `tables` (
  `TAB_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TAB_CAPACITE` int(11) NOT NULL,
  `TAB_ACTIF` char(1) NOT NULL,
  PRIMARY KEY (`TAB_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`TAB_ID`, `TAB_CAPACITE`, `TAB_ACTIF`) VALUES
(1, 4, 'a'),
(2, 4, 'a'),
(3, 8, 'a'),
(4, 8, 'a'),
(5, 8, 'a'),
(8, 2, 'a'),
(9, 4, 'a');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `fk_COMMANDES_CLIENTS1` FOREIGN KEY (`CLIENTS_CLI_ID`) REFERENCES `clients` (`CLI_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `commandes_produits`
--
ALTER TABLE `commandes_produits`
  ADD CONSTRAINT `fk_table1_COMMANDES1` FOREIGN KEY (`COMMANDES_COM_ID`) REFERENCES `commandes` (`COM_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_table1_PRODUITS1` FOREIGN KEY (`PRODUITS_PROD_ID`) REFERENCES `produits` (`PROD_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `fk_PRODUITS_CATEGORIES1` FOREIGN KEY (`CATEGORIES_CAT_ID`) REFERENCES `categories` (`CAT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_RESERVATION_CLIENTS1` FOREIGN KEY (`CLIENTS_CLI_ID`) REFERENCES `clients` (`CLI_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_RESERVATION_PERSONNES1` FOREIGN KEY (`PERS_ID`) REFERENCES `personnes` (`PERS_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_RESERVATION_TABLES1` FOREIGN KEY (`TABLES_TAB_ID`) REFERENCES `tables` (`TAB_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
