-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2015 at 06:18 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `personnes`
--

CREATE TABLE IF NOT EXISTS `personnes` (
  `PERS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERS_NOM` varchar(100) NOT NULL,
  `PERS_PRENOM` varchar(100) NOT NULL,
  `PERS_PHONE` int(11) NOT NULL,
  PRIMARY KEY (`PERS_ID`),
  KEY `PERS_ID` (`PERS_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `personnes`
--

INSERT INTO `personnes` (`PERS_ID`, `PERS_NOM`, `PERS_PRENOM`, `PERS_PHONE`) VALUES
(3, 'asasa', 'fds', 123),
(4, 'Travolta', 'John', 2147483647),
(5, 'Travolta', 'John', 2147483647),
(6, 'Polerca', 'Mihai', 2147483647);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
