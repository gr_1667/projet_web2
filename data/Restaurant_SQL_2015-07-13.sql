-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 13 Juillet 2015 à 19:27
-- Version du serveur :  5.6.21
-- Version de PHP :  5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `restaurant`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateurs`
--

CREATE TABLE IF NOT EXISTS `administrateurs` (
`ADM_ID` int(11) NOT NULL,
  `ADM_EMAIL` varchar(255) NOT NULL,
  `ADM_NOM` varchar(45) NOT NULL,
  `ADM_PRENOM` varchar(45) NOT NULL,
  `ADM_MOTPASSE` varchar(255) DEFAULT NULL,
  `ADM_ROLE` enum('commun','admin') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `administrateurs`
--

INSERT INTO `administrateurs` (`ADM_ID`, `ADM_EMAIL`, `ADM_NOM`, `ADM_PRENOM`, `ADM_MOTPASSE`, `ADM_ROLE`) VALUES
(3, 'marcio.pelegrini@gmail.com', 'Pelegrini', 'Marcio Andrei', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'admin'),
(4, 'bob@bob.com', 'Bob', 'Bob', 'c1133c5e2c3ae838c72bec71ae5dc2bad3ca2adf', 'commun'),
(11, 'drapeau@jacques.com', 'Drapeau', 'Jacques', '982fd8b711279888a3b54f5af24f185041d22ee6', 'commun'),
(12, 'mandrei@mandrei.ca', 'Pelegrini', 'Marcio', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'commun'),
(13, 'fulano@fulano.com.br', 'Fulano da silva', 'Ciclano', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`CAT_ID` int(11) NOT NULL,
  `CAT_NOM` varchar(45) DEFAULT NULL,
  `CAT_ACTIF` char(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`CAT_ID`, `CAT_NOM`, `CAT_ACTIF`) VALUES
(1, 'Pizza', NULL),
(4, 'Salade', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
`CLI_ID` int(11) NOT NULL,
  `CLI_EMAIL` varchar(255) NOT NULL DEFAULT '',
  `CLI_NOM` varchar(45) DEFAULT NULL,
  `CLI_PRENOM` varchar(45) DEFAULT NULL,
  `CLI_ADRESSE` varchar(45) DEFAULT NULL,
  `CLI_VILLE` varchar(45) DEFAULT NULL,
  `CLI_CODEPOSTAL` varchar(45) DEFAULT NULL,
  `CLI_PHONE` varchar(45) DEFAULT NULL,
  `CLI_MOTPASSE` varchar(255) DEFAULT NULL,
  `CLI_ACTIF` char(1) DEFAULT NULL,
  `CLI_POINTS` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `clients`
--

INSERT INTO `clients` (`CLI_ID`, `CLI_EMAIL`, `CLI_NOM`, `CLI_PRENOM`, `CLI_ADRESSE`, `CLI_VILLE`, `CLI_CODEPOSTAL`, `CLI_PHONE`, `CLI_MOTPASSE`, `CLI_ACTIF`, `CLI_POINTS`) VALUES
(3, 'Champlain@test.com', 'Champlain', 'Samuel', 'ooooo', 'oooooo', 'J4Y 1B7', '(514) 441-2633', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '1', 5),
(4, 'fulano@fulano.com.br', 'Fulano da silva', 'Marcio andrei', '3790, Av.', 'Brossards', 'J4Y1B7', '514-441-2632', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '1', 50),
(7, 'marisa.pelegrini@gmail.com', 'Pelegrini', 'Marisa Molaia', '7350, av. Tisserand, app 206-B', 'Brossard', 'J4Y 1B7', '5144412632', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '1', 0),
(8, 'marcio.pelegrini@gmail.com', 'De novo', 'Marcio', 'tisserand', 'Laval', 'J4Y 1B7', '(999) 999-9999', '982fd8b711279888a3b54f5af24f185041d22ee6', '1', 5);

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE IF NOT EXISTS `commandes` (
`COM_ID` int(11) NOT NULL,
  `COM_DATE` date NOT NULL,
  `COM_TOTAL` int(11) NOT NULL,
  `COM_RABAIS` int(11) DEFAULT NULL,
  `COM_MOD_PAIEMENT` varchar(45) NOT NULL,
  `COM_LIVRAISON` int(11) NOT NULL,
  `COM_ACTIF` char(1) NOT NULL,
  `CLIENTS_CLI_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `commandes_produits`
--

CREATE TABLE IF NOT EXISTS `commandes_produits` (
  `COMMANDES_COM_ID` int(11) NOT NULL,
  `PRODUITS_PROD_ID` int(11) NOT NULL,
  `PROD_QTT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `evenements`
--

CREATE TABLE IF NOT EXISTS `evenements` (
`EVE_ID` int(11) NOT NULL,
  `EVE_DESCRIPTION` varchar(45) NOT NULL,
  `EVE_IMAGE` varchar(255) DEFAULT NULL,
  `EVE_DATE` date NOT NULL,
  `EVE_ACTIF` char(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `evenements`
--

INSERT INTO `evenements` (`EVE_ID`, `EVE_DESCRIPTION`, `EVE_IMAGE`, `EVE_DATE`, `EVE_ACTIF`) VALUES
(1, 'Tango', 'tango.jpg', '2015-07-14', ''),
(14, 'Teste encore', 'tango.jpg', '2015-07-24', ''),
(16, 'Jazz Festival', 'jazz.jpg', '2015-07-16', ''),
(17, 'Teste', 'jazz.jpg', '2015-07-08', ''),
(18, 'jjhjkhkjhjk', 'tango.jpg', '2015-07-08', ''),
(19, 'jjhjkhkjhjk', 'tango.jpg', '2015-07-08', '');

-- --------------------------------------------------------

--
-- Structure de la table `infolettre`
--

CREATE TABLE IF NOT EXISTS `infolettre` (
  `INFO_EMAIL` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `infolettre`
--

INSERT INTO `infolettre` (`INFO_EMAIL`) VALUES
('andrei@marcio.ocm'),
('champ@champlain.com'),
('jean.drapeau@parcmtl.ca'),
('mateus@marc.com.br');

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE IF NOT EXISTS `produits` (
`PROD_ID` int(11) NOT NULL,
  `PROD_NOM` varchar(100) NOT NULL,
  `PROD_DESCRIPTION` varchar(255) NOT NULL,
  `PROD_PRIX` double NOT NULL,
  `PROD_IMAGE` varchar(255) NOT NULL,
  `PROD_ACTIF` char(1) DEFAULT NULL,
  `CATEGORIES_CAT_ID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `produits`
--

INSERT INTO `produits` (`PROD_ID`, `PROD_NOM`, `PROD_DESCRIPTION`, `PROD_PRIX`, `PROD_IMAGE`, `PROD_ACTIF`, `CATEGORIES_CAT_ID`) VALUES
(2, 'adsfasf', 'dfasfadsfa', 2.99, 'tango.jpg', NULL, 1),
(3, 'salada', 'ddfgdfgdsfgdsf', 5.99, 'jazz.jpg', NULL, 4);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
`RES_ID` int(11) NOT NULL,
  `RES_DATE` date NOT NULL,
  `RES_HEURE_DEBUT` char(5) NOT NULL,
  `RES_HEURE_FIN` char(5) NOT NULL,
  `RES_PERSONNES` int(11) NOT NULL,
  `RES_TYPE` enum('table','salle') NOT NULL,
  `RES_ACTIF` char(1) NOT NULL,
  `RES_NOM_CLIENT` varchar(80) DEFAULT NULL,
  `RES_PHONE_CLIENT` varchar(45) DEFAULT NULL,
  `TABLES_TAB_ID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `reservation`
--

INSERT INTO `reservation` (`RES_ID`, `RES_DATE`, `RES_HEURE_DEBUT`, `RES_HEURE_FIN`, `RES_PERSONNES`, `RES_TYPE`, `RES_ACTIF`, `RES_NOM_CLIENT`, `RES_PHONE_CLIENT`, `TABLES_TAB_ID`) VALUES
(1, '2015-07-08', '22:00', '00:59', 4, 'salle', '', 'Fulano', '(450) 890-1328', 1),
(2, '2015-07-22', '21:00', '23:00', 2, 'table', '', 'Marcio Andrei', NULL, 1),
(3, '2015-07-13', '23:00', '23:59', 5, 'table', '', 'Marcio', '1', 1),
(4, '2015-07-13', '22:00', '00:00', 4, 'table', '', 'Andrei', '(514)', 2),
(5, '2015-07-24', '19:00', '20:00', 7, 'table', '', 'Ciclano', '(450) 890-1328', 1);

-- --------------------------------------------------------

--
-- Structure de la table `tables`
--

CREATE TABLE IF NOT EXISTS `tables` (
`TAB_ID` int(11) NOT NULL,
  `TAB_CAPACITE` int(11) NOT NULL,
  `TAB_ACTIF` char(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `tables`
--

INSERT INTO `tables` (`TAB_ID`, `TAB_CAPACITE`, `TAB_ACTIF`) VALUES
(1, 10, 'a'),
(2, 5, '1');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `administrateurs`
--
ALTER TABLE `administrateurs`
 ADD PRIMARY KEY (`ADM_ID`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`CAT_ID`);

--
-- Index pour la table `clients`
--
ALTER TABLE `clients`
 ADD PRIMARY KEY (`CLI_ID`,`CLI_EMAIL`);

--
-- Index pour la table `commandes`
--
ALTER TABLE `commandes`
 ADD PRIMARY KEY (`COM_ID`), ADD KEY `fk_COMMANDES_CLIENTS1_idx` (`CLIENTS_CLI_ID`);

--
-- Index pour la table `commandes_produits`
--
ALTER TABLE `commandes_produits`
 ADD KEY `fk_table1_COMMANDES1_idx` (`COMMANDES_COM_ID`), ADD KEY `fk_table1_PRODUITS1_idx` (`PRODUITS_PROD_ID`);

--
-- Index pour la table `evenements`
--
ALTER TABLE `evenements`
 ADD PRIMARY KEY (`EVE_ID`);

--
-- Index pour la table `infolettre`
--
ALTER TABLE `infolettre`
 ADD PRIMARY KEY (`INFO_EMAIL`);

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
 ADD PRIMARY KEY (`PROD_ID`), ADD KEY `fk_PRODUITS_CATEGORIES1_idx` (`CATEGORIES_CAT_ID`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
 ADD PRIMARY KEY (`RES_ID`), ADD KEY `fk_RESERVATION_TABLES1_idx` (`TABLES_TAB_ID`);

--
-- Index pour la table `tables`
--
ALTER TABLE `tables`
 ADD PRIMARY KEY (`TAB_ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `administrateurs`
--
ALTER TABLE `administrateurs`
MODIFY `ADM_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
MODIFY `CAT_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `clients`
--
ALTER TABLE `clients`
MODIFY `CLI_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `commandes`
--
ALTER TABLE `commandes`
MODIFY `COM_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `evenements`
--
ALTER TABLE `evenements`
MODIFY `EVE_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `produits`
--
ALTER TABLE `produits`
MODIFY `PROD_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
MODIFY `RES_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `tables`
--
ALTER TABLE `tables`
MODIFY `TAB_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
ADD CONSTRAINT `fk_COMMANDES_CLIENTS1` FOREIGN KEY (`CLIENTS_CLI_ID`) REFERENCES `clients` (`CLI_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `commandes_produits`
--
ALTER TABLE `commandes_produits`
ADD CONSTRAINT `fk_table1_COMMANDES1` FOREIGN KEY (`COMMANDES_COM_ID`) REFERENCES `commandes` (`COM_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_table1_PRODUITS1` FOREIGN KEY (`PRODUITS_PROD_ID`) REFERENCES `produits` (`PROD_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
ADD CONSTRAINT `fk_PRODUITS_CATEGORIES1` FOREIGN KEY (`CATEGORIES_CAT_ID`) REFERENCES `categories` (`CAT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
ADD CONSTRAINT `fk_RESERVATION_TABLES1` FOREIGN KEY (`TABLES_TAB_ID`) REFERENCES `tables` (`TAB_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
