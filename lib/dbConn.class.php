<?php
//db connection class using singleton pattern
class dbConn{
    const HOTE = "127.0.0.1";
    const USERNAME = "root";
    const PASSWORD = "";
    const DBNAME = "restaurant";
    //variable to hold connection object.
    protected static $db;
    //private construct - class cannot be instatiated externally.
    public function __construct() {

        try {
            // assign PDO object to db variable
            self::$db = new PDO('mysql:host='.dbConn::HOTE.';dbname='.dbConn::DBNAME, dbConn::USERNAME, dbConn::PASSWORD );
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//            echo 'configuration de paramentres reussi <Br>' ; 
        }
        catch (PDOException $e) {
            //Output error - would normally log this to error file rather than output to user.
            echo "Connection Error: " . $e->getMessage();
        }

    }

    // get connection function. Static method - accessible without instantiation
    public static function getConnection() {
        //Guarantees single instance, if no connection object exists then create one.
        if (!self::$db) {
        //new connection object.
        new dbConn();       
        }
//        echo 'connexion reussi<br>';        
        //return connection.
        return self::$db;
    }
    
    public static function recuperer($sRequete){
        $resultat = self::$db->prepare($sRequete);
        $resultat ->execute();
        $resultat->setFetchMode(PDO::FETCH_ASSOC);
        $aResultat = $resultat ->fetchall();
        return $aResultat;
    } 
    
    public static function executer($sRequete, $aTableau){
        $resultat = self::$db->prepare($sRequete);
        if($resultat ->execute($aTableau))
        return true;
        return false;
    }    
    public static function returnerLastId(){
        return self::$db->lastInsertId();
    }

    protected function __clone(){
    }
}//end class
 
?>