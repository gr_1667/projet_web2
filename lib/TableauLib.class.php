<?php

	/**
	 * classe TableauLib 
	 * 582-P41-MA Programmation Web Dynamique 3
	 * @author Caroline Martin
	 * @version 2015-03-05
	 */
    class TableauLib {
    	
		/* Constantes */
		
		
		
		 
    	/* Propriétés privées */
    	private $aArray = array();
		private $sMethode;
		private $mValeur;
		
		
		
		/* accesseurs */
		
		/**
		 * affecte une valeur à la propriété privée
		 * @param array $aArray
		 */
		public function setArray($aArray){			
			TypeException::estArray($aArray);			
			TypeException::estArrayAvecElement($aArray);
			
			$this->aArray = $aArray;
		}//fin de la fonction setArray()
		/**
		 * récupère la valeur de la propriété privée
		 * @return string
		 */
		public function getArray(){
			return $this->aArray;
		}//fin de la fonction getArray()
		
		/**
		 * affecte une valeur à la propriété privée
		 * @param string $sCle
		 */
		public function setMethode($sMethode){			

			$this->sMethode = $sMethode;
		}//fin de la fonction setMethode()		
		/**
		 * récupère la valeur de la propriété privée
		 * @return string
		 */
		public function getMethode(){
			return $this->sMethode;
		}//fin de la fonction getMethode()
		
		/**
		 * affecte une valeur à la propriété privée
		 * @param mixed $mValeur
		 */
		public function setValeur($mValeur){			

			$this->mValeur = $mValeur;
		}//fin de la fonction setValeur()		
		/**
		 * récupère la valeur de la propriété privée
		 * @return string
		 */
		public function getValeur(){
			return $this->mValeur;
		}//fin de la fonction getValeur()
		
		
		public function __construct($aArray = array(), $sMethode="", $mValeur=""){
			$this->setArray($aArray);			
			$this->setMethode($sMethode);
			$this->setValeur($mValeur);
		}//fin de la fonction __construct()
		
		/**
		 * supprimer un élément du tableau à 2 dimensions dont la 2eme est un objet de type sType 
		 */
		public function supprimerUnElementDuTableau(){
			$aResult = array();
			$iEtape=0;
			foreach($this->aArray as $sCle => $aValeur){//1ere dimension
				$sCh = '$iEtape = $aValeur->'.$this->getMethode().';';
				eval($sCh);
				if($iEtape != $this->mValeur){//comparer 
					$aResult[] = $aValeur;
				}
			}//fin du for
			$this->aArray = $aResult;
			
		}//fin de la fonction supprimerUnElementDuTableau()
		
		
		
    }//fin de la classe TableauLib
?>